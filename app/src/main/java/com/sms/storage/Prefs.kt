package com.sms.storage

import android.content.Context
import android.content.SharedPreferences

class Prefs private constructor(private val context: Context) {

    companion object {

        private fun getPrefs(context: Context): SharedPreferences {
            return context.getSharedPreferences(context.packageName, 0)
        }

        //    public void destroySP() {
        //        sharedPreferences = null;
        //    }

        fun clearDefaultShared(key: String, context: Context) {
            val e = getPrefs(context).edit()
            e.remove(key)
            e.apply()
        }

        fun setString(key: String, sunDefult: String, context: Context) {
            val e = getPrefs(context).edit()
            e.putString(key, sunDefult)
            e.apply()
        }

        fun setInt(key: String, defultVale: Int, context: Context) {
            val e = getPrefs(context).edit()
            e.putInt(key, defultVale)
            e.apply()
        }

        fun setBoolean(key: String, defaultVal: Boolean, context: Context) {
            val e = getPrefs(context).edit()
            e.putBoolean(key, defaultVal)
            e.apply()
        }

        fun getString(key: String, sunDefult: String, context: Context): String {
            return getPrefs(context).getString(key, sunDefult)
        }

        fun getInt(key: String, sunDefult: Int, context: Context): Int {
            return getPrefs(context).getInt(key, sunDefult)
        }

//        fun getFloat(key: String, sunDefult: Float, context: Context): Float {
//            return getPrefs(context).getFloat(key, sunDefult)
//        }
//
//        fun getLong(key: String, sunDefult: Long, context: Context): Long {
//            return getPrefs(context).getLong(key, sunDefult)
//        }

        fun getBoolean(key: String, sunDefult: Boolean, context: Context): Boolean {
            return getPrefs(context).getBoolean(key, sunDefult)
        }

        fun clearAll(context: Context) {
            val editor = getPrefs(context).edit()
            editor.clear()
            editor.commit()
        }
    }
}