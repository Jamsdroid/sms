package com.sms.storage

class PrefsKey {

    companion object {
        const val IS_LOGIN = "isLogin"
        const val AUTHORIZATION = "authorization"
        const val FIRST_NAME = "firstName"
        const val LAST_NAME = "lastName"
        const val EMAIL = "email"
        const val DATE_OF_BIRTH = "dateOfBirth"
        const val PROFILE_IMAGE = "profile_image"
        const val IS_VERIFY = "isVerify"
        const val STATUS = "status"
        const val ID = "id"
        const val DEVICE_TOKEN = "deviceToken"
        const val FIRST_COURSE_ID = "course_id"
        const val USER_TYPE = "user_type"
        const val CLASS_ID = "class_id"
        const val DIVISION_ID = "division_id"

    }
}