package com.sms.webservice

import retrofit2.Call
import retrofit2.http.*

interface RetroPlaceAPIInterface {

//    val BASE_URL: String = "https://maps.googleapis.com"

    @GET()
    fun callWebserviceGetPlaces(@Url url: String, @QueryMap params: Map<String, String>): Call<String>

}