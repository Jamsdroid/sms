package com.sms

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.sms.R.string.date_of_birth
import com.sms.common.*
import com.sms.common.Constants.Companion.GET
import com.sms.common.Constants.Companion.POST
import com.sms.common.Constants.Companion.PleaseCheckInternetConnectionEng
import com.sms.common.Constants.Companion.W_FORGOT_PASSWORD
import com.sms.common.Constants.Companion.W_LOGOUT
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.RetroClient
import com.sms.webservice.RetroFitResponse
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.net.HttpCookie
import java.util.*


class ApiHelper(internal val activity: Context) : Constants {
    internal var pd: CustomProgressBar? = null

    internal var cm: CustomProgressBar? = null
    internal var res: Response<String>? = null
    private val TAG = javaClass.simpleName
    private val httpCookie: HttpCookie? = null

    fun callWB(relativeURL: String?, cContext: Context, reqHashMap: HashMap<String, String>,
               methodName: String, aActivity: RetroFitResponse, isProgressEnable: Boolean,
               isCancelable: Boolean, canCancelOther: Boolean, methodType: String) {
        Logg.e("method Name", "--" + methodName)
        if (!APIUtils.isOnline(cContext)) {
            Toast.show(cContext, PleaseCheckInternetConnectionEng)
        } else {
            if (cm == null) {
                cm = CustomProgressBar(cContext)
                cm!!.setCancelable(false)
            }
            if (isProgressEnable) {
                try {
                    if (!(cContext as Activity).isFinishing) {
                        cm!!.show()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            if (relativeURL != null) {
                val service = RetroClient.client
                Logg.e(TAG, "uuid ===> " + Utils.getDeviceUUID(activity))
                if (!methodName.equals(W_LOGOUT, true) && !(methodName.equals(W_FORGOT_PASSWORD, true))) {
                    reqHashMap.put("device_udid", "" + Utils.getDeviceUUID(activity))
                }

                Logg.e(TAG, "Request --->> " + reqHashMap)
                Logg.e(TAG, "Relative URL --->> " + relativeURL)

                Logg.e(TAG, "API_TOKEN ==> " + Prefs.getString(PrefsKey.AUTHORIZATION, "Basic YWRtaW46MTIzNA==", activity))
                /*if (!(Prefs.getString(PrefsKey.AUTHORIZATION, "", activity).isNullOrBlank())) {
//                    AlcoholApplication.cookieListInString = Prefs.getString(SP_TOKEN, "", cContext)!!
                    Logg.e(TAG, "API_TOKEN from Prefs ==> " + AlcoholApplication.cookieListInString)
                }*/

                var call: Call<String>? = null
                if (methodType == POST) {
                    if (service != null) {
                        if ((Prefs.getString(PrefsKey.AUTHORIZATION, "Basic YWRtaW46MTIzNA==", activity)).isNullOrBlank()) {
                            call = service.callWebserviceWithoutHeader(relativeURL, reqHashMap)

                        } else {
                            call = service.callWebservice(Prefs.getString(PrefsKey.AUTHORIZATION, "Basic YWRtaW46MTIzNA==", activity), relativeURL, reqHashMap)
                        }

                    }
                } else if (methodType == GET) {
                    if (service != null) {
                        call = service.callWebserviceGet(Prefs.getString(PrefsKey.AUTHORIZATION, "Basic YWRtaW46MTIzNA==", activity), relativeURL, reqHashMap)
                    }
                }

                assert(call != null)

                call!!.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>?) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()

                        }
                        Logg.e(TAG, "response code --->> " + response!!.code())
                        if (response != null) {
                            Logg.e(TAG, "response body --->> " + response!!.body())

                            if (response!!.body() != null) {
                                try {
                                    var responseObject = JSONObject(response!!.body().toString())

                                    if (responseObject != null) {
                                        if (responseObject.has("status_code")) {
                                            if (responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE || responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE_NEW) {
                                                // call Logout api
//                                                val dataMap = HashMap<String, String>()
//                                                dataMap.put(UtilsKey.LogoutRequest.USER_ID, Prefs.getString(Constants.SP_CUSTOMER_ID, "", cContext).toString())
//                                                dataMap.put(UtilsKey.LogoutRequest.PANEL, "app")
//                                                dataMap.put(UtilsKey.LogoutRequest.CUSTOMER_ID, Prefs.getString(SP_CUSTOMER_ID, "", cContext).toString())
//                                                callWB(Constants.RELATIVE_URL_LOOUT, cContext, dataMap, Constants.W_LOGOUT_MANUAL, aActivity, false, false, true, POST)
////                                                LoginManager.getInstance().logOut()
////                                                googleSignOut(cContext)
//                                                activity.sendBroadcast(Intent().setAction(ACTION_FINISH_ACT))

                                                if (!(methodName.equals(W_LOGOUT, true))) {
                                                    try {
                                                        if (responseObject.optString("msg") != null) {
                                                            if (responseObject.getString("msg").equals("Someone is login from your device.", true)
                                                                    || responseObject.getString("msg").equals("Your account has been disabled. Contact Administrator", true)) {
                                                                Toast.showLongToast(cContext, "" + responseObject.getString("msg"))
                                                            }
                                                        }
                                                    } catch (e: Exception) {
                                                        Logg.e(TAG, "" + e.message)
                                                    }
                                                }
                                            } else {
                                                val retrofitResponse = aActivity as RetroFitResponse
                                                retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                                res = response
                                            }
                                        } else {
                                            val retrofitResponse = aActivity as RetroFitResponse
                                            retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                            res = response
                                        }
                                    } else {
                                        val retrofitResponse = aActivity as RetroFitResponse
                                        retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                        res = response
                                    }
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            } else if (response!!.errorBody() != null) {
                                try {
                                    //                                    Logg.e(TAG, "response errorBody --->> " + response.errorBody());
                                    val retrofitResponse = aActivity as RetroFitResponse
//                                    val errorResponse = response.errorBody().toString()
                                    val errorResponse = response!!.errorBody()!!.string().toString()

                                    //                                    Logg.e(TAG, "response errorBody in String --->> " + errorResponse);
                                    retrofitResponse.onRetrofitResponse(RESPONSE_OK, errorResponse, methodName)
                                    res = response

                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            } else {
                                Logg.d(TAG, "response --->> Null Response")
                                val retrofitResponse = aActivity as RetroFitResponse
                                retrofitResponse.onRetrofitResponse(RESPONSE_ERROR, response.body().toString(), methodName)
                                res = null
                            }

                        } else {
                            Logg.d(TAG, "response --->> Null Response")
                            val retrofitResponse = aActivity as RetroFitResponse
                            retrofitResponse.onRetrofitResponse(RESPONSE_ERROR, response.body().toString(), methodName)
                            res = null
                        }
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()
                        }
                        Logg.d("response", "<<onFailure --> " + t.message)
                        val retrofitResponse = aActivity as RetroFitResponse
                        retrofitResponse.onRetrofitError(RESPONSE_ERROR, t.message.toString(), methodName)
                    }
                })
            }
        }
    }

    fun callUpdateProfile(relativeURL: String?, cContext: Context,
                          methodName: String, aActivity: RetroFitResponse, isProgressEnable: Boolean,
                          isCancelable: Boolean, canCancelOther: Boolean, methodType: String, profileImage: File?,
                          firstNameFull: String, lastNameFull: String, dateOfBirthday: String, emailId: String, isDeletePhoto: String,
                          userId: String) {
        Logg.e("method Name", "--" + methodName)
        if (!APIUtils.isOnline(cContext)) {
            Toast.show(cContext, PleaseCheckInternetConnectionEng)
        } else {
            if (cm == null) {
                cm = CustomProgressBar(cContext)
                cm!!.setCancelable(false)
            }
            if (isProgressEnable) {
                try {
                    if (!(cContext as Activity).isFinishing) {
                        cm!!.show()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            if (relativeURL != null) {

//                Logg.e(TAG, "API_TOKEN ==> " + AlcoholApplication.cookieListInString)
                val service = RetroClient.client
                //                Logg.e(TAG, "Request --->> " + reqHashMap);
                Logg.e(TAG, "Relative URL --->> " + relativeURL)

                var body: MultipartBody.Part? = null
                if (profileImage != null) {
                    val reqFile = RequestBody.create(MediaType.parse("image/*"), profileImage)
                    body = MultipartBody.Part.createFormData("profile_image", profileImage.name, reqFile)
                }

                val firstName = RequestBody.create(MediaType.parse("text/plain"), firstNameFull)
                val lastName = RequestBody.create(MediaType.parse("text/plain"), lastNameFull)
                val date_of_birth = RequestBody.create(MediaType.parse("text/plain"), dateOfBirthday)
                val is_delete_photo = RequestBody.create(MediaType.parse("text/plain"), isDeletePhoto)
                val email = RequestBody.create(MediaType.parse("text/plain"), emailId)
                val id = RequestBody.create(MediaType.parse("text/plain"), userId)

                Logg.e(TAG, "uuid ===> " + Utils.getDeviceUUID(activity))
                val deviceUUD = RequestBody.create(MediaType.parse("text/plain"), Utils.getDeviceUUID(activity))

                var call: Call<String>? = null
                if (methodType == POST) {
                    if (service != null) {
                        call = service.callWebserviceUploadFile(Prefs.getString(PrefsKey.AUTHORIZATION, "Basic YWRtaW46MTIzNA==", activity),
                                relativeURL, body, first_name = firstName, last_name = lastName, email = email , is_delete_photo = is_delete_photo, id = id,
                                date_of_birth = date_of_birth)
                    }
                }

                //                else if (methodType.equals(GET)) {
                //                    call = service.callWebserviceGet(AlcoholApplication.cookieListInString, relativeURL, reqHashMap);
                //                }

                assert(call != null)

                call!!.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>?) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()

                        }
                        Logg.e(TAG, "response code --->> " + response!!.code())
                        if (response != null) {

                            try {
                                if (response!!.errorBody() != null) {
                                    Logg.e(TAG, "response errorBody --->> " + response!!.errorBody()!!.string())
                                    //                                        Utils.writeFile(response.errorBody().string(), methodName +"_Response.txt");
                                }
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                            var responseObject = JSONObject(response!!.body().toString())

                            if (responseObject != null) {
                                if (responseObject.has("status_code")) {
//                                    if (responseObject.getInt("status_code") == 401) {
                                    if (responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE || responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE_NEW) {
                                        // call Logout api
//                                        val dataMap = HashMap<String, String>()
//                                        dataMap.put(UtilsKey.LogoutRequest.USER_ID, Prefs.getString(Constants.SP_CUSTOMER_ID, "", activity!!).toString())
//                                        dataMap.put(UtilsKey.LogoutRequest.PANEL, "app")
//                                        dataMap.put(UtilsKey.LogoutRequest.CUSTOMER_ID, Prefs.getString(SP_CUSTOMER_ID, "", context).toString())
//                                        callWB(Constants.RELATIVE_URL_LOOUT, activity!!, dataMap, Constants.W_LOGOUT_MANUAL, aActivity, false, false, true, POST)
//                                        activity.sendBroadcast(Intent().setAction(ACTION_FINISH_ACT))

                                        if (!(methodName.equals(W_LOGOUT, true))) {
                                            try {
                                                if (responseObject.optString("msg") != null) {
                                                    if (responseObject.getString("msg").equals("Someone is login from your device.", true)
                                                            || responseObject.getString("msg").equals("Your account has been disabled. Contact Administrator", true)) {
                                                        Toast.showLongToast(cContext, "" + responseObject.getString("msg"))
                                                    }
                                                }
                                            } catch (e: Exception) {
                                                Logg.e(TAG, "" + e.message)
                                            }
                                        }
                                    } else {
                                        val retrofitResponse = aActivity as RetroFitResponse
                                        retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                        res = response
                                    }
                                } else {
                                    val retrofitResponse = aActivity as RetroFitResponse
                                    retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                    res = response
                                }
                            } else {
                                val retrofitResponse = aActivity as RetroFitResponse
                                retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                res = response
                            }
                        } else {
                            Logg.d(TAG, "response --->> Null Response")
                            val retrofitResponse = aActivity as RetroFitResponse
                            retrofitResponse.onRetrofitResponse(RESPONSE_ERROR, response.body().toString(), methodName)
                            res = null
                        }
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()
                        }
                        Logg.d("response", "<<onFailure --> " + t.message)
                        val retrofitResponse = aActivity as RetroFitResponse
                        retrofitResponse.onRetrofitError(RESPONSE_ERROR, t.message!!, methodName)
                    }
                })
            }
        }
    }

    fun callUpdateProfileParent(relativeURL: String?, cContext: Context,
                          methodName: String, aActivity: RetroFitResponse, isProgressEnable: Boolean,
                          isCancelable: Boolean, canCancelOther: Boolean, methodType: String, profileImage: File?,
                          firstNameFull: String, occupationFull: String, educationFull: String, phoneNumber: String,
                          userId: String) {
        Logg.e("method Name", "--" + methodName)
        if (!APIUtils.isOnline(cContext)) {
            Toast.show(cContext, PleaseCheckInternetConnectionEng)
        } else {
            if (cm == null) {
                cm = CustomProgressBar(cContext)
                cm!!.setCancelable(false)
            }
            if (isProgressEnable) {
                try {
                    if (!(cContext as Activity).isFinishing) {
                        cm!!.show()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            if (relativeURL != null) {

//                Logg.e(TAG, "API_TOKEN ==> " + AlcoholApplication.cookieListInString)
                val service = RetroClient.client
                //                Logg.e(TAG, "Request --->> " + reqHashMap);
                Logg.e(TAG, "Relative URL --->> " + relativeURL)

                var body: MultipartBody.Part? = null
                if (profileImage != null) {
                    val reqFile = RequestBody.create(MediaType.parse("image/*"), profileImage)
                    body = MultipartBody.Part.createFormData("profile_image", profileImage.name, reqFile)
                }

                val firstName = RequestBody.create(MediaType.parse("text/plain"), firstNameFull)
                val occupation = RequestBody.create(MediaType.parse("text/plain"), occupationFull)
                val education = RequestBody.create(MediaType.parse("text/plain"), educationFull)
                val phone = RequestBody.create(MediaType.parse("text/plain"), phoneNumber)
                val id = RequestBody.create(MediaType.parse("text/plain"), userId)

                Logg.e(TAG, "uuid ===> " + Utils.getDeviceUUID(activity))
                val deviceUUD = RequestBody.create(MediaType.parse("text/plain"), Utils.getDeviceUUID(activity))

                var call: Call<String>? = null
                if (methodType == POST) {
                    if (service != null) {
                        call = service.callWebserviceUploadFileParent(Prefs.getString(PrefsKey.AUTHORIZATION, "Basic YWRtaW46MTIzNA==", activity),
                                relativeURL, body, father_full_name = firstName, father_occupation = occupation, father_phone_number = phone , id = id,
                                father_education = education)
                    }
                }

                //                else if (methodType.equals(GET)) {
                //                    call = service.callWebserviceGet(AlcoholApplication.cookieListInString, relativeURL, reqHashMap);
                //                }

                assert(call != null)

                call!!.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>?) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()

                        }
                        Logg.e(TAG, "response code --->> " + response!!.code())
                        if (response != null) {

                            try {
                                if (response!!.errorBody() != null) {
                                    Logg.e(TAG, "response errorBody --->> " + response!!.errorBody()!!.string())
                                    //                                        Utils.writeFile(response.errorBody().string(), methodName +"_Response.txt");
                                }
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                            var responseObject = JSONObject(response!!.body().toString())

                            if (responseObject != null) {
                                if (responseObject.has("status_code")) {
//                                    if (responseObject.getInt("status_code") == 401) {
                                    if (responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE || responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE_NEW) {
                                        // call Logout api
//                                        val dataMap = HashMap<String, String>()
//                                        dataMap.put(UtilsKey.LogoutRequest.USER_ID, Prefs.getString(Constants.SP_CUSTOMER_ID, "", activity!!).toString())
//                                        dataMap.put(UtilsKey.LogoutRequest.PANEL, "app")
//                                        dataMap.put(UtilsKey.LogoutRequest.CUSTOMER_ID, Prefs.getString(SP_CUSTOMER_ID, "", context).toString())
//                                        callWB(Constants.RELATIVE_URL_LOOUT, activity!!, dataMap, Constants.W_LOGOUT_MANUAL, aActivity, false, false, true, POST)
//                                        activity.sendBroadcast(Intent().setAction(ACTION_FINISH_ACT))

                                        if (!(methodName.equals(W_LOGOUT, true))) {
                                            try {
                                                if (responseObject.optString("msg") != null) {
                                                    if (responseObject.getString("msg").equals("Someone is login from your device.", true)
                                                            || responseObject.getString("msg").equals("Your account has been disabled. Contact Administrator", true)) {
                                                        Toast.showLongToast(cContext, "" + responseObject.getString("msg"))
                                                    }
                                                }
                                            } catch (e: Exception) {
                                                Logg.e(TAG, "" + e.message)
                                            }
                                        }
                                    } else {
                                        val retrofitResponse = aActivity as RetroFitResponse
                                        retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                        res = response
                                    }
                                } else {
                                    val retrofitResponse = aActivity as RetroFitResponse
                                    retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                    res = response
                                }
                            } else {
                                val retrofitResponse = aActivity as RetroFitResponse
                                retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                res = response
                            }
                        } else {
                            Logg.d(TAG, "response --->> Null Response")
                            val retrofitResponse = aActivity as RetroFitResponse
                            retrofitResponse.onRetrofitResponse(RESPONSE_ERROR, response.body().toString(), methodName)
                            res = null
                        }
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()
                        }
                        Logg.d("response", "<<onFailure --> " + t.message)
                        val retrofitResponse = aActivity as RetroFitResponse
                        retrofitResponse.onRetrofitError(RESPONSE_ERROR, t.message!!, methodName)
                    }
                })
            }
        }
    }


    fun callUpdateFileForAnswer(relativeURL: String?, cContext: Context,
                          methodName: String, aActivity: RetroFitResponse, isProgressEnable: Boolean,
                          isCancelable: Boolean, canCancelOther: Boolean, methodType: String, profileImage: File?,
                          userId: String, homeWorkId: String, questionId: String, answerString: String) {
        Logg.e("method Name", "--" + methodName)
        if (!APIUtils.isOnline(cContext)) {
            Toast.show(cContext, PleaseCheckInternetConnectionEng)
        } else {
            if (cm == null) {
                cm = CustomProgressBar(cContext)
                cm!!.setCancelable(false)
            }
            if (isProgressEnable) {
                try {
                    if (!(cContext as Activity).isFinishing) {
                        cm!!.show()
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            if (relativeURL != null) {

                val service = RetroClient.client
                Logg.e(TAG, "Relative URL --->> $relativeURL")

                var body: MultipartBody.Part? = null
                if (profileImage != null) {
                    val reqFile = RequestBody.create(MediaType.parse("*/*"), profileImage)
                    body = MultipartBody.Part.createFormData("answer_file", profileImage.name, reqFile)
                }

                val home_work_id = RequestBody.create(MediaType.parse("text/plain"), homeWorkId)
                val question_id = RequestBody.create(MediaType.parse("text/plain"), questionId)
                val answer = RequestBody.create(MediaType.parse("text/plain"), answerString)
                val user_id = RequestBody.create(MediaType.parse("text/plain"), userId)
                val device_udid = RequestBody.create(MediaType.parse("text/plain"), Utils.getDeviceUUID(activity))

                Logg.e(TAG, "uuid ===> " + Utils.getDeviceUUID(activity))

                var call: Call<String>? = null
                if (methodType == POST) {
                    if (service != null) {
                        call = service.callWebserviceUploadFileForAnswer(Prefs.getString(PrefsKey.AUTHORIZATION, "Basic YWRtaW46MTIzNA==", activity),
                                relativeURL, body, home_work_id, question_id, answer, user_id, device_udid)
                    }
                }

                assert(call != null)

                call!!.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>?) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()

                        }
                        Logg.e(TAG, "response code --->> " + response!!.code())
                        if (response != null) {

                            try {
                                if (response!!.errorBody() != null) {
                                    Logg.e(TAG, "response errorBody --->> " + response!!.errorBody()!!.string())
                                    //                                        Utils.writeFile(response.errorBody().string(), methodName +"_Response.txt");
                                }
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                            var responseObject = JSONObject(response!!.body().toString())

                            if (responseObject != null) {
                                if (responseObject.has("status_code")) {
//                                    if (responseObject.getInt("status_code") == 401) {
                                    if (responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE || responseObject.getInt("status_code") == RESPONSE_TOKEN_EXPIRE_NEW) {
                                        // call Logout api
//                                        val dataMap = HashMap<String, String>()
//                                        dataMap.put(UtilsKey.LogoutRequest.USER_ID, Prefs.getString(Constants.SP_CUSTOMER_ID, "", activity!!).toString())
//                                        dataMap.put(UtilsKey.LogoutRequest.PANEL, "app")
//                                        dataMap.put(UtilsKey.LogoutRequest.CUSTOMER_ID, Prefs.getString(SP_CUSTOMER_ID, "", context).toString())
//                                        callWB(Constants.RELATIVE_URL_LOOUT, activity!!, dataMap, Constants.W_LOGOUT_MANUAL, aActivity, false, false, true, POST)
//                                        activity.sendBroadcast(Intent().setAction(ACTION_FINISH_ACT))

                                        if (!(methodName.equals(W_LOGOUT, true))) {
                                            try {
                                                if (responseObject.optString("msg") != null) {
                                                    if (responseObject.getString("msg").equals("Someone is login from your device.", true)
                                                            || responseObject.getString("msg").equals("Your account has been disabled. Contact Administrator", true)) {
                                                        Toast.showLongToast(cContext, "" + responseObject.getString("msg"))
                                                    }
                                                }
                                            } catch (e: Exception) {
                                                Logg.e(TAG, "" + e.message)
                                            }
                                        }
                                    } else {
                                        val retrofitResponse = aActivity as RetroFitResponse
                                        retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                        res = response
                                    }
                                } else {
                                    val retrofitResponse = aActivity as RetroFitResponse
                                    retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                    res = response
                                }
                            } else {
                                val retrofitResponse = aActivity as RetroFitResponse
                                retrofitResponse.onRetrofitResponse(RESPONSE_OK, response.body().toString(), methodName)
                                res = response
                            }
                        } else {
                            Logg.d(TAG, "response --->> Null Response")
                            val retrofitResponse = aActivity as RetroFitResponse
                            retrofitResponse.onRetrofitResponse(RESPONSE_ERROR, response.body().toString(), methodName)
                            res = null
                        }
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        if (canCancelOther) {
                            if (cm!!.isShowing() && !(cContext as Activity).isFinishing)
                                cm!!.dismiss()
                        }
                        Logg.d("response", "<<onFailure --> " + t.message)
                        val retrofitResponse = aActivity as RetroFitResponse
                        retrofitResponse.onRetrofitError(RESPONSE_ERROR, t.message!!, methodName)
                    }
                })
            }
        }
    }

    companion object {
        val RESPONSE_OK = 200
        val RESPONSE_ERROR = 404
        val RESPONSE_TOKEN_EXPIRE = 401
        val RESPONSE_ERROR_NEW = 400
        val RESPONSE_TOKEN_EXPIRE_NEW = 500
    }

}