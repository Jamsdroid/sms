package com.sms.webservice

import android.content.Context
import android.net.ConnectivityManager

class APIUtils {

    companion object {

        fun isOnline(cContext: Context): Boolean {
            try {
                val cm = cContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val netInfo = cm.activeNetworkInfo
                if (netInfo != null && netInfo.isConnectedOrConnecting) {
                    return true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return false
        }
    }


}
