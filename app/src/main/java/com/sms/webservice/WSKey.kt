package com.sms.webservice

class WSKey {
    companion object {

        const val EMAIL = "email"
        const val PASSWORD = "password"
        const val DEVICE_TOKEN: String = "device_token"
        const val DEVICE_TYPE: String = "device_type"

        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val DATE_OF_BIRTH = "date_of_birth"
        const val IS_DELETE_PHOTO = "is_delete_photo"
        const val PROFILE_IMAGE = "profile_image"
        const val ID = "id"

        const val OLD_PASSWORD = "old_password"
        const val NEW_PASSWORD = "new_password"

        const val USER_ID = "user_id"
        const val PAGE_NO = "page_no" // Pagination start from 0
        const val CLASS_ID = "class_id" // Optional  "2"
        const val SUBJECT_ID = "subject_id"
        const val SUBJECT_TYPE = "subject_type" // 0-Standard Wise,1-Individual
        const val SEARCH_TEXT = "search_text" // Optional

        const val ADVERTISEMENT_ID = "advertisement_id" // Optional

        const val HOME_WORK_ID = "home_work_id"
        const val QUESTION_ID = "question_id"
        const val ANSWER = "answer"
        const val DESCRIPTION = "description"
        const val ANSWER_FILE = "answer_file"

        const val COURSE_ID = "course_id"
        const val COURSE_VERSION_ID = "course_version_id"
        const val PRICE = "price"

        const val CART_ID = "cart_id"
        const val TRANSACTION_ID = "transaction_id"
        const val TOTAL_AMOUNT = "total_amount"
        const val EXAM_ID = "exam_id"

        const val JSON_DATA = "json_data"
        const val ANSWER_STATUS = "answer_status"

        const val QUESTION = "question"
        const val TAB_TYPE = "tab_type"

        const val NOTIFICATION_ID = "notification_id"
        const val REPLY_MESSAGE = "reply_message"

        const val FATHER_FULL_NAME = "father_full_name"
        const val FATHER_OCCUPATION = "father_occupation"
        const val FATHER_EDUCATION = "father_education"
        const val FATHER_PHONE_NO = "father_phone_no"

        const val PARENT_ID = "parent_id"

    }
}