package com.sms.webservice

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface RetroAPIInterface {

    @Headers("Content-Type:application/x-www-form-urlencoded", "Cache-Control:no-cache")
    @FormUrlEncoded
    @POST
    fun callWebservice(@Header("Authorization") headers: String, @Url url: String, @FieldMap fields: Map<String, String>): Call<String>

    @FormUrlEncoded
    @POST
    fun callWebserviceWithoutHeader(@Url url: String, @FieldMap fields: Map<String, String>): Call<String>

    @GET
    fun callWebserviceGet(@Header("Authorization") headers: String, @Url url: String, @QueryMap params: Map<String, String>): Call<String>

    @Multipart
    @POST
    fun callWebserviceUploadFile(@Header("Authorization") headers: String?,
                                 @Url url: String?,
                                 @Part profile_image: MultipartBody.Part?,
                                 @Part(WSKey.FIRST_NAME) first_name: RequestBody?,
                                 @Part(WSKey.LAST_NAME) last_name: RequestBody?,
                                 @Part(WSKey.EMAIL) email: RequestBody?,
                                 @Part(WSKey.IS_DELETE_PHOTO) is_delete_photo: RequestBody?,
                                 @Part(WSKey.ID) id: RequestBody?,
                                 @Part(WSKey.DATE_OF_BIRTH) date_of_birth: RequestBody): Call<String>?

    @Multipart
    @POST
    fun callWebserviceUploadFileParent(@Header("Authorization") headers: String?,
                                 @Url url: String?,
                                 @Part profile_image: MultipartBody.Part?,
                                 @Part(WSKey.FATHER_FULL_NAME) father_full_name: RequestBody?,
                                 @Part(WSKey.FATHER_OCCUPATION) father_occupation: RequestBody?,
                                 @Part(WSKey.FATHER_PHONE_NO) father_phone_number: RequestBody?,
                                 @Part(WSKey.ID) id: RequestBody?,
                                 @Part(WSKey.FATHER_EDUCATION) father_education: RequestBody): Call<String>?


    @Multipart
    @POST
    fun callWebserviceUploadFileForAnswer(@Header("Authorization") headers: String?,
                                          @Url url: String?,
                                          @Part profile_image: MultipartBody.Part?,
                                          @Part(WSKey.HOME_WORK_ID) first_name: RequestBody?,
                                          @Part(WSKey.QUESTION_ID) last_name: RequestBody?,
                                          @Part(WSKey.ANSWER) email: RequestBody?,
                                          @Part(WSKey.USER_ID) id: RequestBody?,
                                          @Part("device_udid") device_udid: RequestBody?): Call<String>?
//
//    @Multipart
//    @POST
//    fun callWebserviceUploadMultipleFiles(@Header("Authorization") headers: String, @Url url: String, @Part files: List<MultipartBody.Part>,
//                                          @Part(UtilsKey.ServiceRequestFormRequest.CUSTOMER_ID) customer_id: RequestBody, @Part(UtilsKey.ServiceRequestFormRequest.SUBJECT) subject: RequestBody,
//                                          @Part(UtilsKey.ServiceRequestFormRequest.DATE) date: RequestBody, @Part(UtilsKey.ServiceRequestFormRequest.TIME) time: RequestBody,
//                                          @Part(UtilsKey.ServiceRequestFormRequest.DESCRIPTION) description: RequestBody, @Part(UtilsKey.ServiceRequestFormRequest.MACHINE_ID) selectedMachineId: RequestBody): Call<String>

}