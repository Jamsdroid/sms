package com.sms.webservice

import com.sms.common.Constants
import com.sms.common.Constants.Companion.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class RetroClient : Constants {
    companion object {

        private var retrofit: Retrofit? = null
        private var retroAPIInterface: RetroAPIInterface? = null

        //                                Logg.e(TAG, "reuqest headers ===> \n" + original.headers().toString());
        //                            .addConverterFactory(ScalarsConverterFactory.create())
        //                            .addConverterFactory(GsonConverterFactory.create())
        val client: RetroAPIInterface?
            get() {
                try {
                    if (retroAPIInterface == null) {
                        val logging = HttpLoggingInterceptor()
                        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

                        val httpClient = OkHttpClient.Builder()
                        httpClient.addInterceptor(logging)

                        val okHttpClient = OkHttpClient()
                        val clientWith30sTimeout = okHttpClient.newBuilder()
                                .addInterceptor { chain ->
                                    val original = chain.request()
                                    val builder = original.newBuilder()
                                    builder.header("Content-Type", "application/x-www-form-urlencoded")
                                    builder.method(original.method(), original.body())
                                    builder.build()
                                    chain.proceed(original)
                                }
                                .readTimeout(30, TimeUnit.SECONDS)
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .build()

                        if (retrofit == null) {
                            retrofit = Retrofit.Builder()
                                    .baseUrl(BASE_URL)
                                    .client(clientWith30sTimeout)
                                    .addConverterFactory(ToStringConverter())
                                    .build()
                        }
                        retroAPIInterface = retrofit!!.create<RetroAPIInterface>(RetroAPIInterface::class.java!!)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

                return retroAPIInterface
            }
    }
}