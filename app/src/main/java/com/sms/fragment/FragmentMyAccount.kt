package com.sms.fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import com.alcohol.core.BaseFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.activity.*
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_LOGOUT
import com.sms.common.Constants.Companion.W_USER_DETAILS
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.include_home_header.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FragmentMyAccount.OnMyProfileFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentMyAccount.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentMyAccount : BaseFragment() {

    private lateinit var mainActivity: MainActivity

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnMyProfileFragmentInteractionListener? = null
    private var deviceWidth = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_my_profile, container, false)

        mainActivity = activity as MainActivity

//        mainActivity!!.tvHeaderTitle.text = resources.getString(R.string.my_courses)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivity!!.llRight.visibility = View.VISIBLE
        mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
        mainActivity!!.llSearchHome.visibility = View.GONE
        mainActivity!!.llRight.visibility = View.GONE

        mainActivity!!.tvHeaderTitle.text = mainActivity.getString(R.string.title_my_profile)

        val display = mainActivity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        deviceWidth = size.x

        if (Prefs.getString(PrefsKey.CLASS_ID, "", mainActivity).isNullOrEmpty() || Prefs.getString(PrefsKey.CLASS_ID, "", mainActivity) == "0"){
            viewLineHomeWork.visibility = View.GONE
            rlHomeWork.visibility = View.GONE
        } else {
            viewLineHomeWork.visibility = View.VISIBLE
            rlHomeWork.visibility = View.VISIBLE
        }

        val params = ivProfileImageMyProfile.layoutParams as RelativeLayout.LayoutParams
        params.height = (deviceWidth*0.50).toInt()
        ivProfileImageMyProfile.layoutParams = params

        tvLogout.setOnClickListener {
            val dialog = Dialog(mainActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_custom)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val tvOne = dialog.findViewById(R.id.tvOne) as TextView
            val txtClose = dialog.findViewById(R.id.tvClose) as TextView
            val txtDone = dialog.findViewById(R.id.tvDone) as TextView

            tvOne.text = getString(R.string.message_logout)

            txtClose.setOnClickListener { dialog.dismiss() }

            txtDone.setOnClickListener {
                dialog.dismiss()
                if (APIUtils.isOnline(activity!!)) {

                    Prefs.clearAll(mainActivity)
                    val intentLogin = Intent(mainActivity, LoginActivity::class.java)
                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intentLogin)
                    mainActivity.finish()

//                    showProgressDialog(mainActivity)
//                    val logoutURL = GET_LOGOUT + "" + Prefs.getString(PrefsKey.ID, "", mainActivity)
//                    val dataMap = HashMap<String, String>()
//                    apiHelper!!.callWB(logoutURL, activity!!, dataMap, Constants.W_LOGOUT, mainActivity, false, false, true, Constants.GET)

                } else {
                    Toast.show(context, Constants.PleaseCheckInternetConnectionEng)
                }
            }
            dialog.show()
        }

        rlHomeWork.setOnClickListener {
            var intentHomeWork = Intent(mainActivity, HomeWorkListActivity::class.java)
            mainActivity.startActivity(intentHomeWork)

//            getProfileDataAPI()

        }

        rlMyProfile.setOnClickListener {
            var intentMyProfile = Intent(mainActivity, MyProfileActivity::class.java)
            mainActivity.startActivity(intentMyProfile)
        }

        rlQuestionAnswers.setOnClickListener {
            var intentQA = Intent(mainActivity, QAActivity::class.java)
            mainActivity.startActivity(intentQA)
        }

        rlMore.setOnClickListener {
            var intentMore = Intent(mainActivity, MoreActivity::class.java)
            mainActivity.startActivity(intentMore)
        }

    }

//    private fun getProfileDataAPI() {
//
//        if (APIUtils.isOnline(mainActivity)) {
//            showProgressDialog(mainActivity)
//            val dataMap = HashMap<String, String>()
//            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", mainActivity)
//             apiHelper!!.callWB(Constants.POST_URL_USER_DETAILS, mainActivity, dataMap, Constants.W_USER_DETAILS, this, false, false, true, Constants.POST)
//        } else {
//            Toast.show(mainActivity, Constants.PleaseCheckInternetConnectionEng)
//        }
//
//    }

    override fun onResume() {
        super.onResume()

        if (!Prefs.getString(PrefsKey.PROFILE_IMAGE, "", mainActivity).isNullOrBlank()) {
            Glide.with(mainActivity)
                    .load(Prefs.getString(PrefsKey.PROFILE_IMAGE, "", mainActivity))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }
                    })
                    .into(ivProfileImageMyProfile)
        }

        if (!Prefs.getString(PrefsKey.FIRST_NAME, "", mainActivity).isNullOrBlank()
                && !Prefs.getString(PrefsKey.LAST_NAME, "", mainActivity).isNullOrBlank()) {
            tvUserName.text = Prefs.getString(PrefsKey.FIRST_NAME, "", mainActivity) + " " +
                    Prefs.getString(PrefsKey.LAST_NAME, "", mainActivity)

        } else if (!Prefs.getString(PrefsKey.FIRST_NAME, "", mainActivity).isNullOrBlank()) {
            tvUserName.text = Prefs.getString(PrefsKey.FIRST_NAME, "", mainActivity)

        } else if (!Prefs.getString(PrefsKey.LAST_NAME, "", mainActivity).isNullOrBlank()) {
            tvUserName.text = Prefs.getString(PrefsKey.LAST_NAME, "", mainActivity)
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_USER_DETAILS -> {
//                val gson = Gson()
//                val advertiseResponse = gson.fromJson<AdvertiseResponse>(mRes.toString(), AdvertiseResponse::class.java!!)
            }
            W_LOGOUT -> {
//                Prefs.clearAll(mainActivity)
//                val intentLogin = Intent(mainActivity, LoginActivity::class.java)
//                intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intentLogin)
//                mainActivity.finish()
            }
        }


    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(mainActivity, getString(R.string.meesage_connection_timeout))
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onMyProfileFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMyProfileFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMyProfileFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnMyProfileFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onMyProfileFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentMyAccount.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentMyAccount().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }

        val TAG: String = FragmentMyAccount::class.java.simpleName
        fun newInstance() = FragmentMyAccount()
    }
}
