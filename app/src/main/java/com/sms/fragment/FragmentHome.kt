package com.sms.fragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.alcohol.core.BaseFragment
import com.google.gson.Gson
import com.sms.R
import com.sms.activity.FilterCourseActivity
import com.sms.activity.MainActivity
import com.sms.adapters.AdvertisePagerAdapter
import com.sms.adapters.CoursesAdapter
import com.sms.common.AppConstants.Companion.SUBJECT_TYPE_INDIVIDUAL_VALUE
import com.sms.common.AppConstants.Companion.SUBJECT_TYPE_STANDARD_VALUE
import com.sms.common.Constants
import com.sms.common.Constants.Companion.PleaseCheckInternetConnectionEng
import com.sms.common.Constants.Companion.VIEW_TYPE_ITEM
import com.sms.common.Constants.Companion.VIEW_TYPE_LOADING
import com.sms.common.Constants.Companion.W_ADVERTISEMENT_LIST
import com.sms.common.Constants.Companion.W_CART_LIST
import com.sms.common.Constants.Companion.W_HOME_DASHBOARD
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.common.Utils
import com.sms.loadmore.OnLoadMoreListener
import com.sms.loadmore.RecyclerViewLoadMoreScroll
import com.sms.model.home.AdvertiseResponse
import com.sms.model.home.CartCountResponse
import com.sms.model.home.CourseDetailsItem
import com.sms.model.home.DashboardResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.RetroFitResponse
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.include_home_header.*
import java.util.*
import kotlin.collections.ArrayList


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FragmentHome.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentHome.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentHome : BaseFragment(), Constants, RetroFitResponse {

    private lateinit var mainActivity: MainActivity

    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnHomeFragmentInteractionListener? = null
    var scrollListenerGrid: RecyclerViewLoadMoreScroll? = null
    var gridLayoutManager: GridLayoutManager? = null

    var coursesAdapter: CoursesAdapter? = null

    var pageIndex: Int = 0
    var totalPage: Int = 0

    var isSearched: Boolean = false

    var subjectTypeValue = SUBJECT_TYPE_STANDARD_VALUE
    var userDetailsList = ArrayList<CourseDetailsItem>()
    var searchedWord: String = ""
    var isRefreshing = false
    lateinit var timer: Timer

    private var currentPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e(TAG, "<=== onCreate() ===>")
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onResume() {
        super.onResume()

        callGetCartCount()

        if (::timer.isInitialized) {
            setupAutoPager()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.e(TAG, "<=== onCreateView() ===>")

        val view = inflater?.inflate(R.layout.fragment_home, container, false)

        // Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.e(TAG, "<=== onViewCreated() ===>")

        val rcvStandardCourses = view.findViewById(R.id.rcvStandardCourses) as RecyclerView
//        val rcvAdvertisement = view.findViewById(R.id.rcvAdvertisement) as RecyclerView

        mainActivity = activity as MainActivity

        if (userDetailsList != null && userDetailsList.size > 0) {
            userDetailsList.clear()
        }

        mainActivity!!.llRight.visibility = View.VISIBLE
        mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
        mainActivity!!.llSearchHome.visibility = View.GONE
        mainActivity!!.edtSearchProductHome.text.clear()

        mainActivity!!.tvHeaderTitle.text = mainActivity.getString(R.string.title_home)

        searchedWord = ""

//        mainActivity!!.tvHeaderTitle.text = resources.getString(R.string.title_home)

        rcvStandardCourses?.layoutManager = GridLayoutManager(mainActivity, 2)
//        rcvAdvertisement?.layoutManager = LinearLayoutManager(mainActivity, LinearLayoutManager.HORIZONTAL, true)

        tvStandardCourses.setOnClickListener {

            tvStandardCourses.run {
                setTextColor(ContextCompat.getColor(mainActivity, R.color.colorAppBlue))
                setBackgroundResource(R.color.colorLightBlue)
            }

            tvIndividualCourses.run {
                setTextColor(ContextCompat.getColor(mainActivity, R.color.colorAppBlack))
                setBackgroundResource(R.color.colorWhite)
            }

            mainActivity!!.llRight.visibility = View.VISIBLE
            mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
            mainActivity!!.llSearchHome.visibility = View.GONE
            mainActivity!!.edtSearchProductHome.text.clear()

            searchedWord = ""

            Utils.hideKeyboard(mainActivity!!)

            if (userDetailsList != null && userDetailsList.size > 0) {
                userDetailsList.clear()
            }

            pageIndex = 0
            totalPage = 0

            subjectTypeValue = SUBJECT_TYPE_STANDARD_VALUE

            callGetStandardCoursesWS(subjectTypeValue, pageIndex, "", true)
        }

        tvIndividualCourses.setOnClickListener {

            tvIndividualCourses.run {
                setTextColor(ContextCompat.getColor(mainActivity, R.color.colorAppBlue))
                setBackgroundResource(R.color.colorLightBlue)
            }

            tvStandardCourses.run {
                setTextColor(ContextCompat.getColor(mainActivity, R.color.colorAppBlack))
                setBackgroundResource(R.color.colorWhite)
            }

            mainActivity!!.llRight.visibility = View.VISIBLE
            mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
            mainActivity!!.llSearchHome.visibility = View.GONE
            mainActivity!!.edtSearchProductHome.text.clear()

            searchedWord = ""

            Utils.hideKeyboard(mainActivity!!)

            if (userDetailsList != null && userDetailsList.size > 0) {
                userDetailsList.clear()
            }

            pageIndex = 0
            totalPage = 0

            subjectTypeValue = SUBJECT_TYPE_INDIVIDUAL_VALUE

            callGetStandardCoursesWS(subjectTypeValue, pageIndex, "", true)
        }

        swipeRefresh.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            // Your code to refresh the list here.
            // Make sure you call swipeContainer.setRefreshing(false)
            // once the network request has completed successfully.

            mainActivity!!.llRight.visibility = View.VISIBLE
            mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
            mainActivity!!.llSearchHome.visibility = View.GONE
            mainActivity!!.edtSearchProductHome.text.clear()

            isRefreshing = true

            if (subjectTypeValue == SUBJECT_TYPE_STANDARD_VALUE) {

                searchedWord = ""

                Utils.hideKeyboard(mainActivity!!)

//                if (userDetailsList != null && userDetailsList.size > 0) {
//                    userDetailsList.clear()
//                }

                pageIndex = 0
                totalPage = 0

                subjectTypeValue = SUBJECT_TYPE_STANDARD_VALUE

                callGetStandardCoursesWS(subjectTypeValue, pageIndex, "", false)

            } else if (subjectTypeValue == SUBJECT_TYPE_INDIVIDUAL_VALUE) {

                searchedWord = ""

                Utils.hideKeyboard(mainActivity!!)

//                if (userDetailsList != null && userDetailsList.size > 0) {
//                    userDetailsList.clear()
//                }

                pageIndex = 0
                totalPage = 0

                subjectTypeValue = SUBJECT_TYPE_INDIVIDUAL_VALUE

                callGetStandardCoursesWS(subjectTypeValue, pageIndex, "", false)
            }


        })

        // Configure the refreshing colors
        swipeRefresh.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light)

        init()
    }

    private fun init() {


        mainActivity!!.tvSearch.setOnClickListener {
            mainActivity!!.llRight.visibility = View.GONE
            mainActivity!!.tvHeaderTitle.visibility = View.GONE
            mainActivity!!.llSearchHome.visibility = View.VISIBLE

            mainActivity!!.edtSearchProductHome.requestFocus()
            Utils.showKeyboard(mainActivity!!, mainActivity!!.edtSearchProductHome)
        }

        gridLayoutManager = GridLayoutManager(mainActivity, 2)

        scrollListenerGrid = RecyclerViewLoadMoreScroll(gridLayoutManager!!)
        scrollListenerGrid!!.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreDataGrid()
            }
        })

        mainActivity!!.edtSearchProductHome.setOnEditorActionListener() { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                if (userDetailsList != null && userDetailsList.size > 0) {
                    userDetailsList.clear()
                }

                pageIndex = 0
                totalPage = 0
                searchedWord = mainActivity!!.edtSearchProductHome.text.toString()
                callGetStandardCoursesWS(subjectType = subjectTypeValue, pageNo = pageIndex, searchedText = searchedWord, shouldShowProgress = true)
                true
            }
            false
        }

        mainActivity!!.tvSearchCloseHome.setOnClickListener {
            mainActivity!!.llRight.visibility = View.VISIBLE
            mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
            mainActivity!!.llSearchHome.visibility = View.GONE
            mainActivity!!.edtSearchProductHome.text.clear()
            mainActivity!!.llRight.visibility = View.VISIBLE

            searchedWord = ""

            Utils.hideKeyboard(mainActivity!!)

            if (userDetailsList != null && userDetailsList.size > 0) {
                userDetailsList.clear()
            }

            pageIndex = 0
            totalPage = 0

            callGetStandardCoursesWS(subjectTypeValue, pageIndex, "", true)
        }

        fab.setOnClickListener {
            var intentFilter = Intent(mainActivity, FilterCourseActivity::class.java)
            intentFilter.putExtra("subjectTypeValue", subjectTypeValue)
            startActivity(intentFilter)
        }

        rcvStandardCourses.addOnScrollListener(scrollListenerGrid!!)

        callGetAdvertisementList()

    }

    private fun callGetAdvertisementList() {

        if (APIUtils.isOnline(mainActivity)) {
            showProgressDialog(mainActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", mainActivity)
            dataMap[WSKey.ADVERTISEMENT_ID] = ""
            apiHelper!!.callWB(Constants.POST_ADVERTISEMENT_LIST, mainActivity, dataMap, Constants.W_ADVERTISEMENT_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(mainActivity, PleaseCheckInternetConnectionEng)
        }

    }

    private fun callGetCartCount() {

        if (APIUtils.isOnline(mainActivity)) {
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", mainActivity)
            apiHelper!!.callWB(Constants.POST_CART_COUNT, mainActivity, dataMap, Constants.W_CART_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(mainActivity, PleaseCheckInternetConnectionEng)
        }

    }

    private fun loadMoreDataGrid() {
        Logg.e(TAG, "<=== loadMoreDataGrid ===>")

        if (pageIndex < (totalPage - 1)) {
            coursesAdapter!!.addLoadingView()

            pageIndex++
            callGetStandardCoursesWS(subjectTypeValue, pageIndex, searchedWord, false)

        }
    }

    private fun callGetStandardCoursesWS(subjectType: String, pageNo: Int, searchedText: String, shouldShowProgress: Boolean) {

        if (APIUtils.isOnline(mainActivity)) {
            if (shouldShowProgress) {
                showProgressDialog(mainActivity)
            }

            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", mainActivity)
            dataMap[WSKey.PAGE_NO] = pageNo.toString()
            if (!Prefs.getString(PrefsKey.CLASS_ID, "", mainActivity).isNullOrEmpty()) {
                dataMap[WSKey.CLASS_ID] = Prefs.getString(PrefsKey.CLASS_ID, "", mainActivity)
            }
            dataMap[WSKey.SUBJECT_TYPE] = subjectType
            dataMap[WSKey.SEARCH_TEXT] = searchedText
            apiHelper!!.callWB(Constants.POST_HOME_DASHBOARD, mainActivity, dataMap, Constants.W_HOME_DASHBOARD, this, false, false, true, Constants.POST)
        } else {
            Toast.show(mainActivity, PleaseCheckInternetConnectionEng)
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onHomeFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.e(TAG, "<=== onAttach() ===>")
        if (context is OnHomeFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        Log.e(TAG, "<=== onDetach() ===>")
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnHomeFragmentInteractionListener {
        fun onHomeFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentHome.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentHome().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }

        val TAG: String = FragmentHome::class.java.simpleName
        fun newInstance() = FragmentHome()
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        when (responseTag) {
            W_ADVERTISEMENT_LIST -> {

                val gson = Gson()
                val advertiseResponse = gson.fromJson<AdvertiseResponse>(mRes.toString(), AdvertiseResponse::class.java!!)

                if (advertiseResponse.flag) {
                    if (advertiseResponse.adsList != null && advertiseResponse.adsList.isNotEmpty()) {
//                        rcvAdvertisement.adapter = AdvertiseAdapter(mainActivity, advertiseResponse.adsList)
//                        rcvAdvertisement.layoutManager?.scrollToPosition(0)

                        val imagePagerAdapter = AdvertisePagerAdapter(mainActivity, advertiseResponse.adsList)
//                        val imagePagerAdapter = InfinitePagerAdapter(mainActivity, advertiseResponse.adsList)
                        vpPromotional.adapter = imagePagerAdapter
//                        vpPromotional.pageMargin = mainActivity.resources.getDimension(R.dimen._10sdp).toInt()
//                        vpPromotional.setPadding(mainActivity.resources.getDimension(R.dimen._24sdp).toInt(), 0, mainActivity.resources.getDimension(R.dimen._24sdp).toInt(), 0)
//                        vpPromotional.pageMargin = mainActivity.resources.getDimension(R.dimen._12sdp).toInt()

                        setupAutoPager()

                    }
                }

                subjectTypeValue = SUBJECT_TYPE_STANDARD_VALUE
                callGetStandardCoursesWS(subjectTypeValue, pageIndex, "", true)

            }
            W_HOME_DASHBOARD -> {
                hideProgressDialog()

                swipeRefresh.isRefreshing = false

                val gson = Gson()
                val dashboardResponse = gson.fromJson<DashboardResponse>(mRes.toString(), DashboardResponse::class.java!!)

                if (dashboardResponse.flag) {

                    /*if (dashboardResponse.totalCart > 0) {
                        mainActivity!!.tvCartCount.visibility = View.VISIBLE
                        mainActivity!!.tvCartCount.text = dashboardResponse.totalCart.toString()
                    } else {
                        mainActivity!!.tvCartCount.visibility = View.GONE
                        mainActivity!!.tvCartCount.text = dashboardResponse.totalCart.toString()
                    }*/

                    if (dashboardResponse.userDetails != null && dashboardResponse.userDetails.size > 0) {

                        if (isRefreshing) {
                            if (dashboardResponse.userDetails[0].id == Prefs.getString(PrefsKey.FIRST_COURSE_ID, "", mainActivity)) {
                                Toast.show(mainActivity, mainActivity.getString(R.string.no_new_courses_found))
                            } else {
                                if (userDetailsList != null && userDetailsList.size > 0) {
                                    userDetailsList.clear()
                                }
                            }
                        }

                        Prefs.setString(PrefsKey.FIRST_COURSE_ID, dashboardResponse.userDetails[0].id, mainActivity)
                    }

                    isRefreshing = false


                    totalPage = dashboardResponse.totalPage!!

                    if (totalPage > 1) {
//                        if (dashboardResponse.userDetails != null && dashboardResponse.userDetails.isNotEmpty()) {
//                            coursesAdapter = CoursesAdapter(mainActivity, dashboardResponse.userDetails)
//                            rcvStandardCourses.adapter = coursesAdapter
//                        }

                        if (userDetailsList != null && userDetailsList.size > 0) {
                            coursesAdapter!!.removeLoadingView()
                            coursesAdapter!!.addData(dashboardResponse.userDetails)
                            coursesAdapter!!.notifyDataSetChanged()
                            scrollListenerGrid!!.setLoaded()

                        } else {
                            userDetailsList!!.addAll(dashboardResponse.userDetails)

                            coursesAdapter = CoursesAdapter(mainActivity, userDetailsList!!)
                            rcvStandardCourses.adapter = coursesAdapter
                            coursesAdapter!!.notifyDataSetChanged()

                            gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                override fun getSpanSize(position: Int): Int {
                                    return when (coursesAdapter!!.getItemViewType(position)) {
                                        VIEW_TYPE_ITEM -> 1
                                        VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                        else -> -1
                                    }
                                }
                            }
                        }

                    } else {
                        userDetailsList!!.addAll(dashboardResponse.userDetails)
//                        productDetailsSearchedList = modelProductList!!.productList

                        coursesAdapter = CoursesAdapter(mainActivity, userDetailsList!!)
                        rcvStandardCourses.adapter = coursesAdapter
                        coursesAdapter!!.notifyDataSetChanged()

                        gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                            override fun getSpanSize(position: Int): Int {
                                return when (coursesAdapter!!.getItemViewType(position)) {
                                    VIEW_TYPE_ITEM -> 1
                                    VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                    else -> -1
                                }
                            }
                        }
                    }


                } else {
                    Toast.showLongToast(mainActivity, dashboardResponse.message)
                }
            }
            W_CART_LIST -> {
                val gson = Gson()
                val cartCountResponse = gson.fromJson<CartCountResponse>(mRes.toString(), CartCountResponse::class.java!!)

                if (cartCountResponse.totalCart > 0) {
                    mainActivity!!.tvCartCount.visibility = View.VISIBLE
                    mainActivity!!.tvCartCount.text = cartCountResponse.totalCart.toString()
                } else {
                    mainActivity!!.tvCartCount.visibility = View.GONE
                    mainActivity!!.tvCartCount.text = cartCountResponse.totalCart.toString()
                }
            }
        }

    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        swipeRefresh.isRefreshing = false

        Toast.showLongToast(mainActivity, getString(R.string.meesage_connection_timeout))
    }

    fun setupAutoPager() {
        var handler = Handler()

        var update = Runnable {
            kotlin.run {
                try {
                    if (vpPromotional != null) {
                        vpPromotional.setCurrentItem(currentPage, true)
                        if (currentPage == Integer.MAX_VALUE) {
                            currentPage = 0
                        } else {
                            ++currentPage
                        }
                    }
                } catch (ex:Exception) {
                    ex.printStackTrace()
                }
            }
        }

        timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(update)
            }

        }, 500, 5000)
    }

    override fun onStop() {
        super.onStop()

        if (::timer.isInitialized) {
            timer.cancel()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (::timer.isInitialized) {
            timer.cancel()
        }
    }
}
