package com.sms.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alcohol.core.BaseFragment
import com.google.gson.Gson
import com.sms.activity.MainActivity

import com.sms.R
import com.sms.adapters.ExamListAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_ADD_TO_CART_EXAM
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.exam.ExamListResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.fragment_exam.*
import kotlinx.android.synthetic.main.include_home_header.*
import java.util.HashMap

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FragmentExam.OnNotificationsFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentExam.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class FragmentExam : BaseFragment() {

    private lateinit var mainActivity: MainActivity

    private var param1: String? = null
    private var param2: String? = null
    private var listener: FragmentExam.OnExamFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_exam, container, false)

        mainActivity = activity as MainActivity

//        mainActivity!!.tvHeaderTitle.text = resources.getString(R.string.my_courses)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivity!!.llRight.visibility = View.VISIBLE
        mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
        mainActivity!!.llSearchHome.visibility = View.GONE
        mainActivity!!.llRight.visibility = View.GONE

        mainActivity!!.tvHeaderTitle.text = mainActivity.getString(R.string.title_exam)

        rcvExam?.layoutManager = GridLayoutManager(mainActivity, 2)

        callGetExamListWS()
    }

    private fun callGetExamListWS() {

        if (APIUtils.isOnline(mainActivity)) {
            showProgressDialog(mainActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", mainActivity)

            if (!Prefs.getString(PrefsKey.CLASS_ID, "", mainActivity).isNullOrEmpty()) {
                dataMap[WSKey.CLASS_ID] = Prefs.getString(PrefsKey.CLASS_ID, "", mainActivity)
            } else {
                dataMap[WSKey.CLASS_ID] = ""
            }
            apiHelper!!.callWB(Constants.POST_EXAM_LIST, mainActivity, dataMap, Constants.W_EXAM_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(mainActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    fun onButtonPressed(uri: Uri) {
        listener?.onExamFragmentInteractionListener(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentExam.OnExamFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnNotificationsFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnExamFragmentInteractionListener {
        fun onExamFragmentInteractionListener(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment FragmentExam.
         */
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentExam().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }

        val TAG: String = FragmentExam::class.java.simpleName
        fun newInstance() = FragmentExam()
    }

    public fun addToCartExamAPI(examId: String, examPrice: String) {

        if (APIUtils.isOnline(mainActivity)) {
            showProgressDialog(mainActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", mainActivity)
            dataMap[WSKey.EXAM_ID] = examId
            dataMap[WSKey.PRICE] = examPrice
            apiHelper!!.callWB(Constants.POST_ADD_TO_CART_EXAM, mainActivity, dataMap, Constants.W_ADD_TO_CART_EXAM, this, false, false, true, Constants.POST)
        } else {
            Toast.show(mainActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(FragmentHome.TAG, responseTag + " Response " + mRes.toString())

        when (responseTag) {

            Constants.W_EXAM_LIST -> {
                hideProgressDialog()

                val gson = Gson()
                val examListResponse = gson.fromJson<ExamListResponse>(mRes.toString(), ExamListResponse::class.java!!)

                if (examListResponse.flag) {

                    if (examListResponse.examList != null && examListResponse.examList.size > 0) {
                        tvNoExam.visibility = View.GONE
                        rcvExam.visibility = View.VISIBLE
                        var examListAdapter = ExamListAdapter(mainActivity, examListResponse.examList, this)
                        rcvExam.adapter = examListAdapter
                    } else {
                        tvNoExam.visibility = View.VISIBLE
                        rcvExam.visibility = View.GONE
                    }

                    /*if (totalPage > 1) {
//                        if (examListResponse.userDetails != null && examListResponse.userDetails.isNotEmpty()) {
//                            coursesAdapter = CoursesAdapter(mainActivity, examListResponse.userDetails)
//                            rcvStandardCourses.adapter = coursesAdapter
//                        }

                        if (userDetailsList != null && userDetailsList.size > 0) {
                            coursesAdapter!!.removeLoadingView()
                            coursesAdapter!!.addData(examListResponse.userDetails)
                            coursesAdapter!!.notifyDataSetChanged()
                            scrollListenerGrid!!.setLoaded()

                        } else {
                            userDetailsList!!.addAll(examListResponse.userDetails)

                            coursesAdapter = ExamListAdapter(mainActivity, userDetailsList!!)
                            rcvExam.adapter = coursesAdapter
                            coursesAdapter!!.notifyDataSetChanged()

                            gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                override fun getSpanSize(position: Int): Int {
                                    return when (coursesAdapter!!.getItemViewType(position)) {
                                        Constants.VIEW_TYPE_ITEM -> 1
                                        Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                        else -> -1
                                    }
                                }
                            }
                        }

                    } else {
                        userDetailsList!!.addAll(examListResponse.userDetails)
//                        productDetailsSearchedList = modelProductList!!.productList

                        coursesAdapter = CoursesAdapter(mainActivity, userDetailsList!!)
                        rcvStandardCourses.adapter = coursesAdapter
                        coursesAdapter!!.notifyDataSetChanged()

                        gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                            override fun getSpanSize(position: Int): Int {
                                return when (coursesAdapter!!.getItemViewType(position)) {
                                    Constants.VIEW_TYPE_ITEM -> 1
                                    Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                    else -> -1
                                }
                            }
                        }
                    }*/

                } else {
                    Toast.showLongToast(mainActivity, examListResponse.message)
                }
            }
            W_ADD_TO_CART_EXAM -> {

            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(mainActivity, getString(R.string.meesage_connection_timeout))
    }
}
