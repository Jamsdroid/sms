package com.sms.fragment

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.support.v4.content.FileProvider
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alcohol.core.BaseFragment
import com.folioreader.FolioReader
import com.folioreader.model.ReadPosition
import com.folioreader.model.ReadPositionImpl
import com.google.gson.Gson
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.sms.R
import com.sms.activity.MainActivity
import com.sms.adapters.MyCourseDownloadedListAdapter
import com.sms.adapters.MyCourseListAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_MY_COURSE
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.common.Utils
import com.sms.common.Utils.Companion.getDecryptedPath
import com.sms.custom.FileList
import com.sms.model.mycourse.MyCoursesResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.fragment_my_courses.*
import kotlinx.android.synthetic.main.include_home_header.*
import java.io.*
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.CipherOutputStream
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.SecretKeySpec
import kotlin.collections.ArrayList

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FragmentMyCourses : BaseFragment() {

    private lateinit var mainActivity: MainActivity

    private var param1: String? = null
    private var param2: String? = null
    private var listener: FragmentMyCourses.OnMyCoursesFragmentInteractionListener? = null
    var decFilename = ""
    private lateinit var fragmentMyCourses: FragmentMyCourses

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater?.inflate(R.layout.fragment_my_courses, container, false)

        mainActivity = activity as MainActivity

        fragmentMyCourses = FragmentMyCourses()

//        mainActivity!!.tvHeaderTitle.text = resources.getString(R.string.my_courses)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainActivity!!.llRight.visibility = View.VISIBLE
        mainActivity!!.tvHeaderTitle.visibility = View.VISIBLE
        mainActivity!!.llSearchHome.visibility = View.GONE
        mainActivity!!.llRight.visibility = View.GONE

        mainActivity!!.tvHeaderTitle.text = mainActivity.getString(R.string.my_courses)

        rcvMyCourseList?.layoutManager = GridLayoutManager(mainActivity, 2)


        checkFilePermission()

//        callGetMyCoursesListWS()
    }

    private fun getDownloadedFileList() : ArrayList<String> {

//        var downloadFolder = Environment.getExternalStorageDirectory().toString() + "" + File.separator + "SMS/"
        var downloadFolder = Environment.getExternalStorageDirectory().toString() + File.separator + "Android/data/" + activity!!.packageName + File.separator + "document/";

        Logg.e(FragmentExam.TAG, "downloadFolder ====> $downloadFolder")

        val directory = File(downloadFolder)

        if (!directory.exists()) {
            Logg.e(FragmentExam.TAG, "<===== directory not exist =====>")
            directory.mkdirs()
        }

        var fileList = FileList(downloadFolder)

        try {
            if (fileList?.filesList != null) {
                var filesList = fileList.filesList

//            var filesList = ArrayList(Arrays.asList(directory.listFiles()))

                for (i in filesList.indices) {
                    if (fileList.filesList[i].contains(".epub")) {
                        filesList.remove(fileList.filesList[i])
                        val actual_file = File(fileList.filesList[i])
                        actual_file.delete()
                    }
                }

                Logg.e(FragmentExam.TAG, "filesList size ===> ${filesList.size}")

                return filesList
            } else {
                return ArrayList<String>()
            }
        } catch (ex:Exception) {
            ex.printStackTrace()
            return ArrayList<String>()
        }


//        for (i in 0 until filesList.size) {
//            Logg.e(TAG, "filesList[i] ====> " +filesList[i])
//            if (filesList[i].toString() == fileName) {
//            }
//        }

//        return filesList

    }

    private fun callGetMyCoursesListWS() {

        if (APIUtils.isOnline(mainActivity)) {
            showProgressDialog(mainActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", mainActivity)
            apiHelper!!.callWB(Constants.POST_MY_COURSE, mainActivity, dataMap, Constants.W_MY_COURSE, this, false, false, true, Constants.POST)
        } else {
            Toast.show(mainActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onMyCoursesFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentMyCourses.OnMyCoursesFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnExamFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnMyCoursesFragmentInteractionListener {
        fun onMyCoursesFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                FragmentMyCourses().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }

        val TAG: String = FragmentMyCourses::class.java.simpleName
        fun newInstance() = FragmentMyCourses()
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        when (responseTag) {

            W_MY_COURSE -> {
                hideProgressDialog()

                val gson = Gson()
                val myCourseResponse = gson.fromJson<MyCoursesResponse>(mRes.toString(), MyCoursesResponse::class.java!!)

                if (myCourseResponse.flag) {

                    if (myCourseResponse.myCourseList != null && myCourseResponse.myCourseList.size > 0) {
                        tvNoCourse.visibility = View.GONE
                        rcvMyCourseList.visibility = View.VISIBLE
                        var examListAdapter = MyCourseListAdapter(mainActivity, myCourseResponse.myCourseList)
                        rcvMyCourseList.adapter = examListAdapter
                    } else {
                        tvNoCourse.visibility = View.VISIBLE
                        rcvMyCourseList.visibility = View.GONE
                    }

                } else {
                    Toast.showLongToast(mainActivity, myCourseResponse.message)
                }
            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(mainActivity, getString(R.string.meesage_connection_timeout))
    }

    private fun checkFilePermission() {

        val permissionlistener = object : PermissionListener {
            override fun onPermissionGranted() {
                // process file

                processDownloadedFileListing()

            }

            override fun onPermissionDenied(deniedPermissions: java.util.ArrayList<String>) {
            }
        }

        TedPermission.with(mainActivity)
                .setPermissionListener(permissionlistener)
//                .setRationaleMessage("we need permission to access your external storage")
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check()
    }

    private fun processDownloadedFileListing() {

        var filesList : ArrayList<String> = getDownloadedFileList()

        if (filesList != null && filesList.size > 0) {
            filesList.reverse()
            tvNoCourse.visibility = View.GONE
            rcvMyCourseList.visibility = View.VISIBLE
            var courseListAdapter = MyCourseDownloadedListAdapter(mainActivity, filesList, fragmentMyCourses)
            rcvMyCourseList.adapter = courseListAdapter
        } else {
            tvNoCourse.visibility = View.VISIBLE
            rcvMyCourseList.visibility = View.GONE
        }
    }

    fun openPDF(context: Context, file: File) {

        val path = FileProvider.getUriForFile(context, "com.sms", file)
        val pdfOpenintent = Intent(Intent.ACTION_VIEW)
        pdfOpenintent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        pdfOpenintent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        pdfOpenintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        pdfOpenintent.setDataAndType(path, "application/pdf")
        try {
//            context.startActivity(pdfOpenintent)

            var folioReader : FolioReader = FolioReader.get()
            Logg.e(TAG, "file path ===>"+file.path)
            Logg.e(TAG, "file path ===>"+file.absolutePath)
            Logg.e(TAG, "file path ===>"+file.canonicalPath)
            folioReader.openBook(file.canonicalPath)

//            val readPosition = getLastReadPosition()
//
//            var config = AppUtil.getSavedConfig(mainActivity)
//            if (config == null)
//                config = Config()
//            config!!.allowedDirection = Config.AllowedDirection.VERTICAL_AND_HORIZONTAL
//
//            folioReader.setReadPosition(readPosition)
//                    .setConfig(config, true)
//                    .openBook("file:///android_asset/dummy.epub")


        } catch (ex: ActivityNotFoundException) {
            Toast.showLongToast(context, "Please install pdf reader app from google play store to open the pdf")
            ex.printStackTrace()
        }
    }

    private fun getLastReadPosition(): ReadPosition? {

        val jsonString = loadAssetTextAsString("read_positions/read_position.json")
        return ReadPositionImpl.createInstance(jsonString)
    }

    private fun loadAssetTextAsString(name: String): String? {
        var bufferReader: BufferedReader? = null
        try {
            val buf = StringBuilder()
            val inputStream = mainActivity.assets.open(name)
            bufferReader = BufferedReader(InputStreamReader(inputStream))

            var str: String
            var isFirst = true
            str = bufferReader.readLine()
            while (str != null) {
                if (isFirst)
                    isFirst = false
                else
                    buf.append('\n')
                buf.append(str)
                str = bufferReader.readLine()
            }
            return buf.toString()
        } catch (e: IOException) {
            Log.e("HomeActivity", "Error opening asset $name")
        } finally {
            if (bufferReader != null) {
                try {
                    bufferReader.close()
                } catch (e: IOException) {
                    Log.e("HomeActivity", "Error closing asset $name")
                }

            }
        }
        return null
    }

    fun cipherDecFileAndopen(context: Context, courseName: String) {

        try {
            var tempFileName = courseName.replace(".ciphernenc", ".epub");
            decFilename = tempFileName
            cipherDecFile(context, "/" + courseName)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchPaddingException) {
            e.printStackTrace()
        } catch (e: InvalidKeyException) {
            e.printStackTrace()
        }
    }


    @Throws(IOException::class, NoSuchAlgorithmException::class, NoSuchPaddingException::class, InvalidKeyException::class)
    internal fun cipherDecFile(context: Context, enc_file_name: String) {

        val extStore = File(Environment.getExternalStorageDirectory().toString() + File.separator + "Android/data/" + context?.packageName + File.separator + "document/")
        val fis = FileInputStream(extStore.toString() + "/" + enc_file_name)
        val dec_file_name = getDecryptedPath(extStore.toString() + "/." + enc_file_name)

        val fos = FileOutputStream(dec_file_name)
        val sks = SecretKeySpec("NirCipher456DEnc".toByteArray(),
                "AES")
        val cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.DECRYPT_MODE, sks)
        val cis = CipherInputStream(fis, cipher)
        var b: Int = 0
        val d = ByteArray(1024 * 1024)

        while ({ b = cis.read(d);b }() != -1) {
            fos.write(d, 0, b)
        }

        fos.flush()
        fos.close()
        cis.close()
        val file = File(dec_file_name)
        openPDF(context, file)

        val handler = Handler()
        handler.postDelayed({

            if (!decFilename.equals("")) {
                try {
                    cipherEncFile(Environment.getExternalStorageDirectory().toString() + File.separator + "Android/data/" + context?.packageName + File.separator + "document/" + decFilename)
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (e: NoSuchAlgorithmException) {
                    e.printStackTrace()
                } catch (e: NoSuchPaddingException) {
                    e.printStackTrace()
                } catch (e: InvalidKeyException) {
                    e.printStackTrace()
                }
            }
        }, 3000)


    }

    @Throws(IOException::class, NoSuchAlgorithmException::class, NoSuchPaddingException::class, InvalidKeyException::class)
    internal fun cipherEncFile(file_name: String) {

        val fis = FileInputStream(file_name)
        val enc_file_name = Utils.getCipherEncPath(file_name)
        val fos = FileOutputStream(enc_file_name)
        val sks = SecretKeySpec("NirCipher456DEnc".toByteArray(), "AES")
        val cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.ENCRYPT_MODE, sks)
        val cos = CipherOutputStream(fos, cipher)
        var b: Int = 0
        val data = ByteArray(1024 * 1024)
        while ({ b = fis.read(data);b }() != -1) {
            cos.write(data, 0, b)
        }

        cos.flush()
        cos.close()
        fis.close()

        val actual_file = File(file_name)
        actual_file.delete()

    }
}
