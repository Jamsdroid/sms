package com.sms.common;

/**
 * Created by Tops on 1/17/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;


import com.sms.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Class that helps, for Android App, selecting image from gallery, getting image from camera and
 * cropping image.
 * <p/>
 * <p/>
 * IMPORTANT: The Activity, that contains this object or that contains the fragment that uses this
 * object, must handle the orientation change during taking photo. Either lock the orientation of
 * the Activity or handle orientation changes. Otherwise taking photo feature will not work since
 * the new instance of this object will be created when device rotates.
 */
public class ImageInputHelperNew extends FileProvider {

    public static final int REQUEST_PICTURE_FROM_GALLERY = 23;
    public static final int REQUEST_PICTURE_FROM_CAMERA = 24;
    public static final int REQUEST_CROP_PICTURE = 25;
    private static final String TAG = "ImageInputHelper";

    private File tempFileFromSource = null;
    private Uri tempUriFromSource = null;

    private File tempFileFromCrop = null;
    private Uri tempUriFromCrop = null;

    /**
     * Activity object that will be used while calling startActivityForResult(). Activity then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImageInputHelper for handling result and being notified.
     */
    private Activity mContext;

    /**
     * Fragment object that will be used while calling startActivityForResult(). Fragment then will
     * receive the callbacks to its own onActivityResult() and is responsible of calling the
     * onActivityResult() of the ImageInputHelper for handling result and being notified.
     */
    private Fragment fragment;

    /**
     * Listener instance for callbacks on user events. It must be set to be able to use
     * the ImageInputHelper object.
     */
    private ImageActionListener imageActionListener;

    public ImageInputHelperNew() {

    }

    public ImageInputHelperNew(Activity mContext) {
        this.mContext = mContext;
    }

    public ImageInputHelperNew(Fragment fragment) {
        this.fragment = fragment;
        this.mContext = fragment.getActivity();
    }

    public void setImageActionListener(ImageActionListener imageActionListener) {
        this.imageActionListener = imageActionListener;
    }

    /**
     * Handles the result of events that the Activity or Fragment receives on its own
     * onActivityResult(). This method must be called inside the onActivityResult()
     * of the container Activity or Fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult of imageInputHelper");
        if ((requestCode == REQUEST_PICTURE_FROM_GALLERY) && (resultCode == Activity.RESULT_OK)) {
            Log.d(TAG, "Image selected from gallery");
            imageActionListener.onImageSelectedFromGallery(data.getData(), tempFileFromSource);

        } else if ((requestCode == REQUEST_PICTURE_FROM_CAMERA) && (resultCode == Activity.RESULT_OK)) {
            Log.d(TAG, "Image selected from camera");
            imageActionListener.onImageTakenFromCamera(tempUriFromSource, tempFileFromSource);

        } else if ((requestCode == REQUEST_CROP_PICTURE) && (resultCode == Activity.RESULT_OK)) {
            Log.d(TAG, "Image returned from crop");
            imageActionListener.onImageCropped(tempUriFromCrop, tempFileFromCrop);
        }
    }

    /**
     * Starts an intent for selecting image from gallery. The result is returned to the
     * onImageSelectedFromGallery() method of the ImageSelectionListener interface.
     */
    public void selectImageFromGallery() {
        checkListener();
        try {
            Date date = Calendar.getInstance().getTime();
            String prefix = "IMG_";
            String str_appointmentStrtEndDateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str_appointmentStrtEndDateFormat);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/" + mContext.getString(R.string.app_name));

            if (!myDir.exists())
                myDir.mkdirs();

            tempFileFromSource = new File(myDir, (prefix + System.currentTimeMillis() + ".jpg"));
            tempUriFromSource = Uri.fromFile(tempFileFromSource);

//            tempUriFromSource = FileProvider.getUriForFile(mContext,
//                    mContext.getApplicationContext().getPackageName() + ".com.alcohol.common.provider", tempFileFromSource);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUriFromSource);
        if (fragment == null) {
            mContext.startActivityForResult(intent, REQUEST_PICTURE_FROM_GALLERY);
        } else {
            fragment.startActivityForResult(intent, REQUEST_PICTURE_FROM_GALLERY);
        }
    }

    /**
     * Starts an intent for taking photo with camera. The result is returned to the
     * onImageTakenFromCamera() method of the ImageSelectionListener interface.
     */
    public void takePhotoWithCamera() {
        checkListener();
        try {
            Date date = Calendar.getInstance().getTime();
            String str_appointmentStrtEndDateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str_appointmentStrtEndDateFormat);
            String prefix = "IMG_";
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/" + mContext.getString(R.string.app_name));
            myDir.mkdirs();
            tempFileFromSource  = new File(myDir, (prefix + System.currentTimeMillis() + ".jpg"));
            //tempUriFromSource = Uri.fromFile(tempFileFromSource);

            tempUriFromSource = FileProvider.getUriForFile(mContext,
                    mContext.getApplicationContext().getPackageName() + ".com.alcohol.common.provider", tempFileFromSource);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUriFromSource);
        if (fragment == null) {
            mContext.startActivityForResult(intent, REQUEST_PICTURE_FROM_CAMERA);
        } else {
            fragment.startActivityForResult(intent, REQUEST_PICTURE_FROM_CAMERA);
        }
    }

    /**
     * Starts an intent for cropping an image that is saved in the uri. The result is
     * returned to the onImageCropped() method of the ImageSelectionListener interface.
     *
     * @param uri     uri that contains the data of the image to crop
     * @param outputX width of the result image
     * @param outputY height of the result image
     * @param aspectX horizontal ratio value while cutting the image
     * @param aspectY vertical ratio value of while cutting the image
     */
    public void requestCropImage(File file, Uri uri, int FromWhere, int outputX, int outputY, int aspectX, int aspectY) {
        checkListener();
//FromWhere = camera =1,galary=2
        //  if (tempFileFromCrop == null) {
        try {
            Date date = Calendar.getInstance().getTime();
            String prefix = "IMG_";
            String str_appointmentStrtEndDateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str_appointmentStrtEndDateFormat);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/" + mContext.getString(R.string.app_name));
            myDir.mkdirs();
            tempFileFromCrop = new File(myDir, (prefix + System.currentTimeMillis() + ".jpg"));

            // tempFileFromCrop = File.createTempFile("crop", ".png", mContext.getExternalCacheDir());

            tempUriFromCrop = Uri.fromFile(tempFileFromCrop);

//            tempUriFromCrop = FileProvider.getUriForFile(mContext,
//                    mContext.getApplicationContext().getPackageName() + "com.humati.helper.provider", tempFileFromCrop);

//            tempUriFromCrop = getUriForFile(getContext(), "com.humati.helper.provider", tempFileFromCrop);

//            mContext.getApplicationContext().grantUriPermission("com.android.camera", tempUriFromCrop,
//                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        } catch (Exception e) {
            e.printStackTrace();
        }

        // open crop intent when user selects image
        final Intent intent = new Intent("com.android.camera.action.CROP");
        if (FromWhere == 1) {
            intent.setDataAndType(uri, "image/*");
        } else if (FromWhere == 2) {
            //Uri ur = Uri.fromFile(new File(file.getAbsolutePath()));
            intent.setDataAndType(uri, "image/*");
        }
        intent.putExtra("output", tempUriFromCrop);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("aspectX", aspectX);
        intent.putExtra("aspectY", aspectY);
        intent.putExtra("scale", true);
        intent.putExtra("noFaceDetection", true);

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.putExtra("return-data", true);

        List<ResolveInfo> resInfoList = mContext.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resInfoList) {
            String packageName = resolveInfo.activityInfo.packageName;
            mContext.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }

        if (fragment == null) {
            mContext.startActivityForResult(intent, REQUEST_CROP_PICTURE);
        } else {
            fragment.startActivityForResult(intent, REQUEST_CROP_PICTURE);
        }
    }

    private void checkListener() {
        if (imageActionListener == null) {
            throw new RuntimeException("ImageSelectionListener must be set before calling openGalleryIntent(), openCameraIntent() or requestCropImage().");
        }
    }

    /**
     * Listener interface for receiving callbacks from the ImageInputHelper.
     */
    public interface ImageActionListener {
        void onImageSelectedFromGallery(Uri uri, File imageFile);

        void onImageTakenFromCamera(Uri uri, File imageFile);

        void onImageCropped(Uri uri, File imageFile);


    }


}
