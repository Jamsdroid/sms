package com.sms.common

import android.content.Context

class Toast {

    companion object {
        fun show(context: Context?, message: String) {
            android.widget.Toast.makeText(context, message, android.widget.Toast.LENGTH_SHORT).show()
        }

        fun show(context: Context?, message: Int) {
            android.widget.Toast.makeText(context, "" + message, android.widget.Toast.LENGTH_SHORT).show()
        }

        fun showLongToast(context: Context?, message: String) {
            android.widget.Toast.makeText(context, message, android.widget.Toast.LENGTH_LONG).show()
        }

        fun showLongToast(context: Context?, message: Int) {
            android.widget.Toast.makeText(context, "" + message, android.widget.Toast.LENGTH_LONG).show()
        }
    }

    fun show(context: Context?, message: String) {
        android.widget.Toast.makeText(context, message, android.widget.Toast.LENGTH_SHORT).show()
    }

    fun show(context: Context?, message: Int) {
        android.widget.Toast.makeText(context, "" + message, android.widget.Toast.LENGTH_SHORT).show()
    }

    fun showLongToast(context: Context?, message: String) {
        android.widget.Toast.makeText(context, message, android.widget.Toast.LENGTH_LONG).show()
    }

    fun showLongToast(context: Context?, message: Int) {
        android.widget.Toast.makeText(context, "" + message, android.widget.Toast.LENGTH_LONG).show()
    }
}
