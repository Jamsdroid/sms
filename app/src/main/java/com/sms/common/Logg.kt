package com.sms.common

import android.util.Log
import com.sms.BuildConfig

import org.json.JSONObject

class Logg {

    companion object {

        fun e(tag: String, message: String) {
            try {
                if (BuildConfig.DEBUG)
                    Log.e(tag, message)
            } catch (e: Exception) {
            }
        }

        fun e(tag: String, message: Boolean?) {
            try {
                if (BuildConfig.DEBUG)
                    Log.e(tag, "" + message!!)
            } catch (e: Exception) {
            }
        }

        fun e(tag: String, message: JSONObject) {
            try {
                if (BuildConfig.DEBUG)
                    Log.e(tag, "" + message.toString())
            } catch (e: Exception) {
            }
        }

        fun e(tag: String, message: Int?) {
            try {
                if (BuildConfig.DEBUG)
                    Log.e(tag, "" + message!!)
            } catch (e: Exception) {
            }
        }

        fun d(tag: String, message: String) {
            try {
                if (BuildConfig.DEBUG)
                    Log.d(tag, message)
            } catch (e: Exception) {
            }
        }

        fun i(tag: String, message: String) {
            try {
                if (BuildConfig.DEBUG)
                    Log.i(tag, message)
            } catch (e: Exception) {
            }
        }

        fun v(tag: String, message: String) {
            try {
                if (BuildConfig.DEBUG)
                    Log.v(tag, message)
            } catch (e: Exception) {
            }
        }
    }
}