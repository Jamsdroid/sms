package com.sms.common

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.provider.Settings
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.sms.R
import kotlinx.android.synthetic.main.dialog.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class Utils {

    companion object {

        var TAG: String = javaClass.simpleName

        var PASSWORD_PATTERN: String = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@\$%^&*-]).{6,}\$"

        private var pattern: Pattern? = null
        private var matcher: Matcher? = null

        private val calendar = Calendar.getInstance()

        @SuppressLint("HardwareIds")
        fun getDeviceUUID(context: Context): String {
            return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        }

        fun validatePassword(password: String): Boolean {
            val passwordValidator = PasswordValidator()
            if (pattern != null) {
                matcher = pattern!!.matcher(password)
            }
            return matcher?.matches()!!
        }

        fun PasswordValidator() {
            pattern = Pattern.compile(PASSWORD_PATTERN)
        }

        fun selectBirthDate(activity: Activity, textview: TextView, agetextview: TextView?) {
//            val c = Calendar.getInstance();

            var year = calendar!!.get(Calendar.YEAR)
            var month = calendar!!.get(Calendar.MONTH)
            var day = calendar!!.get(Calendar.DAY_OF_MONTH)

            var selectedDate: String
            if (!TextUtils.isEmpty(textview!!.text.toString())) {
                selectedDate = textview!!.text.toString()
                Logg.e(TAG, "selectedDate ====> " + selectedDate)
                val simpleSelectedDate = SimpleDateFormat("dd/MM/yyyy").parse(selectedDate)
                Logg.e(TAG, "simpleSelectedDate ====> " + simpleSelectedDate)
                val calSelected = Calendar.getInstance()
                calSelected.timeInMillis = simpleSelectedDate.time

                year = calSelected!!.get(Calendar.YEAR)
                month = calSelected!!.get(Calendar.MONTH)
                day = calSelected!!.get(Calendar.DAY_OF_MONTH)
            }

            val AdultDate = SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().timeInMillis)

            val datePickerDialog = DatePickerDialog(activity,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        if (calendar != null) {
                            calendar.set(year, monthOfYear, dayOfMonth)
                        }
                        val date = dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year
                        val simpleDate = SimpleDateFormat("dd-MM-yyyy").parse(date)
                        if (validateAge(SimpleDateFormat("dd/MM/yyyy").format(simpleDate), AdultDate)) {
                            textview.text = SimpleDateFormat("dd/MM/yyyy").format(simpleDate)
                            if (agetextview != null) {
//                                agetextview.setText(countAge(SimpleDateFormat("dd-MMM-yyyy").format(simpleDate).toString(), "dd-MMM-yyyy").toString())
                            }
                        } else
                            showAlert(activity.resources.getString(R.string.validate_age), activity.resources.getString(R.string.validation), activity)
                    }, year, month, day)
//            datePickerDialog.datePicker.maxDate = calendar.timeInMillis
            var cal: Calendar = Calendar.getInstance()
            cal.add(Calendar.YEAR, -3)
//            cal.add(Calendar.DAY_OF_MONTH, -1)
//            cal.set(Calendar.HOUR_OF_DAY, 23)
//            cal.set(Calendar.MINUTE, 0)
//            cal.set(Calendar.SECOND, 0)
            Logg.e(TAG, "cal =====> " + cal.time)
            val maxDateInLong: Long = cal.timeInMillis
            datePickerDialog.datePicker.maxDate = maxDateInLong
            datePickerDialog.show()
        }

        fun validateAge(selectedDate: String, adultage: String): Boolean {
            val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.US)
            val date1 = simpleDateFormat.parse(selectedDate)
            val Adultdate = simpleDateFormat.parse(adultage)

            Logg.d("System out", "date1" + date1)

            Logg.d("System out", "Adultdate" + Adultdate)
            if (Adultdate.before(date1)) {
                Logg.d("System out", "its true")
                return false
            }
            return true
        }

        fun countAge(selectedDate: String, inputDateFormate: String): Int {
            Logg.e(TAG, "countAge entered date ===> " + selectedDate)
            val simpleDateFormat = SimpleDateFormat(inputDateFormate, Locale.US)
            val date1 = simpleDateFormat.parse(selectedDate)
            val currentDate = simpleDateFormat.parse(SimpleDateFormat(inputDateFormate).format(Date()))
            val age = currentDate.year - date1.year
            return age
        }

        fun showAlert(message: String, title: String, activity: Activity) {
            val dialog = Dialog(activity) // Context, this, etc.
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.dialog)
            dialog.dialogMessage.text = message
            dialog.dialogtitle.text = title
            dialog.dialogOK.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }

        fun convertDDMMYYYTOYYYYMMDD(inputDate: String): String {

            val simpleDate = SimpleDateFormat("dd/MM/yyyy").parse(inputDate)
            val destDate = SimpleDateFormat("yyyy/MM/dd").format(simpleDate)

            return destDate

        }

        fun convertDDMMYYYTOYYYYMMDDwithDesh(inputDate: String): String {

            val simpleDate = SimpleDateFormat("dd/MM/yyyy").parse(inputDate)
            val destDate = SimpleDateFormat("yyyy-MM-dd").format(simpleDate)

            return destDate

        }

        fun convertYYYYMMDDTODDMMYYY(inputDate: String): String {

            return if (!inputDate.isNullOrBlank()) {
                val simpleDate = SimpleDateFormat("yyyy-MM-dd").parse(inputDate)
                val destDate = SimpleDateFormat("dd/MM/yyyy").format(simpleDate)

                destDate
            } else {
                ""
            }
        }

        fun convertDDMMYYYYTODDMMYYY(inputDate: String): String {

            return if (!inputDate.isNullOrBlank()) {
                val simpleDate = SimpleDateFormat("dd-MM-yyyy").parse(inputDate)
                val destDate = SimpleDateFormat("dd/MM/yyyy").format(simpleDate)

                destDate
            } else {
                ""
            }
        }

        fun showKeyboard(context: Activity, editText: EditText) {
            // Check if no view has focus:
            val view = context.currentFocus
            if (view != null) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
            }
        }

        fun hideKeyboard(context: Activity) {
            // Check if no view has focus:
            val view = context.currentFocus
            if (view != null) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        fun formatePaymentDate(date: String): String {

            return if (date.isNullOrBlank()) {
                ""
            } else {

                try {
                    val simpleDate = SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date)
                    val destDate = SimpleDateFormat("dd MMM yyyy, hh:mma").format(simpleDate)

                    destDate
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    ""
                }
            }
        }

        fun formateNotificationDate(date: String): String {

            return if (date.isNullOrBlank()) {
                ""
            } else {

                try {
                    val simpleDate = SimpleDateFormat("dd-MM-yyyy hh:mm a").parse(date)
                    val destDate = SimpleDateFormat("dd MMM, yyyy, hh:mm a").format(simpleDate)

                    destDate
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    ""
                }
            }
        }

        fun formatExamHistoryDate(date: String): String {

            return if (date.isNullOrBlank()) {
                ""
            } else {

                try {
                    val simpleDate = SimpleDateFormat("dd-MM-yyyy").parse(date)
                    val destDate = SimpleDateFormat("dd MMM, yyyy").format(simpleDate)

                    destDate
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    ""
                }
            }
        }

        fun getNumberSuffix(number: String): String {

            return try {

                var num = number.toInt()

                val suffix = arrayOf("th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th")
                when (num % 100) {
                    11, 12, 13 -> num.toString() + "th"
                    else -> num.toString() + suffix[num % 10]
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
                ""
            }
        }

        fun getCipherEncPath(path: String): String {
            val index = path.lastIndexOf(".")
            return path.substring(0, index) + ".ciphernenc"
        }

        fun getDecryptedPath(path: String): String {
            val index = path.lastIndexOf(".")
            return path.substring(0, index) + ".epub"
        }

    }
}

// extension functions

fun AppCompatActivity.hideKeyboard(context: Activity) {
    // Check if no view has focus:
    val view = context.currentFocus
    if (view != null) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun AppCompatActivity.isEmailValid(email: String): Boolean {
    return Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    ).matcher(email).matches()
}

fun BottomNavigationView.disableShiftMode() {
    val menuView = getChildAt(0) as BottomNavigationMenuView
    try {
        menuView.javaClass.getDeclaredField("mShiftingMode").also { shiftMode ->
            shiftMode.isAccessible = true
            shiftMode.setBoolean(menuView, false)
            shiftMode.isAccessible = false
        }
        for (i in 0 until menuView.childCount) {
            (menuView.getChildAt(i) as BottomNavigationItemView).also { item ->
                //                item.setShiftingMode(false)
//                item.setChecked(item.itemData.isChecked)
            }
        }
    } catch (e: NoSuchFieldException) {
        Logg.e("BottomNavigationHelper", "Unable to get shift mode field" + e)
    } catch (e: IllegalAccessException) {
        Logg.e("BottomNavigationHelper", "Unable to change value of shift mode" + e)
    }
}

fun AppCompatActivity.displayOkDialog(context: Activity, message: String) {

    val dialog = Dialog(context)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setContentView(R.layout.dialog_custom_ok)
    dialog.setCancelable(false)
    dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
    val tvOne = dialog.findViewById(R.id.tvOne) as TextView
    val txtDone = dialog.findViewById(R.id.tvDone) as TextView

//    txtDone.text = getString(R.string.continue_btn)
    tvTitle.visibility = View.GONE
//        tvTitle.text = context!!.getString(R.string.label_exit)
    tvOne.text = message

    txtDone.setOnClickListener {
        dialog.dismiss()
        context.finish()
    }
    dialog.show()

}


fun BottomNavigationView.active(position: Int) {
    menu.getItem(position).isChecked = true
}