package com.sms.common

class AppConstants {

    companion object {
        const val SPLASH_TIME_INTERVAL = 2000L
        const val DEVICE_TYPE_VALUE = "2"
        const val SUBJECT_TYPE_STANDARD_VALUE = "0"
        const val SUBJECT_TYPE_INDIVIDUAL_VALUE = "1"
        const val REQUEST_CODE_ANSWER: Int = 101

        const val TAB_TYPE_ALL_QUESTION = "1"
        const val TAB_TYPE_MY_QUESTION = "2"
        const val TAB_TYPE_MY_ANSWER = "3"
    }
}