package com.sms.common

interface Constants {

    companion object {

        const val GET = "GET"
        const val POST = "POST"
        const val PUT = "PUT"
        const val DELETE = "DELETE"

        const val VIEW_TYPE_ITEM = 0
        const val VIEW_TYPE_LOADING = 1

        const val PleaseCheckInternetConnectionEng = "No Internet connection. Make sure that the Wifi or mobile data is turned ON, then try again"
        const val SOMETHING_WENT_WRONG = "Something went wrong, Please try again"
        //    String BASE_URL = "http://192.168.0.56/web_services/"; // local
//        const val BASE_URL = "http://lotshot.in/sm_system/api/" // live
//        const val BASE_URL = "http://spaarg.com/beta/sm_system/api/" // live
        const val BASE_URL = "https://cybershikshak.com/api/" // live new

        // Users
        const val POST_URL_LOGIN = "user/user_login"
        const val POST_URL_REGISTER = "user/user_signup"
        const val POST_URL_FORGOT_PASSWORD = "user/forget_password"
        const val POST_URL_CHANGE_PASSWORD = "user/change_password"
        const val POST_URL_USER_DETAILS = "user/profile_details"
        const val POST_URL_EDIT_PROFILE = "user/edit_profile/"
        const val GET_LOGOUT = "logout/id/"
        const val POST_MY_COURSE = "user/my_course"

        // Home Feeds
        const val POST_HOME_DASHBOARD = "home/dashboard"

        // Master
        const val POST_ADVERTISEMENT_LIST = "master/advertisement_list"
        const val POST_CLASS_LIST = "master/class_list"
        const val POST_SUBJECT_LIST = "master/subject_list"
        const val POST_CART_COUNT = "master/cart_count"

        // Home Works

        const val POST_HOME_WORK_LIST = "home_work/home_work_list"
        const val POST_HOME_WORK_DETAILS = "home_work/home_work_details"
        const val POST_HOME_WORK_QUE_ANS_FILL = "home_work/home_work_que_ans_fill"

        // Cart

        const val POST_ADD_TO_CART = "cart/add_to_cart"
        const val POST_REMOVE_CART = "cart/remove_cart"
        const val POST_CART_LIST = "cart/cart_list"
        const val POST_ADD_TO_CART_EXAM = "cart/add_to_cart_exam"

        // Payment

        const val POST_PROCESS_TO_CHECKOUT = "payment/process_to_checkout"
        const val POST_PAYMENT_HISTORY = "payment/payment_history"

        // Exam

        const val POST_EXAM_LIST = "exam/exam_list"
        const val POST_EXAM_SUBJECT_LIST = "exam/exam_subject_list"
        const val POST_EXAM_START = "exam/exam_start"
        const val POST_EXAM_QUESTION_ATTEMPT_MULTIPLE = "exam/question_attempt_multiple"

        // question and answer
        const val POST_ASK_QUESTION = "question/ask_question"
        const val POST_ANSWER = "question/answer"
        const val POST_QUESTION_LIST= "question/list"
        const val POST_QUESTION_DETAILS = "question/details"

        const val POST_NOTIFICATION_LIST = "notification/list"
        const val POST_NOTIFICATION_REPLY = "notification/reply"

        const val POST_PARENT_PROFILE_DETAILS = "parent/profile_details/"
        const val POST_PARENT_EDIT_PROFILE = "parent/edit_profile/"
        const val POST_PARENT_EXAM_LIST = "parent/exam_list/"

        const val W_LOGIN = "login"
        const val W_REGISTER = "register"
        const val W_LOGOUT = "logout"
        const val W_FORGOT_PASSWORD = "forgotPassword"
        const val W_CHANGE_PASSWORD = "changePassword"
        const val W_USER_DETAILS = "profileDetails"
        const val W_URL_EDIT_PROFILE = "editProfile"
        const val W_GET_LOGOUT = "logout"
        const val W_OTHER_SETTINGS = "otherSettings"
        const val W_EDIT_PROFILE = "editProfile"

        const val W_HOME_DASHBOARD = "dashboard"
        const val W_ADVERTISEMENT_LIST = "advertisementList"
        const val W_CLASS_LIST = "classList"
        const val W_SUBJECT_LIST = "subjectList"
        const val W_CART_COUNT = "cartCount"

        const val W_HOME_WORK_LIST = "homeWorkList"
        const val W_HOME_WORK_DETAILS = "homeWorkDetails"
        const val W_HOME_WORK_QUE_ANS_FILL = "homeWorkQueAnsFill"

        const val W_ADD_TO_CART = "addToCart"
        const val W_REMOVE_CART = "removeCart"
        const val W_CART_LIST = "cartList"
        const val W_ADD_TO_CART_EXAM = "addToCartExam"

        const val W_PROCESS_TO_CHECKOUT = "processToCheckout"
        const val W_PAYMENT_HISTORY = "paymentHistory"

        const val W_EXAM_LIST = "examList"
        const val W_EXAM_SUBJECT_LIST = "examSubjectList"
        const val W_EXAM_START = "examStart"
        const val W_EXAM_QUESTION_ATTEMPT_MULTIPLE = "questionAttemptMultiple"
        const val W_MY_COURSE = "myCourse"

        const val W_ASK_QUESTION = "askQuestion"
        const val W_ANSWER = "answer"
        const val W_QUESTION_LIST= "answerList"
        const val W_QUESTION_DETAILS = "questionDetails"

        const val W_NOTIFICATION_LIST = "notificationList"
        const val W_NOTIFICATION_REPLY = "notificationReply"

        const val W_PARENT_EXAM_LIST = "parent_exam_list"

    }
}