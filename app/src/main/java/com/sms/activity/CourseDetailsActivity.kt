package com.sms.activity

import android.Manifest
import android.app.ProgressDialog
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import com.alcohol.core.BaseActivity
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.sms.R
import com.sms.activity.MainActivity.Companion.bottomNavigation
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_ADD_TO_CART
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.common.Utils.Companion.getCipherEncPath
import com.sms.custom.FileList
import com.sms.model.common.CommonResponse
import com.sms.model.home.CourseDetailsItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_course_details.*
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.SecretKeySpec


class CourseDetailsActivity : BaseActivity() {

    lateinit var courseDetails: CourseDetailsItem
    private var TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_details)

        if (intent.extras != null) {
            courseDetails = intent.extras.getSerializable("courseDetails") as CourseDetailsItem

            if (courseDetails != null) {
                // set datas
                tvCourseNameCourseDetails.text = courseDetails.name
                tvFileNameCourseDetails.text = courseDetails.subjectName
                tvCourseUploadedBy.text = courseDetails.courseCreatedBy
                tvDownloadCountCourseDetails.text = courseDetails.totalDownload // courseDetail.
                tvCoursesDescription.text = courseDetails.description

                if (isCourseDownloaded(courseDetails.document)) {
                    tvPurchase.text = getString(R.string.go_to_my_course)
                } else if (courseDetails.isFree == "0") {
                    if (courseDetails.isPurchased == "1") {
                        // free course
                        tvCoursePriceCourseDetails.text = "Free"
                        tvPurchase.text = getString(R.string.download)
                    } else {
                        // paid course
                        tvCoursePriceCourseDetails.text = getString(R.string.symbol_rupee) + " " + courseDetails.price
                        tvPurchase.text = getString(R.string.add_to_cart)
                    }
                } else if (courseDetails.isFree == "1") {
                    // free course
                    tvCoursePriceCourseDetails.text = "Free"
                    tvPurchase.text = getString(R.string.download)
                }

                if (!courseDetails.image.isNullOrBlank()) {
                    Glide.with(this@CourseDetailsActivity).load(courseDetails.image).into(ivCourseImageCourseDetails)
                }

                try {
                    val extension = courseDetails.document.substring(courseDetails.document.lastIndexOf(".") + 1)
                    when (extension.toLowerCase()) {
                        "pdf" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.ic_pdf)
                        }
                        "doc" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.ic_doc)
                        }
                        "docx" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.ic_doc)
                        }
                        "xls" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.ic_xls)
                        }
                        "xlsx" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.ic_xls)
                        }
                        "ppt" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.ic_ppt)
                        }
                        "pptx" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.ic_ppt)
                        }
                        "epub" -> {
                            ivCourseFileTypeIcon.setImageResource(R.drawable.epub)
                        }
                    }
                    courseRating.rating = courseDetails.ratings.toFloat()

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        }

        tvPurchase.setOnClickListener {

            checkFilePermission()

        }

        tvBackCourseDetails.setOnClickListener {
            finish()
        }

    }

    private fun isCourseDownloaded(document: String): Boolean {

        if (document.isNullOrBlank()) {
            return false
        } else {

//            var fileName = document!!.substring(document!!.lastIndexOf('/') + 1, document.length)

            var fileName = courseDetails.name + ".epub"

            Logg.e(TAG, "fileName ====> $fileName")

//            var downloadFolder = Environment.getExternalStorageDirectory().toString() + "" + File.separator + "SMS/"

            var downloadFolder = Environment.getExternalStorageDirectory().toString() + File.separator + "Android/data/" + packageName + File.separator + "document/";

            Logg.e(TAG, "downloadFolder ====> $downloadFolder")

            val directory = File(downloadFolder)

            if (!directory.exists()) {
                Logg.e(TAG, "<===== directory not exist =====>")
                directory.mkdirs()
            }

            try {
                var fileList = FileList(downloadFolder)
                var filesList = fileList.filesList

//            var filesList = ArrayList(Arrays.asList(directory.listFiles()))
                Logg.e(TAG, "filesList size ===> ${filesList.size}")

                for (i in 0 until filesList.size) {
                    Logg.e(TAG, "filename ===> $fileName")
                    Logg.e(TAG, "filesList[i] ====> " + filesList[i])
                    var tempFile = fileName
                    tempFile = tempFile.replace(".epub", ".ciphernenc")
                    Logg.e("=========>", filesList[i].toString() + " vs " + tempFile)
                    if (filesList[i].toString() == tempFile) {
                        return true
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return false
        }
    }

    private fun addToCartAPI(courseId: String, courseVersionId: String, coursePrice: String) {

        if (APIUtils.isOnline(this@CourseDetailsActivity)) {
            showProgressDialog(this@CourseDetailsActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@CourseDetailsActivity)
            dataMap[WSKey.COURSE_ID] = courseId
            dataMap[WSKey.COURSE_VERSION_ID] = courseVersionId
            dataMap[WSKey.PRICE] = coursePrice
            apiHelper!!.callWB(Constants.POST_ADD_TO_CART, this@CourseDetailsActivity, dataMap, Constants.W_ADD_TO_CART, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@CourseDetailsActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_ADD_TO_CART -> {
                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    if (commonResponse.message == "Your book has been added to cart successfully") {
                        Toast.showLongToast(this@CourseDetailsActivity, commonResponse.message)
                        finish()
                    } else {
                        Toast.showLongToast(this@CourseDetailsActivity, commonResponse.message)
                    }

                } else {
                    Toast.showLongToast(this@CourseDetailsActivity, commonResponse.message)
                }
            }
        }

    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@CourseDetailsActivity, getString(R.string.meesage_connection_timeout))
    }

    inner class DownloadFile : AsyncTask<String, String, String>() {

        private var progressDialog: ProgressDialog? = null
        private var fileName: String? = null
        private var folder: String? = null
        private var isDownloaded: Boolean = false

        override fun onPreExecute() {
            super.onPreExecute()

            progressDialog = ProgressDialog(this@CourseDetailsActivity)
            progressDialog?.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
            progressDialog?.progress = 0
            progressDialog?.setCancelable(false)
            progressDialog?.show()

        }

        override fun onProgressUpdate(vararg values: String?) {
            super.onProgressUpdate(*values)

            progressDialog?.progress = values[0]?.toInt()!!
        }

        override fun doInBackground(vararg f_url: String?): String? {

            var input: InputStream? = null
            var output: OutputStream? = null
            var connection: HttpURLConnection? = null
            try {
                val url = URL(f_url[0])
                Log.e(TAG, "URL====> $url")
//                if (f_url[0]!!.contains("https")) {
//
//                } else {
//
//                }
                connection = url.openConnection() as HttpURLConnection
                connection.connectTimeout = 30000
                connection.readTimeout = 30000
                connection.requestMethod = "GET"
                connection!!.connect()

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection!!.responseCode != HttpURLConnection.HTTP_OK) {
                    return ("Server returned HTTP " + connection!!.responseCode
                            + " " + connection!!.responseMessage)
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length

//                fileName = f_url[0]!!.substring(f_url[0]!!.lastIndexOf('/') + 1, f_url[0]!!.length)
                fileName = courseDetails.name + ".epub"

                val fileLength = connection!!.contentLength

//                folder = Environment.getExternalStorageDirectory().toString() + "" + File.separator + "SMS/"
                folder = Environment.getExternalStorageDirectory().toString() + File.separator + "Android/data/" + packageName + File.separator + "document/";

                val directory = File(folder)

                if (!directory.exists()) {
                    Logg.e(TAG, "Download file directory not exist ====> ")
                    directory.mkdirs()
                }

                // download the file
                input = connection!!.inputStream
//                output = FileOutputStream("/sdcard/SMS/$fileName")
                output = FileOutputStream(folder + fileName)

                val data = ByteArray(4096)
                var total: Long = 0
                var count: Int = 0
                while (count != -1) {
                    count = input!!.read(data)
                    // allow canceling with back button
                    if (isCancelled) {
                        input!!.close()
                        return null
                    }
                    total += count.toLong()
                    // publishing the progress....
                    if (fileLength > 0) {
                        // only if total length is known
                        publishProgress((total * 100 / fileLength).toInt().toString())
                    }

                    output!!.write(data, 0, count)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return e.toString()
            } finally {
                try {
                    if (output != null)
                        output!!.close()
                    if (input != null)
                        input!!.close()
                } catch (ignored: IOException) {
                    ignored.printStackTrace()
                }

                if (connection != null)
                    connection!!.disconnect()

                return "Downloaded"
            }

            return "Something went wrong"

        }

        override fun onPostExecute(message: String?) {
            super.onPostExecute(message)

            /*if (progressDialog?.isShowing!!) {
                progressDialog?.dismiss()
            }

            if (message == "Downloaded") {
                tvPurchase.text = getString(R.string.go_to_my_course)

                // Display File path after downloading
                Toast.showLongToast(this@CourseDetailsActivity, getString(R.string.course_is_downloaded))
//                android.widget.Toast.makeText(this@CourseDetailsActivity, message, android.widget.Toast.LENGTH_LONG).show()
            } else {
                Toast.showLongToast(this@CourseDetailsActivity, message.toString())
            }*/

            try {
                cipherEncFile(folder + "/" + fileName, progressDialog, message)
            } catch (e: IOException) {
                if (progressDialog?.isShowing!!) {
                    progressDialog?.dismiss()
                }
                e.printStackTrace()
            } catch (e: NoSuchAlgorithmException) {
                if (progressDialog?.isShowing!!) {
                    progressDialog?.dismiss()
                }
                e.printStackTrace()
            } catch (e: NoSuchPaddingException) {
                if (progressDialog?.isShowing!!) {
                    progressDialog?.dismiss()
                }
                e.printStackTrace()
            } catch (e: InvalidKeyException) {
                if (progressDialog?.isShowing!!) {
                    progressDialog?.dismiss()
                }
                e.printStackTrace()
            }
        }
    }

    private fun checkFilePermission() {

        val permissionlistener = object : PermissionListener {
            override fun onPermissionGranted() {

                // process file

                processFileDownloadOrView()

            }

            override fun onPermissionDenied(deniedPermissions: ArrayList<String>) {
            }
        }

        TedPermission.with(this@CourseDetailsActivity)
                .setPermissionListener(permissionlistener)
//                .setRationaleMessage("we need permission to access your external storage")
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check()
    }

    private fun processFileDownloadOrView() {

        if (courseDetails.isFree == "0") {
            // paid course

            if(courseDetails.isPurchased == "1") {
                // free course

                if (isCourseDownloaded(courseDetails.document)) {

                    finish()
//                    bottomNavigation.selectedItemId = R.id.my_courses

                    Handler().postDelayed(
                            {
                                var view: View = bottomNavigation.findViewById(R.id.my_courses)
                                view.performClick()
                            }, 50)

                } else if (courseDetails.document.isNullOrBlank()) {
                    Toast.show(this@CourseDetailsActivity, getString(R.string.message_no_document_found))
                } else {
                    DownloadFile().execute(courseDetails.document)
                }
            } else {
                addToCartAPI(courseDetails.id, courseDetails.courseVersionId, courseDetails.price)
            }
        } else if (courseDetails.isFree == "1") {
            // free course, download it

            if (isCourseDownloaded(courseDetails.document)) {

                finish()
//                    bottomNavigation.selectedItemId = R.id.my_courses

                Handler().postDelayed(
                        {
                            var view: View = bottomNavigation.findViewById(R.id.my_courses)
                            view.performClick()
                        }, 50)

            } else if (courseDetails.document.isNullOrBlank()) {
                Toast.show(this@CourseDetailsActivity, getString(R.string.message_no_document_found))
            } else {
                DownloadFile().execute(courseDetails.document)
            }

        }
    }

    @Throws(IOException::class, NoSuchAlgorithmException::class, NoSuchPaddingException::class, InvalidKeyException::class)
    internal fun cipherEncFile(file_name: String, progressDialog: ProgressDialog?, message: String?) {

        val fis = FileInputStream(file_name)
        val enc_file_name = getCipherEncPath(file_name)
        val fos = FileOutputStream(enc_file_name)
        val sks = SecretKeySpec("NirCipher456DEnc".toByteArray(), "AES")
        val cipher = Cipher.getInstance("AES")
        cipher.init(Cipher.ENCRYPT_MODE, sks)
        val cos = CipherOutputStream(fos, cipher)
        var b: Int = 0
        val data = ByteArray(1024 * 1024)
        while ({ b = fis.read(data);b }() != -1) {
            cos.write(data, 0, b)
        }
        cos.flush()
        cos.close()
        fis.close()

        val actual_file = File(file_name)
        actual_file.delete()

        if (progressDialog?.isShowing!!) {
            progressDialog?.dismiss()
        }

        if (message == "Downloaded") {
            tvPurchase.text = getString(R.string.go_to_my_course)
            Toast.showLongToast(this@CourseDetailsActivity, getString(R.string.course_is_downloaded))
        } else {
            Toast.showLongToast(this@CourseDetailsActivity, message.toString())
        }
    }
}
