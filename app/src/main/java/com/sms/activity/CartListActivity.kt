package com.sms.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.widget.TextView
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.CartListAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_CART_LIST
import com.sms.common.Constants.Companion.W_REMOVE_CART
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.merchant.AvenuesParams
import com.sms.merchant.Constants.ACCESS_CODE
import com.sms.merchant.Constants.MERCHANT_ID
import com.sms.merchant.WebViewActivity
import com.sms.model.cart.CartListResponse
import com.sms.model.common.CommonResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_cart.*
import java.util.HashMap

class CartListActivity : BaseActivity() {

    private var TAG: String = javaClass.simpleName
    private var totalPrice = ""
    private lateinit var cartListResponse: CartListResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        var linearLayoutManager = LinearLayoutManager(this@CartListActivity)
        rcvCartList.layoutManager = linearLayoutManager
        rcvCartList.addItemDecoration(DividerItemDecoration(this@CartListActivity, DividerItemDecoration.VERTICAL))


        tvBackCart.setOnClickListener {
            finish()
        }

        llPlaceOrder.setOnClickListener {
            if (cartListResponse.cartList != null && cartListResponse.cartList.size > 0) {
                showConfirmPlaceOrderDiaog(this@CartListActivity)
            } else {
                showEmptyCartDiaog(this@CartListActivity)
            }

        }

        getCartListAPI()
    }

    private fun showConfirmPlaceOrderDiaog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_custom)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        val tvOne = dialog.findViewById(R.id.tvOne) as TextView
        val txtClose = dialog.findViewById(R.id.tvClose) as TextView
        val txtDone = dialog.findViewById(R.id.tvDone) as TextView

        txtDone.text = getString(R.string.continue_btn)
        txtClose.text = getString(R.string.cancel)
        tvTitle.visibility = View.GONE
//        tvTitle.text = context!!.getString(R.string.label_exit)
        tvOne.text = context!!.getString(R.string.confirm_submit_place_order)

        txtClose.setOnClickListener { dialog.dismiss() }

        txtDone.setOnClickListener {
            dialog.dismiss()
            redirectToCcavanue()
        }
        dialog.show()
    }

    private fun showEmptyCartDiaog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_custom)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        val tvOne = dialog.findViewById(R.id.tvOne) as TextView
        val txtClose = dialog.findViewById(R.id.tvClose) as TextView
        val txtDone = dialog.findViewById(R.id.tvDone) as TextView

        txtDone.text = getString(R.string.lable_OK)
        txtClose.text = getString(R.string.cancel)
        tvTitle.visibility = View.GONE
//        tvTitle.text = context!!.getString(R.string.label_exit)
        tvOne.text = context!!.getString(R.string.cart_is_empty)

        txtClose.visibility = View.GONE

        txtDone.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun redirectToCcavanue() {

        val intent = Intent(this@CartListActivity, WebViewActivity::class.java)
//        intent.putExtra(AvenuesParams.ACCESS_CODE, "AVOF65DF78BQ38FOQB")
//        intent.putExtra(AvenuesParams.MERCHANT_ID, "101970")
        intent.putExtra(AvenuesParams.ACCESS_CODE, ACCESS_CODE)
        intent.putExtra(AvenuesParams.MERCHANT_ID, MERCHANT_ID)
        intent.putExtra(AvenuesParams.ORDER_ID, "1")
        intent.putExtra(AvenuesParams.CURRENCY, "INR")
        intent.putExtra(AvenuesParams.AMOUNT, totalPrice)
//        intent.putExtra(AvenuesParams.AMOUNT, "1")

        intent.putExtra(AvenuesParams.REDIRECT_URL, "http://germaniuminc.com/merchant/ccavResponseHandler.php")
        intent.putExtra(AvenuesParams.CANCEL_URL, "http://germaniuminc.com/merchant/ccavResponseHandler.php")
        intent.putExtra(AvenuesParams.RSA_KEY_URL, "http://germaniuminc.com/merchant/GetRSA.php")

//        intent.putExtra(AvenuesParams.REDIRECT_URL, "http://143.95.252.45/merchant/ccavResponseHandler.php")
//        intent.putExtra(AvenuesParams.CANCEL_URL, "http://143.95.252.45/merchant/ccavResponseHandler.php")
//        intent.putExtra(AvenuesParams.RSA_KEY_URL, "http://143.95.252.45/merchant/GetRSA.php")

//        intent.putExtra(AvenuesParams.REDIRECT_URL, "http://spaarg.com/beta/sm_system/ccavenue/merchant/ccavResponseHandler.php")
//        intent.putExtra(AvenuesParams.CANCEL_URL, "http://spaarg.com/beta/sm_system/ccavenue/merchant/ccavResponseHandler.php")
//        intent.putExtra(AvenuesParams.RSA_KEY_URL, "http://spaarg.com/beta/sm_system/ccavenue/merchant/GetRSA.php")


        startActivity(intent)

    }

    private fun getCartListAPI() {

        if (APIUtils.isOnline(this@CartListActivity)) {
            showProgressDialog(this@CartListActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@CartListActivity)
            apiHelper!!.callWB(Constants.POST_CART_LIST, this@CartListActivity, dataMap, Constants.W_CART_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@CartListActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_CART_LIST -> {
                val gson = Gson()
                cartListResponse = gson.fromJson<CartListResponse>(mRes.toString(), CartListResponse::class.java!!)

                if (cartListResponse.flag) {
                    totalPrice = cartListResponse.totalCart.toString()

                    tvCartTotal.text = getString(R.string.symbol_rupee) + "" + totalPrice
                    tvNoOrdersFound.visibility = View.GONE
                    cvCart.visibility = View.VISIBLE

                    if (cartListResponse.cartList != null && cartListResponse.cartList.size > 0) {
                        if (cartListResponse.cartList.size == 1) {
                            tvCartTotalItems.text = cartListResponse.cartList.size.toString() + " " + getString(R.string.item)
                        } else {
                            tvCartTotalItems.text = cartListResponse.cartList.size.toString() + " " + getString(R.string.items)
                        }

                        var allOrdersList = cartListResponse.cartList
                        var cartListAdapter = CartListAdapter(this@CartListActivity, allOrdersList!!)
                        rcvCartList.adapter = cartListAdapter
                    }

                } else {
                    tvCartTotal.text = getString(R.string.symbol_rupee) + "0"
                    tvCartTotalItems.text = "0 " + getString(R.string.items)
                    tvNoOrdersFound.visibility = View.VISIBLE
                    cvCart.visibility = View.GONE
                }
            }
            W_REMOVE_CART -> {
                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    Toast.showLongToast(this@CartListActivity, commonResponse.message)
                    getCartListAPI()
                } else {
                    Toast.showLongToast(this@CartListActivity, commonResponse.message)
                }
            }
        }

    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@CartListActivity, getString(R.string.meesage_connection_timeout))
    }
}
