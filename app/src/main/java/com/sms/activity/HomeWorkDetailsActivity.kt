package com.sms.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.CoursesAdapter
import com.sms.adapters.HomeWorkAdapter
import com.sms.common.AppConstants
import com.sms.common.Constants
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.loadmore.OnLoadMoreListener
import com.sms.loadmore.RecyclerViewLoadMoreScroll
import com.sms.model.home.AdvertiseResponse
import com.sms.model.homework.HomeWorkDetailsResponse
import com.sms.model.homework.HomeWorkListItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_home_work_details.*
import kotlinx.android.synthetic.main.activity_home_work_list.*
import java.util.ArrayList
import java.util.HashMap

class HomeWorkDetailsActivity : BaseActivity() {

    private var TAG: String = javaClass.simpleName

    var scrollListenerList: RecyclerViewLoadMoreScroll? = null
    //    var scrollListenerList: GridLayoutManager? = null
//    var coursesAdapter: CoursesAdapter? = null

    var userQuestionsList = ArrayList<HomeWorkListItem>()

    var pageIndex: Int = 0
    var totalPage: Int = 0

    lateinit var homeWorkDetail: HomeWorkListItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_work_details)

        if (intent.extras != null) {
            homeWorkDetail = intent.extras.getSerializable("homeWorkDetail") as HomeWorkListItem

            tvHomeWorkName.text = homeWorkDetail.name
        }

        var linearLayoutManager = LinearLayoutManager(this@HomeWorkDetailsActivity)
        rcvHomeWorkDetails.layoutManager = linearLayoutManager
//        rcvHomeWorkDetails.addItemDecoration(DividerItemDecoration(this@HomeWorkDetailsActivity, DividerItemDecoration.VERTICAL))


        scrollListenerList = RecyclerViewLoadMoreScroll(linearLayoutManager!!)
        scrollListenerList!!.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreDataList()
            }
        })

        rcvHomeWorkDetails.addOnScrollListener(scrollListenerList!!)

        tvBackHomeWorkDetails.setOnClickListener {
            finish()
        }

        getHomeWorkDetailsAPI(pageIndex)

//        getStaticHomeWorkData()
    }

    private fun loadMoreDataList() {


    }

//    private fun getStaticHomeWorkData() {
//
//        var homeWorkListItem = HomeWorkListItem()
//
//        homeWorkListItem.questionId = "1"
//        homeWorkListItem.question = "Describe the Nutrition in Plants VS Nutrition in Animals, in Details ?"
//        homeWorkListItem.questionStatus = "pending"
//
//        userQuestionsList.add(homeWorkListItem)
//
//        var homeWorkListItem1 = HomeWorkListItem()
//
//        homeWorkListItem1.questionId = "2"
//        homeWorkListItem1.question = "The centre of cyclone is calm and is called the heart of the cyclone mouth, is it true ?"
//        homeWorkListItem1.questionStatus = "pending"
//
//        userQuestionsList.add(homeWorkListItem1)
//
//        var homeWorkListItem2 = HomeWorkListItem()
//
//        homeWorkListItem2.questionId = "3"
//        homeWorkListItem2.question = "Water changes to vapour by the process, What's the name of that process and write in brief?"
//        homeWorkListItem2.questionStatus = "completed"
//
//        userQuestionsList.add(homeWorkListItem2)
//
//        var homeWorkListItem3 = HomeWorkListItem()
//
//        homeWorkListItem3.questionId = "4"
//        homeWorkListItem3.question = "Explain the the process of photosynthesis"
//        homeWorkListItem3.questionStatus = "pending"
//
//        userQuestionsList.add(homeWorkListItem3)
//
//        var homeWorkListItem4 = HomeWorkListItem()
//
//        homeWorkListItem4.questionId = "5"
//        homeWorkListItem4.question = "Describe the Nutrition in Plants VS Nutrition in Animals, in Details ?"
//        homeWorkListItem4.questionStatus = "completed"
//
//        userQuestionsList.add(homeWorkListItem4)
//
//        var homeWorkListItem5 = HomeWorkListItem()
//
//        homeWorkListItem5.questionId = "6"
//        homeWorkListItem5.question = "he centre of cyclone is calm and is called the heart of the cyclone mouth, is it true ?"
//        homeWorkListItem5.questionStatus = "pending"
//
//        userQuestionsList.add(homeWorkListItem5)
//
//        var homeWorkListItem6 = HomeWorkListItem()
//
//        homeWorkListItem6.questionId = "7"
//        homeWorkListItem6.question = "Water changes to vapour by the process, What's the name of that process and write in brief?"
//        homeWorkListItem6.questionStatus = "pending"
//
//        userQuestionsList.add(homeWorkListItem6)
//
//        var homeWorkListItem7 = HomeWorkListItem()
//
//        homeWorkListItem7.questionId = "8"
//        homeWorkListItem7.question = "Explain the the process of photosynthesis"
//        homeWorkListItem7.questionStatus = "pending"
//
//        userQuestionsList.add(homeWorkListItem7)
//
//    }

    private fun getHomeWorkDetailsAPI(pageNo:Int) {

        if (APIUtils.isOnline(this@HomeWorkDetailsActivity)) {
            showProgressDialog(this@HomeWorkDetailsActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@HomeWorkDetailsActivity)
            dataMap[WSKey.PAGE_NO] = pageNo.toString()
            dataMap[WSKey.HOME_WORK_ID] = homeWorkDetail.homeWorkId
            apiHelper!!.callWB(Constants.POST_HOME_WORK_DETAILS, this@HomeWorkDetailsActivity, dataMap, Constants.W_HOME_WORK_DETAILS, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@HomeWorkDetailsActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            Constants.W_HOME_WORK_DETAILS -> {
                val gson = Gson()
                val homeWorkDetailsResponse = gson.fromJson<HomeWorkDetailsResponse>(mRes.toString(), HomeWorkDetailsResponse::class.java!!)

                if (homeWorkDetailsResponse.flag) {

                    if (homeWorkDetailsResponse.homeWorkQueList != null && homeWorkDetailsResponse.homeWorkQueList.size > 0) {
                        rcvHomeWorkDetails.adapter = HomeWorkAdapter(this@HomeWorkDetailsActivity, homeWorkDetailsResponse.homeWorkQueList)
                    }
                }
            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@HomeWorkDetailsActivity, getString(R.string.meesage_connection_timeout))
    }

    override fun onDestroy() {
        super.onDestroy()
        hideProgressDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppConstants.REQUEST_CODE_ANSWER) {
                pageIndex = 0

                getHomeWorkDetailsAPI(pageIndex)
            }
        }
    }
}
