package com.sms.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.sms.R
import com.sms.adapters.TestReviewListAdapter
import com.sms.model.examresult.ExamResultResponse
import com.sms.model.examstart.ExamQuestionItem
import kotlinx.android.synthetic.main.activity_exam_start.*
import kotlinx.android.synthetic.main.activity_test_review.*

class TestReviewActivity : AppCompatActivity() {

    private var TAG: String = javaClass.simpleName
    private lateinit var examResultResponse: ExamResultResponse
    private lateinit var examQuestions: ArrayList<ExamQuestionItem>

    private lateinit var correctAnswerCount: String
    private lateinit var incorrectAnswerCount: String
    private lateinit var notAttemptCount: String
    private lateinit var chapterNumber: String

    private lateinit var chapterName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_review)

        tvBackTestReview.setOnClickListener {
            finish()
        }

        if (intent.extras != null) {
            examResultResponse = intent.extras.getSerializable("examResultResponse") as ExamResultResponse
            examQuestions = intent.extras.getSerializable("examQuestions") as ArrayList<ExamQuestionItem>
            correctAnswerCount = intent.extras.getString("correctAnswerCount")
            incorrectAnswerCount = intent.extras.getString("incorrectAnswerCount")
            notAttemptCount = intent.extras.getString("notAttemptCount")
            chapterNumber = intent.extras.getString("chapterNumber")
            chapterName = intent.extras.getString("chapterName")

            tvChapterNumberExamTestReview.text = chapterNumber

            tvChapterNameExamTestReview.text = chapterName

            tvCurrentOutOfTotalTestReview.text = examQuestions.size.toString()

            tvCorrectAnswersTotalTestReview.text = correctAnswerCount

            tvIncorrectAnswersTotalTestReview.text = incorrectAnswerCount

            tvNotAttemptedTestReview.text = notAttemptCount

            rcvExamResultList.layoutManager = LinearLayoutManager(this@TestReviewActivity, LinearLayoutManager.VERTICAL, false)

            val testReviewListAdapter = TestReviewListAdapter(this@TestReviewActivity, examQuestions)

            rcvExamResultList.adapter = testReviewListAdapter

        }
    }
}
