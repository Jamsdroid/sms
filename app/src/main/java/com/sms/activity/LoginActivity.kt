package com.sms.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.alcohol.core.BaseActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.sms.R
import com.sms.common.*
import com.sms.common.AppConstants.Companion.DEVICE_TYPE_VALUE
import com.sms.common.Constants.Companion.POST
import com.sms.common.Constants.Companion.POST_URL_LOGIN
import com.sms.common.Constants.Companion.W_LOGIN
import com.sms.model.authentication.LoginDetailsItem
import com.sms.model.authentication.LoginResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.storage.PrefsKey.Companion.IS_LOGIN
import com.sms.webservice.RetroFitResponse
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.util.*

class LoginActivity : BaseActivity(), RetroFitResponse, Constants {

    private val TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        tvForgotPassword.setOnClickListener {
            var intentForgotPassword = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
            startActivity(intentForgotPassword)
        }

        tvCreateAccount.setOnClickListener {
            var intentRegister = Intent(this@LoginActivity, RegisterActivity::class.java)
            startActivity(intentRegister)
        }

        llLogin.setOnClickListener {

            hideKeyboard(this@LoginActivity)
            val email = edtUserName.text.toString().trim()
            val password = edtPassword.text.toString().trim()
            var deviceToken = Prefs.getString(PrefsKey.DEVICE_TOKEN, "", this@LoginActivity)

            if (deviceToken.isNullOrBlank()) {
                FirebaseApp.initializeApp(this@LoginActivity)
                deviceToken = FirebaseInstanceId.getInstance().token.toString()
                Prefs.setString(PrefsKey.DEVICE_TOKEN, deviceToken, this@LoginActivity)
            }

            if (validateInput(email, password)) {
                val dataMap = HashMap<String, String>()
                dataMap[WSKey.EMAIL] = email
                dataMap[WSKey.PASSWORD] = password
                dataMap[WSKey.DEVICE_TYPE] = DEVICE_TYPE_VALUE
                dataMap[WSKey.DEVICE_TOKEN] = deviceToken
                apiHelper.callWB(POST_URL_LOGIN, this@LoginActivity, dataMap, W_LOGIN, this@LoginActivity, true, false, true, POST)
            }

        }
    }

    private fun validateInput(email: String, password: String): Boolean {

        when {
            TextUtils.isEmpty(email) -> {
                Toast.show(this, resources.getString(R.string.empty_email))
                return false
            }
            !isEmailValid(email) -> {
                Toast.show(this, resources.getString(R.string.validate_email))
                return false
            }
            TextUtils.isEmpty(password) -> {
                Toast.show(this, resources.getString(R.string.empty_password))
                return false
            }
            else -> {
                return true
            }
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {

        try {
            if (!mRes.toString().isNullOrBlank()) {

                Logg.e(TAG, responseTag + " Response " + mRes.toString())
                val responseObject = JSONObject(mRes.toString())
//                if (responseObject.optInt("status_code") == 400) {
//                    if (responseObject.has("errors")) {
//                        val errorObject = responseObject.getJSONObject("errors")
//
//                        val iter = errorObject.keys()
//                        while (iter.hasNext()) {
//                            val key = iter.next() as String
//                            val value = errorObject.getString(key)
//                            Toast.show(this@LoginActivity, value)
//                            break
//                        }
//                    }
//                } else if (responseObject.optInt("status_code") == 200) {
                if (responseTag.equals(Constants.W_LOGIN, ignoreCase = true)) {
                    println("mRes.toString()---->" + mRes.toString())
                    Logg.e(TAG, responseTag + " Response " + mRes.toString())

                    val responseObject = JSONObject(mRes.toString())

                    val gson = Gson()
                    val modelLogin = gson.fromJson<LoginResponse>(responseObject.toString(), LoginResponse::class.java!!)

                    if (modelLogin.flag) {

                        val loginDetailsItemList: ArrayList<LoginDetailsItem> = modelLogin!!.loginDetails

                        Toast.showLongToast(this@LoginActivity, getString(R.string.message_login_success))
                        val token = loginDetailsItemList[0].deviceToken
                        val customerId = loginDetailsItemList[0]?.id

                        // save to sharedPrefs
                        Prefs.setBoolean(IS_LOGIN, true, this@LoginActivity)
                        if (customerId != null) {
                            Prefs.setString(PrefsKey.ID, customerId, this@LoginActivity)
                        }
                        if (token != null) {
                            Prefs.setString(PrefsKey.DEVICE_TOKEN, token, this@LoginActivity)
                        }

                        Prefs.setString(PrefsKey.EMAIL, loginDetailsItemList[0]!!.email, this@LoginActivity)
                        Prefs.setString(PrefsKey.FIRST_NAME, loginDetailsItemList[0]!!.firstName, this@LoginActivity)
                        Prefs.setString(PrefsKey.DATE_OF_BIRTH, loginDetailsItemList[0]!!.dateOfBirth, this@LoginActivity)
                        Prefs.setString(PrefsKey.LAST_NAME, loginDetailsItemList[0]!!.lastName, this@LoginActivity)
                        Prefs.setString(PrefsKey.PROFILE_IMAGE, loginDetailsItemList[0]!!.profileImage, this@LoginActivity)
                        Prefs.setString(PrefsKey.USER_TYPE, loginDetailsItemList[0]!!.userType, this@LoginActivity)
                        Prefs.setString(PrefsKey.CLASS_ID, loginDetailsItemList[0]!!.classID, this@LoginActivity)

//                        startActivity(Intent(this@LoginActivity, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK))

                        // 0 - Student and 1 - Parent
                        if (loginDetailsItemList[0].userType == "0") {
                            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            finish()
                        } else {
                            startActivity(Intent(this@LoginActivity, ParentHomeActivity::class.java))
                            finish()
                        }


                    } else {
                        Toast.showLongToast(this@LoginActivity, modelLogin.message)
                    }

                }
//                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {

        Toast.showLongToast(this@LoginActivity, getString(R.string.meesage_connection_timeout))
    }
}
