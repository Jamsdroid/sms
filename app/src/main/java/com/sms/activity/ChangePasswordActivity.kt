package com.sms.activity

import android.os.Bundle
import android.text.TextUtils
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_CHANGE_PASSWORD
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.common.Utils
import com.sms.model.common.CommonResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_change_password.*
import java.util.*

class ChangePasswordActivity : BaseActivity() {

    var TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        tvBackChangePassword.setOnClickListener {
            finish()
        }

        llSaveChangePassword.setOnClickListener {
            validateInput()
        }
    }

    private fun validateInput() {

        when {
            TextUtils.isEmpty(edtOldPassword.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_old_password))
            }
            TextUtils.isEmpty(edtNewPassword.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_new_password))
            }
            !Utils.validatePassword(edtNewPassword.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.validate_new_password))
            }
            TextUtils.isEmpty(edtConfirmNewPassword.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_confirm_password))
            }
            (edtNewPassword.text.toString() != edtConfirmNewPassword.text.toString()) -> {
                Toast.show(this, resources.getString(R.string.validate_both_password))
            }
            else -> {
                val dataMap = HashMap<String, String>()
                dataMap[WSKey.OLD_PASSWORD] = edtOldPassword.text.toString()
                dataMap[WSKey.NEW_PASSWORD] = edtNewPassword.text.toString()
                dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@ChangePasswordActivity)
                apiHelper.callWB(Constants.POST_URL_CHANGE_PASSWORD, this@ChangePasswordActivity, dataMap, Constants.W_CHANGE_PASSWORD, this@ChangePasswordActivity, true, false, true, Constants.POST)
            }
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_CHANGE_PASSWORD -> {
                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    finish()
                    Toast.showLongToast(this@ChangePasswordActivity, commonResponse.message)
                } else {
                    Toast.showLongToast(this@ChangePasswordActivity, commonResponse.message)
                }
            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@ChangePasswordActivity, getString(R.string.meesage_connection_timeout))
    }
}
