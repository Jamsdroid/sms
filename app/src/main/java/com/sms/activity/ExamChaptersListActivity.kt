package com.sms.activity

import android.os.Bundle
import android.view.View
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.ExamChapterListAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.POST_EXAM_SUBJECT_LIST
import com.sms.common.Constants.Companion.W_EXAM_SUBJECT_LIST
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.exam.ExamChaptersResponse
import com.sms.model.exam.ExamListItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_exam_chapters_list.*
import java.util.*

class ExamChaptersListActivity : BaseActivity() {

    private lateinit var examListItem: ExamListItem
    private var TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_chapters_list)

        if (intent.extras != null) {
            examListItem = intent.extras.getSerializable("examListItem") as ExamListItem

            tvCourseNameExamChapter.text = examListItem?.name
        }

        tvBackExamChapter.setOnClickListener {
            finish()
        }

        getChaptersListWS()

    }

    private fun getChaptersListWS() {

        if (APIUtils.isOnline(this@ExamChaptersListActivity)) {
            showProgressDialog(this@ExamChaptersListActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@ExamChaptersListActivity)
            dataMap[WSKey.SUBJECT_ID] = examListItem?.subjectId.toString()
            dataMap[WSKey.CLASS_ID] = examListItem?.classId.toString()
            apiHelper!!.callWB(POST_EXAM_SUBJECT_LIST, this@ExamChaptersListActivity, dataMap, W_EXAM_SUBJECT_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@ExamChaptersListActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_EXAM_SUBJECT_LIST -> {

                val gson = Gson()
                val examChaptersResponse = gson.fromJson<ExamChaptersResponse>(mRes.toString(), ExamChaptersResponse::class.java!!)

                if (examChaptersResponse.flag) {

                    if (examChaptersResponse.examList != null && examChaptersResponse.examList.size > 0) {
                        tvNoExamChapter.visibility = View.GONE
                        rcvExamChaptersList.visibility = View.VISIBLE
                        var examListAdapter = ExamChapterListAdapter(this@ExamChaptersListActivity, examChaptersResponse.examList, examListItem)
                        rcvExamChaptersList.adapter = examListAdapter
                    } else {
                        tvNoExamChapter.visibility = View.VISIBLE
                        rcvExamChaptersList.visibility = View.GONE
                    }

                } else {
                    Toast.showLongToast(this@ExamChaptersListActivity, examChaptersResponse.message)
                }

            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@ExamChaptersListActivity, getString(R.string.meesage_connection_timeout))
    }
}
