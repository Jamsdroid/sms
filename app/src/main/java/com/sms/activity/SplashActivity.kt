package com.sms.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Window
import android.view.WindowManager
//import com.crashlytics.android.Crashlytics
import com.sms.R
import com.sms.common.AppConstants
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import java.util.*

class SplashActivity : AppCompatActivity() {

    companion object {
        private const val TAG: String = "SplashActivity"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)

//        FirebaseCrash.report(Exception("My first Android non-fatal error"))

        // Log the onCreate event, this will also be printed in logcat
//        Crashlytics.log(Log.VERBOSE, TAG, "onCreate")
//
//        // Add some custom values and identifiers to be included in crash reports
//        Crashlytics.setInt("MeaningOfLife", 42)
//        Crashlytics.setString("LastUIAction", "Test value")
//        Crashlytics.setUserIdentifier("123456789")
//
//        // Report a non-fatal exception, for demonstration purposes
//        Crashlytics.logException(Exception("Non-fatal exception: something went wrong!"))


        val timer = Timer()
        val timerTask = object : TimerTask() {
            override fun run() {
//                startActivity(Intent(this@SplashActivity,LoginActivity ::class.java))
//                finish()
                loadNextScreen()
            }
        }
        timer.schedule(timerTask, AppConstants.SPLASH_TIME_INTERVAL)
    }

    fun loadNextScreen() {

        if (Prefs.getBoolean(PrefsKey.IS_LOGIN, false, this@SplashActivity)) {

            if (Prefs.getString(PrefsKey.USER_TYPE, "", this@SplashActivity) == "0") {
                // student
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            } else {
                // parent
                startActivity(Intent(this@SplashActivity, ParentHomeActivity::class.java))
            }


        } else {
            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
        }

//        if (TextUtils.isEmpty(Prefs.getString(Constants.SP_USERNAME, "", this@SplashActivity))) {
//            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
//        } else {
//            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
//        }
        finish()
    }
}
