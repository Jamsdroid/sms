package com.sms.activity

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.alcohol.core.BaseActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.ExamQuestionCountAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_EXAM_QUESTION_ATTEMPT_MULTIPLE
import com.sms.common.Constants.Companion.W_EXAM_START
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.common.displayOkDialog
import com.sms.model.exam.ExamChaptersItem
import com.sms.model.exam.ExamListItem
import com.sms.model.examresult.ExamResultResponse
import com.sms.model.examstart.ExamDataItem
import com.sms.model.examstart.ExamQuestionItem
import com.sms.model.examstart.ExamStartResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_exam_start.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class ExamStartActivity : BaseActivity() {

    private var TAG: String = javaClass.simpleName
    private var examChapterItem: ExamChaptersItem? = null
    private var examListItem: ExamListItem? = null

    private var examDataItem: ExamDataItem? = null
    private lateinit var examQuestions: ArrayList<ExamQuestionItem>
    public var index: Int = 0
    private var chapterNumber: String = ""

    private var countDownTimer: CountDownTimer? = null
    private var examTime: Int = 0
    private var dataList: ArrayList<String> = ArrayList()
    private lateinit var examQuestionCountAdapter: ExamQuestionCountAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_start)

        if (intent.extras != null) {
            examChapterItem = intent.extras.getSerializable("examChapterItem") as ExamChaptersItem
            examListItem = intent.extras.getSerializable("examListItem") as ExamListItem
            chapterNumber = intent.extras.getString("chapterNumber")

            tvCourseNameExamStart.text = examChapterItem?.examName
            tvChapterNumberExam.text = "Chapter #" + chapterNumber
        }

        rcvExamQuestionIndex.layoutManager = LinearLayoutManager(this@ExamStartActivity,
                LinearLayoutManager.HORIZONTAL, false)

        tvBackExamStart.setOnClickListener {
            onBackPressed()
        }

        llPrevQuestion.setOnClickListener {
            index--
            examQuestionCountAdapter = ExamQuestionCountAdapter(this@ExamStartActivity,
                    dataList)

            rcvExamQuestionIndex.adapter = examQuestionCountAdapter
            setQuestionsToViews(index)
        }

        llNextQuestion.setOnClickListener {
            index++
            examQuestionCountAdapter = ExamQuestionCountAdapter(this@ExamStartActivity,
                    dataList)

            rcvExamQuestionIndex.adapter = examQuestionCountAdapter
            setQuestionsToViews(index)
        }

        llOptionA.setOnClickListener {
            llOptionA.setBackgroundResource(R.drawable.rectange_bg_green)
            llOptionB.setBackgroundResource(R.color.transparent)
            llOptionC.setBackgroundResource(R.color.transparent)
            llOptionD.setBackgroundResource(R.color.transparent)
//            llOptionE.setBackgroundResource(R.color.transparent)

            examQuestions?.get(index)?.selectedAnswer = "1"

            if (examQuestions?.get(index)?.answer == "1") {
                examQuestions?.get(index)?.answerStatus = "1" // correct
            } else {
                examQuestions?.get(index)?.answerStatus = "2" // incorrect
            }

        }

        llOptionB.setOnClickListener {

            llOptionA.setBackgroundResource(R.color.transparent)
            llOptionB.setBackgroundResource(R.drawable.rectange_bg_green)
            llOptionC.setBackgroundResource(R.color.transparent)
            llOptionD.setBackgroundResource(R.color.transparent)
//            llOptionE.setBackgroundResource(R.color.transparent)

            examQuestions?.get(index)?.selectedAnswer = "2"

            if (examQuestions?.get(index)?.answer == "2") {
                examQuestions?.get(index)?.answerStatus = "1" // correct
            } else {
                examQuestions?.get(index)?.answerStatus = "2" // incorrect
            }
        }

        llOptionC.setOnClickListener {

            llOptionA.setBackgroundResource(R.color.transparent)
            llOptionB.setBackgroundResource(R.color.transparent)
            llOptionC.setBackgroundResource(R.drawable.rectange_bg_green)
            llOptionD.setBackgroundResource(R.color.transparent)
//            llOptionE.setBackgroundResource(R.color.transparent)

            examQuestions?.get(index)?.selectedAnswer = "3"

            if (examQuestions?.get(index)?.answer == "3") {
                examQuestions?.get(index)?.answerStatus = "1" // correct
            } else {
                examQuestions?.get(index)?.answerStatus = "2" // incorrect
            }

        }

        llOptionD.setOnClickListener {

            llOptionA.setBackgroundResource(R.color.transparent)
            llOptionB.setBackgroundResource(R.color.transparent)
            llOptionC.setBackgroundResource(R.color.transparent)
            llOptionD.setBackgroundResource(R.drawable.rectange_bg_green)
//            llOptionE.setBackgroundResource(R.color.transparent)

            examQuestions?.get(index)?.selectedAnswer = "4"

            if (examQuestions?.get(index)?.answer == "4") {
                examQuestions?.get(index)?.answerStatus = "1" // correct
            } else {
                examQuestions?.get(index)?.answerStatus = "2" // incorrect
            }

        }

        /*llOptionE.setOnClickListener {

            llOptionA.setBackgroundResource(R.color.transparent)
            llOptionB.setBackgroundResource(R.color.transparent)
            llOptionC.setBackgroundResource(R.color.transparent)
            llOptionD.setBackgroundResource(R.color.transparent)
            llOptionE.setBackgroundResource(R.drawable.rectange_bg_green)

        }*/

        llBottomBar.setOnClickListener {

            val dialog = Dialog(this@ExamStartActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_custom)
            dialog.setCancelable(false)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
            val tvOne = dialog.findViewById(R.id.tvOne) as TextView
            val txtClose = dialog.findViewById(R.id.tvClose) as TextView
            val txtDone = dialog.findViewById(R.id.tvDone) as TextView

            txtDone.text = getString(R.string.continue_btn)
            txtClose.text = getString(R.string.cancel)
            tvTitle.visibility = View.GONE
//        tvTitle.text = context!!.getString(R.string.label_exit)
            tvOne.text = getString(R.string.confirm_submit_exam)

            txtClose.setOnClickListener { dialog.dismiss() }

            txtDone.setOnClickListener {
                dialog.dismiss()
                callSubmitExamWS()
            }
            dialog.show()

        }

        rgQuestions.setOnCheckedChangeListener { group, checkedId ->
            Logg.e(TAG, " setOnCheckedChangeListener checkedId ===> $checkedId")
            when (checkedId) {
                R.id.rbOptionA -> {

                    if (rbOptionA.isChecked) {
                        examQuestions?.get(index)?.selectedAnswer = "1"

                        if (examQuestions?.get(index)?.answer == "1") {
                            examQuestions?.get(index)?.answerStatus = "1" // correct
                        } else {
                            examQuestions?.get(index)?.answerStatus = "2" // incorrect
                        }
                    }
//                    else {
//                        examQuestions?.get(index)?.selectedAnswer = ""
//                    }

                }
                R.id.rbOptionB -> {

                    if (rbOptionB.isChecked) {
                        examQuestions?.get(index)?.selectedAnswer = "2"

                        if (examQuestions?.get(index)?.answer == "2") {
                            examQuestions?.get(index)?.answerStatus = "1" // correct
                        } else {
                            examQuestions?.get(index)?.answerStatus = "2" // incorrect
                        }
                    }
//                    else {
//                        examQuestions?.get(index)?.selectedAnswer = ""
//                    }

                }
                R.id.rbOptionC -> {

                    if (rbOptionC.isChecked) {
                        examQuestions?.get(index)?.selectedAnswer = "3"

                        if (examQuestions?.get(index)?.answer == "3") {
                            examQuestions?.get(index)?.answerStatus = "1" // correct
                        } else {
                            examQuestions?.get(index)?.answerStatus = "2" // incorrect
                        }
                    }
//                    else {
//                        examQuestions?.get(index)?.selectedAnswer = ""
//                    }
                }
                R.id.rbOptionD -> {

                    if (rbOptionD.isChecked) {
                        examQuestions?.get(index)?.selectedAnswer = "4"

                        if (examQuestions?.get(index)?.answer == "4") {
                            examQuestions?.get(index)?.answerStatus = "1" // correct
                        } else {
                            examQuestions?.get(index)?.answerStatus = "2" // incorrect
                        }
                    }
//                    else {
//                        examQuestions?.get(index)?.selectedAnswer = ""
//                    }

                }
                else -> {

                }
            }
        }

        getExamStartListWS()
    }

    override fun onBackPressed() {


        stopCountDownTimer()

        super.onBackPressed()
    }

    private fun stopCountDownTimer() {

        if (countDownTimer != null) {
            countDownTimer?.cancel()
            countDownTimer = null
        }
    }

    private fun callSubmitExamWS() {

        if (APIUtils.isOnline(this@ExamStartActivity)) {
            showProgressDialog(this@ExamStartActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@ExamStartActivity)
            dataMap[WSKey.EXAM_ID] = examDataItem?.id.toString()
            dataMap[WSKey.JSON_DATA] = getJsonDataFromSelectedAnswer()
            apiHelper!!.callWB(Constants.POST_EXAM_QUESTION_ATTEMPT_MULTIPLE, this@ExamStartActivity, dataMap, Constants.W_EXAM_QUESTION_ATTEMPT_MULTIPLE, this, false, false, true, Constants.POST)

        } else {
            Toast.show(this@ExamStartActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    private fun getJsonDataFromSelectedAnswer(): String {

        var jsonArrayRequest = JSONArray()
        var jsonRequestString: String

        if (examQuestions != null && examQuestions?.size > 0) {
            for (i in 0 until examQuestions.size) {
                var examListItem = examQuestions.get(i)
                var jObject = JSONObject()

                jObject.put(WSKey.QUESTION_ID, examListItem.id)
                jObject.put(WSKey.ANSWER, examListItem.selectedAnswer)
                jObject.put(WSKey.ANSWER_STATUS, examListItem.answerStatus)

                jsonArrayRequest.put(jObject)

            }
        }

        jsonRequestString = jsonArrayRequest.toString()

        Logg.e(TAG, "jsonRequestString ===>" + jsonRequestString)

        return jsonRequestString
    }

    private fun getExamStartListWS() {

        if (APIUtils.isOnline(this@ExamStartActivity)) {
            showProgressDialog(this@ExamStartActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@ExamStartActivity)
            dataMap[WSKey.EXAM_ID] = examChapterItem?.id.toString()
            apiHelper!!.callWB(Constants.POST_EXAM_START, this@ExamStartActivity, dataMap, Constants.W_EXAM_START, this, false, false, true, Constants.POST)

        } else {
            Toast.show(this@ExamStartActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_EXAM_START -> {
                val gson = Gson()
                val examStartResponse = gson.fromJson<ExamStartResponse>(mRes.toString(), ExamStartResponse::class.java!!)

                if (examStartResponse.flag) {

                    if (examStartResponse.data != null && examStartResponse.data.size > 0
                            && examStartResponse.data[0]?.examQuestion != null && examStartResponse.data[0]?.examQuestion.size > 0) {
                        examDataItem = examStartResponse.data[0]
                        examQuestions = examStartResponse.data[0].examQuestion

                        tvChapterNameExam.text = examDataItem?.chapter

                        setQuestionsToViews(index)

                        dataList.clear()

                        for (i in 0 until examQuestions.size) {
                            dataList.add((i + 1).toString())
                        }

                        examQuestionCountAdapter = ExamQuestionCountAdapter(this@ExamStartActivity,
                                dataList)

                        rcvExamQuestionIndex.adapter = examQuestionCountAdapter

                        if (!examStartResponse.data[0].examTime.isNullOrBlank()) {
                            examTime = examStartResponse.data[0].examTime.toInt()
                        }

                        startTimer(examTime)
                    } else {
                        Toast.showLongToast(this@ExamStartActivity, getString(R.string.no_exam_fond))
                    }

                } else {
                    Toast.showLongToast(this@ExamStartActivity, examStartResponse.message)
                }
            }
            W_EXAM_QUESTION_ATTEMPT_MULTIPLE -> {
                val gson = Gson()
                val examResultResponse = gson.fromJson<ExamResultResponse>(mRes.toString(), ExamResultResponse::class.java!!)

                if (examResultResponse.flag) {
//                    Toast.showLongToast(this@ExamStartActivity, getString(R.string.exam_submitted_successfully))
//                    finish()
                    stopCountDownTimer()
                    displayResultDialog(examResultResponse)
//                    displayOkDialog(this@ExamStartActivity, getString(R.string.exam_submitted_successfully))
                } else {
                    stopCountDownTimer()
//                    Toast.showLongToast(this@ExamStartActivity, getString(R.string.exam_submitted_failed))
                    displayOkDialog(this@ExamStartActivity, getString(R.string.exam_submitted_failed))
                }
            }
        }
    }

    public fun setQuestionsToViews(index: Int) {

        llOptionA.setBackgroundResource(R.color.transparent)
        llOptionB.setBackgroundResource(R.color.transparent)
        llOptionC.setBackgroundResource(R.color.transparent)
        llOptionD.setBackgroundResource(R.color.transparent)

//        rbOptionA.isChecked = false
//        rbOptionB.isChecked = false
//        rbOptionC.isChecked = false
//        rbOptionD.isChecked = false

        rgQuestions.clearCheck()

        var examQuestionItem = examQuestions?.get(index)

        tvQuestionNameExam.text = (index + 1).toString() + ". " + examQuestionItem?.question

        if (!examQuestionItem.questionFile.isNullOrBlank()) {
            rlQuestionWithImages.visibility = View.VISIBLE
            rlQuestionAnswersOptions.visibility = View.GONE

            Glide.with(this@ExamStartActivity)
                    .load(examQuestionItem.questionFile)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            return false
                        }
                    })
                    .into(ivQuestionAnswerImage)

        } else {
            rlQuestionWithImages.visibility = View.GONE
            rlQuestionAnswersOptions.visibility = View.VISIBLE
        }

        tvOptionA.text = examQuestionItem?.option1
        tvOptionB.text = examQuestionItem?.option2
        tvOptionC.text = examQuestionItem?.option3
        tvOptionD.text = examQuestionItem?.option4

        tvCurrentOutOfTotal.text = (index + 1).toString() + "/" + examQuestions?.size.toString()

        llBottomBar.visibility = View.GONE

        if (index == 0) {
            llPrevQuestion.visibility = View.INVISIBLE
            llNextQuestion.visibility = View.VISIBLE

            if (examQuestions?.size == 1) {
                llPrevQuestion.visibility = View.INVISIBLE
                llNextQuestion.visibility = View.INVISIBLE
                llBottomBar.visibility = View.VISIBLE
            }

        } else if (index == (examQuestions?.size?.minus(1))) {
            llPrevQuestion.visibility = View.VISIBLE
            llNextQuestion.visibility = View.INVISIBLE
            llBottomBar.visibility = View.VISIBLE
        } else {
            llPrevQuestion.visibility = View.VISIBLE
            llNextQuestion.visibility = View.VISIBLE
        }

        if (!examQuestions?.get(index)?.selectedAnswer.isNullOrBlank()) {
            setSelectedAnswer(examQuestions?.get(index)?.selectedAnswer, examQuestions?.get(index).questionFile)
        }

    }

    private fun setSelectedAnswer(selectedAnswer: String?, questionFile: String) {

        when (selectedAnswer) {

            "1" -> {

                if (questionFile.isNullOrBlank()) {
                    llOptionA.setBackgroundResource(R.drawable.rectange_bg_green)
                    llOptionB.setBackgroundResource(R.color.transparent)
                    llOptionC.setBackgroundResource(R.color.transparent)
                    llOptionD.setBackgroundResource(R.color.transparent)
                } else {
                    rbOptionA.isChecked = true
//                    rbOptionB.isChecked = false
//                    rbOptionC.isChecked = false
//                    rbOptionD.isChecked = false
                }

            }
            "2" -> {

                if (questionFile.isNullOrBlank()) {
                    llOptionA.setBackgroundResource(R.color.transparent)
                    llOptionB.setBackgroundResource(R.drawable.rectange_bg_green)
                    llOptionC.setBackgroundResource(R.color.transparent)
                    llOptionD.setBackgroundResource(R.color.transparent)
                } else {
//                    rbOptionA.isChecked = false
                    rbOptionB.isChecked = true
//                    rbOptionC.isChecked = false
//                    rbOptionD.isChecked = false
                }


            }
            "3" -> {

                if (questionFile.isNullOrBlank()) {
                    llOptionA.setBackgroundResource(R.color.transparent)
                    llOptionB.setBackgroundResource(R.color.transparent)
                    llOptionC.setBackgroundResource(R.drawable.rectange_bg_green)
                    llOptionD.setBackgroundResource(R.color.transparent)
                } else {
//                    rbOptionA.isChecked = false
//                    rbOptionB.isChecked = false
                    rbOptionC.isChecked = true
//                    rbOptionD.isChecked = false
                }


            }
            "4" -> {

                if (questionFile.isNullOrBlank()) {
                    llOptionA.setBackgroundResource(R.color.transparent)
                    llOptionB.setBackgroundResource(R.color.transparent)
                    llOptionC.setBackgroundResource(R.color.transparent)
                    llOptionD.setBackgroundResource(R.drawable.rectange_bg_green)
                } else {
//                    rbOptionA.isChecked = false
//                    rbOptionB.isChecked = false
//                    rbOptionC.isChecked = false
                    rbOptionD.isChecked = true
                }

            }
            else -> {

                if (!questionFile.isNullOrBlank()) {

                    if (rbOptionA.isChecked) {
                        rbOptionA.isChecked = false
                    }

                    if (rbOptionB.isChecked) {
                        rbOptionB.isChecked = false
                    }

                    if (rbOptionC.isChecked) {
                        rbOptionC.isChecked = false
                    }

                    if (rbOptionD.isChecked) {
                        rbOptionD.isChecked = false
                    }
                }

            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@ExamStartActivity, getString(R.string.meesage_connection_timeout))
    }

    private fun startTimer(minutes: Int) {
        countDownTimer = object : CountDownTimer((60 * minutes * 1000).toLong(), 500) {
            // 500 means, onTick function will be called at every 500 milliseconds

            override fun onTick(leftTimeInMilliseconds: Long) {
                val seconds = leftTimeInMilliseconds / 1000

                tvExamTimer.text = String.format("%02d", seconds / 3600) + ":" +
                        String.format("%02d", seconds / 60) + ":" +
                        String.format("%02d", seconds % 60)

            }

            override fun onFinish() {
                Logg.e(TAG, "onFinish ---> ")
                callSubmitExamWS()
            }
        }.start()
    }

    override fun onDestroy() {
        super.onDestroy()

        stopCountDownTimer()
    }

    private fun displayResultDialog(examResultResponse: ExamResultResponse) {

        val dialog = Dialog(this@ExamStartActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_custom_exam_result)
        dialog.setCancelable(false)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val tvTestCompletedFor = dialog.findViewById(R.id.tvTestCompletedFor) as TextView
        val ivSmiley = dialog.findViewById(R.id.ivSmiley) as ImageView
        val tvCloseExamResultDialog = dialog.findViewById(R.id.tvCloseExamResultDialog) as ImageView
        val tvTestResultTitle = dialog.findViewById(R.id.tvTestResultTitle) as TextView
        val tvCorrectAnswers = dialog.findViewById(R.id.tvCorrectAnswers) as TextView
        val tvIncorrectAnswers = dialog.findViewById(R.id.tvIncorrectAnswers) as TextView
        val tvNotAttemptedAnswers = dialog.findViewById(R.id.tvNotAttemptedAnswers) as TextView

        val tvReviewTest = dialog.findViewById(R.id.tvReviewTest) as TextView
        val tvDone = dialog.findViewById(R.id.tvDone) as TextView


        tvTestCompletedFor.text = examChapterItem?.examName

        tvCorrectAnswers.text = examResultResponse.data.correctAnswer.toString()
        tvIncorrectAnswers.text = examResultResponse.data.incorrectAnswer.toString()
        tvNotAttemptedAnswers.text = examResultResponse.data.notAttempt.toString()

        tvTestResultTitle.text = examResultResponse.data.exam.examStatusText

        when (examResultResponse.data.exam.examStatus) {
            "0" -> {
                ivSmiley.setImageResource(R.drawable.fail)
            }
            "1" -> {
                ivSmiley.setImageResource(R.drawable.good)
            }
            "2" -> {
                ivSmiley.setImageResource(R.drawable.average)
            }
            "3" -> {
                ivSmiley.setImageResource(R.drawable.bad)
            }
            "4" -> {
                ivSmiley.setImageResource(R.drawable.excellent)
            }
        }

        tvCloseExamResultDialog.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        tvDone.setOnClickListener {
            dialog.dismiss()
            finish()
        }

        tvReviewTest.setOnClickListener {
            dialog.dismiss()
            val intentReviewTest = Intent(this@ExamStartActivity, TestReviewActivity::class.java)
            intentReviewTest.putExtra("examResultResponse", examResultResponse)
            intentReviewTest.putExtra("examQuestions", examQuestions)
            intentReviewTest.putExtra("correctAnswerCount", examResultResponse.data.correctAnswer.toString())
            intentReviewTest.putExtra("incorrectAnswerCount", examResultResponse.data.incorrectAnswer.toString())
            intentReviewTest.putExtra("notAttemptCount", examResultResponse.data.notAttempt.toString())
            intentReviewTest.putExtra("chapterNumber", "Chapter #$chapterNumber")
            intentReviewTest.putExtra("chapterName", examDataItem?.chapter)
            startActivity(intentReviewTest)
            finish()
        }
        dialog.show()

    }

}
