package com.sms.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.Window
import android.widget.TextView
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.sms.R
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_HOME_WORK_QUE_ANS_FILL
import com.sms.common.ImageInputHelperNew
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.common.CommonResponse
import com.sms.model.homework.HomeWorkDetailsItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.theartofdev.edmodo.cropper.CropImage
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.models.sort.SortingTypes
import droidninja.filepicker.utils.Orientation
import kotlinx.android.synthetic.main.activity_answer_of_question.*
import java.io.File


class AnswerOfQuestionActivity : BaseActivity(), ImageInputHelperNew.ImageActionListener {

    override fun onImageSelectedFromGallery(uri: Uri?, imageFile: File?) {
    }

    override fun onImageTakenFromCamera(uri: Uri?, imageFile: File?) {
    }

    override fun onImageCropped(uri: Uri?, imageFile: File?) {
    }

    private val MAX_ATTACHMENT_COUNT = 1
    private var TAG: String = javaClass.simpleName
    private lateinit var homeWorkDetailsItem: HomeWorkDetailsItem

    private var imageInputHelperNew: ImageInputHelperNew? = null

    private var photoOrDocPath: String = ""
    private var finalFile: File? = null
    //    private var photoPaths = ArrayList<String>()
//    private var docPaths = ArrayList<String>()
    private var photoOrDocPathsList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_answer_of_question)

        imageInputHelperNew = ImageInputHelperNew(this)
        imageInputHelperNew!!.setImageActionListener(this)

        if (intent.extras != null) {
            homeWorkDetailsItem = intent.extras.getSerializable("homeWorkDetailsItem") as HomeWorkDetailsItem

            tvHomeWorkQuestionAnswer.text = homeWorkDetailsItem.question

        } else {
            homeWorkDetailsItem = HomeWorkDetailsItem()
        }

        tvBackAnswer.setOnClickListener {
            finish()
        }

        llUploadFile.setOnClickListener {
            checkCameraPermission()
        }

        ivClearFile.setOnClickListener {
            rlUploadedFile.visibility = View.GONE
            llUploadFile.visibility = View.VISIBLE
            if (photoOrDocPathsList.size > 0) {
                photoOrDocPathsList.clear()
                photoOrDocPath = ""
                finalFile = null
            }
        }

//        edtAnswer.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(p0: Editable?) {
//            }
//
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//            }
//
//            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//            }
//
//        })

        llSubmitAnswer.setOnClickListener {

            if (checkValidation()) {
                showConfirmAnswerDiaog(this@AnswerOfQuestionActivity)
            }
        }

        editor.setEditorHeight(200)
        editor.setEditorFontSize(22)
        editor.setEditorFontColor(Color.RED)
        //editor.setEditorBackgroundColor(Color.BLUE);
        //editor.setBackgroundColor(Color.BLUE);
        //editor.setBackgroundResource(R.drawable.bg);
        editor.setPadding(10, 10, 10, 10)
        //editor.setBackground("https://raw.githubusercontent.com/wasabeef/art/master/chip.jpg");
        editor.setPlaceholder("Insert text here...")
        //editor.setInputEnabled(false);

        editor.setOnTextChangeListener { text ->
            preview.text = text
        }

        editor.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                llTextEffect.visibility = View.VISIBLE
            } else {
                llTextEffect.visibility = View.GONE
            }
        }

        findViewById<View>(R.id.action_undo).setOnClickListener { editor.undo() }

        findViewById<View>(R.id.action_redo).setOnClickListener { editor.redo() }

        findViewById<View>(R.id.action_bold).setOnClickListener { editor.setBold() }

        findViewById<View>(R.id.action_italic).setOnClickListener { editor.setItalic() }

        findViewById<View>(R.id.action_subscript).setOnClickListener { editor.setSubscript() }

        findViewById<View>(R.id.action_superscript).setOnClickListener { editor.setSuperscript() }

        findViewById<View>(R.id.action_strikethrough).setOnClickListener { editor.setStrikeThrough() }

        findViewById<View>(R.id.action_underline).setOnClickListener { editor.setUnderline() }

        findViewById<View>(R.id.action_heading1).setOnClickListener { editor.setHeading(1) }

        findViewById<View>(R.id.action_heading2).setOnClickListener { editor.setHeading(2) }

        findViewById<View>(R.id.action_heading3).setOnClickListener { editor.setHeading(3) }

        findViewById<View>(R.id.action_heading4).setOnClickListener { editor.setHeading(4) }

        findViewById<View>(R.id.action_heading5).setOnClickListener { editor.setHeading(5) }

        findViewById<View>(R.id.action_heading6).setOnClickListener { editor.setHeading(6) }

        findViewById<View>(R.id.action_txt_color).setOnClickListener(object : View.OnClickListener {
            private var isChanged: Boolean = false

            override fun onClick(v: View) {
                editor.setTextColor(if (isChanged) Color.BLACK else Color.RED)
                isChanged = !isChanged
            }
        })

        findViewById<View>(R.id.action_bg_color).setOnClickListener(object : View.OnClickListener {
            private var isChanged: Boolean = false

            override fun onClick(v: View) {
                editor.setTextBackgroundColor(if (isChanged) Color.TRANSPARENT else Color.YELLOW)
                isChanged = !isChanged
            }
        })

        findViewById<View>(R.id.action_indent).setOnClickListener { editor.setIndent() }

        findViewById<View>(R.id.action_outdent).setOnClickListener { editor.setOutdent() }

        findViewById<View>(R.id.action_align_left).setOnClickListener { editor.setAlignLeft() }

        findViewById<View>(R.id.action_align_center).setOnClickListener { editor.setAlignCenter() }

        findViewById<View>(R.id.action_align_right).setOnClickListener { editor.setAlignRight() }

        findViewById<View>(R.id.action_blockquote).setOnClickListener { editor.setBlockquote() }

        findViewById<View>(R.id.action_insert_bullets).setOnClickListener { editor.setBullets() }

        findViewById<View>(R.id.action_insert_numbers).setOnClickListener { editor.setNumbers() }

        findViewById<View>(R.id.action_insert_image).setOnClickListener {
            editor.insertImage("http://www.1honeywan.com/dachshund/image/7.21/7.21_3_thumb.JPG",
                    "dachshund")
        }

        findViewById<View>(R.id.action_insert_link).setOnClickListener { editor.insertLink("https://github.com/wasabeef", "wasabeef") }
        findViewById<View>(R.id.action_insert_checkbox).setOnClickListener { editor.insertTodo() }
    }

    private fun showConfirmAnswerDiaog(context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_custom)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        val tvOne = dialog.findViewById(R.id.tvOne) as TextView
        val txtClose = dialog.findViewById(R.id.tvClose) as TextView
        val txtDone = dialog.findViewById(R.id.tvDone) as TextView


        txtDone.text = getString(R.string.continue_btn)
        txtClose.text = getString(R.string.cancel)
        tvTitle.visibility = View.GONE
//        tvTitle.text = context!!.getString(R.string.label_exit)
        tvOne.text = context!!.getString(R.string.confirm_submit_your_answer)

        txtClose.setOnClickListener { dialog.dismiss() }

        txtDone.setOnClickListener {
            dialog.dismiss()
            answerTheQuestionAPI()
        }
        dialog.show()
    }

    private fun checkCameraPermission() {

        var permissiongratned: Boolean? = null
        val permissionlistener = object : PermissionListener {
            override fun onPermissionGranted() {
                permissiongratned = true
//                uploadPhoto()

                selectImageDocsDialog()

            }

            override fun onPermissionDenied(deniedPermissions: ArrayList<String>) {
                permissiongratned = false
            }
        }

        TedPermission.with(this@AnswerOfQuestionActivity)
                .setPermissionListener(permissionlistener)
//                .setRationaleMessage("we need permission to access your external storage")
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check()
    }

    fun selectImageDocsDialog() {
        val options = arrayOf<CharSequence>("Image", "Docs", "Cancel")
        val builder = AlertDialog.Builder(this@AnswerOfQuestionActivity)
        builder.setTitle(getString(R.string.select_option))
        builder.setItems(options, DialogInterface.OnClickListener { dialog, item ->
            if (item == 0) {
                try {
//                    selectImageDialog()

                    // start picker to get image for cropping and then use the image in cropping activity
                    CropImage.activity().start(this@AnswerOfQuestionActivity)

                    dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else if (item == 1) {
                try {
                    selectDocDialog()
                    dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.show()
    }

    /*private fun selectImageDialog() {
        val options = arrayOf<CharSequence>("Take Photo", "Choose From Gallery", "Cancel")
        val builder = AlertDialog.Builder(this@AnswerOfQuestionActivity)
        builder.setTitle(getString(R.string.select_option))
        builder.setItems(options, { dialog, item ->
            if (item == 0) {
                try {
                    imageInputHelperNew!!.takePhotoWithCamera()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            } else if (item == 1) {
                try {
                    imageInputHelperNew!!.selectImageFromGallery()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.show()
    }*/

    private fun selectDocDialog() {

        Logg.e(TAG, "selectDocDialog")

//        val zips = arrayOf(".zip", ".rar")
//        val pdfs = arrayOf(".pdf")
//        val docs = arrayOf(".doc", ".docs")
//        val xls = arrayOf(".xls", ".xlsx")
//        val ppts = arrayOf(".ppt", ".pptx", "ppsx")
//        val texts = arrayOf(".rtf", ".txt", "csv")
//        val maxCount = MAX_ATTACHMENT_COUNT - photoPaths.size
//        if (docPaths.size + photoPaths.size == MAX_ATTACHMENT_COUNT) {
//            Logg.e(TAG, "selectDocDialog if ====> ")
//            android.widget.Toast.makeText(this, "Cannot select more than $MAX_ATTACHMENT_COUNT items",
//                    android.widget.Toast.LENGTH_SHORT).show()
//        } else {
//            Logg.e(TAG, "selectDocDialog else ====> ")
//            FilePickerBuilder.getInstance()
//                    .setMaxCount(maxCount)
//                    .setSelectedFiles(docPaths)
//                    .setActivityTheme(R.style.FilePickerTheme)
//                    .setActivityTitle("Please select doc")
//                    .addFileSupport("ZIP", zips)
//                    .addFileSupport("PDF", pdfs, R.drawable.pdf_blue)
//                    .addFileSupport("DOC", docs)
//                    .addFileSupport("XLS", xls)
//                    .addFileSupport("PPT", ppts)
//                    .addFileSupport("Text", texts)
//                    .enableDocSupport(false)
//                    .enableSelectAll(true)
//                    .sortDocumentsBy(SortingTypes.name)
//                    .withOrientation(Orientation.UNSPECIFIED)
//                    .pickFile(this@AnswerOfQuestionActivity)
//        }

        val zips = arrayOf(".zip", ".rar")
        val pdfs = arrayOf(".pdf")
        val docs = arrayOf(".doc", ".docs")
        val xls = arrayOf(".xls", ".xlsx")
        val ppts = arrayOf(".ppt", ".pptx", "ppsx")
        val texts = arrayOf(".rtf", ".txt", "csv")
//        val maxCount = MAX_ATTACHMENT_COUNT - photoPaths.size
        val maxCount = photoOrDocPathsList.size
//        if (docPaths.size + photoPaths.size == MAX_ATTACHMENT_COUNT) {
//            Logg.e(TAG, "selectDocDialog if ====> ")
//            android.widget.Toast.makeText(this, "Cannot select more than $MAX_ATTACHMENT_COUNT items",
//                    android.widget.Toast.LENGTH_SHORT).show()
//        } else {
//            Logg.e(TAG, "selectDocDialog else ====> ")
//            FilePickerBuilder.getInstance()
//                    .setMaxCount(maxCount)
//                    .setSelectedFiles(docPaths)
//                    .setActivityTheme(R.style.FilePickerTheme)
//                    .setActivityTitle("Please select doc")
//                    .addFileSupport("ZIP", zips)
//                    .addFileSupport("PDF", pdfs, R.drawable.pdf_blue)
//                    .addFileSupport("DOC", docs)
//                    .addFileSupport("XLS", xls)
//                    .addFileSupport("PPT", ppts)
//                    .addFileSupport("Text", texts)
//                    .enableDocSupport(false)
//                    .enableSelectAll(true)
//                    .sortDocumentsBy(SortingTypes.name)
//                    .withOrientation(Orientation.UNSPECIFIED)
//                    .pickFile(this)
//        }

        if (photoOrDocPathsList.size == MAX_ATTACHMENT_COUNT) {
            Logg.e(TAG, "selectDocDialog if ====> ")
            android.widget.Toast.makeText(this, "Cannot select more than $MAX_ATTACHMENT_COUNT items",
                    android.widget.Toast.LENGTH_SHORT).show()
        } else {
            Logg.e(TAG, "selectDocDialog else ====> ")
            FilePickerBuilder.getInstance()
                    .setMaxCount(MAX_ATTACHMENT_COUNT)
                    .setSelectedFiles(photoOrDocPathsList)
                    .setActivityTheme(R.style.FilePickerTheme)
                    .setActivityTitle("Please select doc")
                    .addFileSupport("ZIP", zips)
                    .addFileSupport("PDF", pdfs, R.drawable.pdf_blue)
                    .addFileSupport("DOC", docs)
                    .addFileSupport("XLS", xls)
                    .addFileSupport("PPT", ppts)
                    .addFileSupport("Text", texts)
                    .enableDocSupport(false)
                    .enableSelectAll(true)
                    .sortDocumentsBy(SortingTypes.name)
                    .withOrientation(Orientation.UNSPECIFIED)
                    .pickFile(this)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    var result: CropImage.ActivityResult = CropImage.getActivityResult(data)
                    var resultUri: Uri = result.uri
                    finalFile = File(resultUri.path)
                    Logg.e("image_path:", "" + finalFile + "")
                    photoOrDocPath = finalFile!!.absoluteFile.toString()
                    if (photoOrDocPathsList.size > 0) {
                        photoOrDocPathsList.clear()
                    }

                    photoOrDocPathsList.add(photoOrDocPath)

                    Logg.e(TAG, "image_path ====>$photoOrDocPath")
                    displayFileInView()

                }
            }
            FilePickerConst.REQUEST_CODE_DOC -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    if (photoOrDocPathsList.size > 0) {
                        photoOrDocPathsList.clear()
                    }

                    photoOrDocPathsList.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS))
                    Logg.e(TAG, "docPaths file -------->" + photoOrDocPathsList.get(0).toString())
                    photoOrDocPath = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS)[0].toString()
                    Logg.e(TAG, "photoOrDocPath -------->$photoOrDocPath")
                    finalFile = File(photoOrDocPath)

                    displayFileInView()
                }
            }
        }

//        addThemToView(photoPaths, docPaths)
    }

    private fun displayFileInView() {

        if (photoOrDocPathsList.size > 0) {
            rlUploadedFile.visibility = View.VISIBLE
            llUploadFile.visibility = View.GONE
            try {
                tvChooseFile.text = photoOrDocPath.substring(photoOrDocPath.lastIndexOf("/") + 1)

                var fileExtension = photoOrDocPath.substring(photoOrDocPath.lastIndexOf(".") + 1)

                Logg.e(TAG, "fileExtension =====> $fileExtension")

                when (fileExtension.toLowerCase()) {
                    "pdf" -> ivUploadedFileIcon.setImageResource(R.drawable.ic_pdf)
                    "doc" -> ivUploadedFileIcon.setImageResource(R.drawable.ic_doc)
                    "docx" -> ivUploadedFileIcon.setImageResource(R.drawable.ic_doc)
                    "xls" -> ivUploadedFileIcon.setImageResource(R.drawable.ic_xls)
                    "xlsx" -> ivUploadedFileIcon.setImageResource(R.drawable.ic_xls)
                    "ppt" -> ivUploadedFileIcon.setImageResource(R.drawable.ic_ppt)
                    "pptx" -> ivUploadedFileIcon.setImageResource(R.drawable.ic_ppt)
                    "epub" -> ivUploadedFileIcon.setImageResource(R.drawable.epub)
                    "jpg" -> ivUploadedFileIcon.setImageResource(R.mipmap.ic_launcher)
                    "jpeg" -> ivUploadedFileIcon.setImageResource(R.mipmap.ic_launcher)
                    "png" -> ivUploadedFileIcon.setImageResource(R.mipmap.ic_launcher)
                    "zip" -> ivUploadedFileIcon.setImageResource(R.mipmap.ic_launcher)
                    "rar" -> ivUploadedFileIcon.setImageResource(R.mipmap.ic_launcher)
                    else -> ivUploadedFileIcon.setImageResource(R.mipmap.ic_launcher)
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        } else {
            rlUploadedFile.visibility = View.GONE
            llUploadFile.visibility = View.VISIBLE
        }

    }

    private fun answerTheQuestionAPI() {

        if (APIUtils.isOnline(this@AnswerOfQuestionActivity)) {
            showProgressDialog(this@AnswerOfQuestionActivity)

//                val dataMap = HashMap<String, String>()
//                dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@AnswerOfQuestionActivity)
//                dataMap[WSKey.HOME_WORK_ID] = homeWorkDetailsItem?.homeWorkId
//                dataMap[WSKey.QUESTION_ID] = homeWorkDetailsItem?.id
//                dataMap[WSKey.ANSWER] = edtAnswer.text.toString()
////              dataMap[WSKey.ANSWER_FILE] =
//
//                apiHelper!!.callWB(Constants.POST_HOME_WORK_QUE_ANS_FILL, this@AnswerOfQuestionActivity, dataMap, Constants.W_HOME_WORK_QUE_ANS_FILL, this, false, false, true, Constants.POST)

            Logg.e(TAG, " preview.text ====> " + preview.text)

            apiHelper.callUpdateFileForAnswer(Constants.POST_HOME_WORK_QUE_ANS_FILL, this@AnswerOfQuestionActivity, Constants.W_HOME_WORK_QUE_ANS_FILL,
                    this@AnswerOfQuestionActivity, false, false, false, Constants.POST, finalFile,
                    Prefs.getString(PrefsKey.ID, "", this@AnswerOfQuestionActivity), homeWorkDetailsItem?.homeWorkId, homeWorkDetailsItem?.id,
                    preview.text.toString())

        } else {
            Toast.show(this@AnswerOfQuestionActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    private fun checkValidation(): Boolean {
        if (photoOrDocPathsList.size == 0 && preview.text.toString().isNullOrBlank()) {
            Toast.showLongToast(this@AnswerOfQuestionActivity, getString(R.string.select_file_to_upload))
            return false
        }

        return true
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_HOME_WORK_QUE_ANS_FILL -> {
                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    setResult(Activity.RESULT_OK)
                    finish()
                } else {
                    Toast.showLongToast(this@AnswerOfQuestionActivity, commonResponse.message)
                }
            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@AnswerOfQuestionActivity, getString(R.string.meesage_connection_timeout))
    }

    override fun onDestroy() {
        super.onDestroy()
        hideProgressDialog()
    }
}
