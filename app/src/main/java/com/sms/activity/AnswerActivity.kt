package com.sms.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.QuestionGiveAnswerListAdapter
import com.sms.common.Constants
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.common.CommonResponse
import com.sms.model.questionanswer.QuestionItem
import com.sms.model.questiondetails.QuestionDetailsResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_answer.*
import java.util.*

class AnswerActivity : BaseActivity() {

    private var TAG = javaClass.simpleName
    private lateinit var questionItem: QuestionItem
    private var position: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_answer)

        var linearLayoutManager = LinearLayoutManager(this@AnswerActivity)
        rcvAnswerQuestion.layoutManager = linearLayoutManager

        if (intent.extras != null) {
            questionItem = intent.getSerializableExtra("questionItem") as QuestionItem
            position = intent.getIntExtra("position", 1)

            tvQuestionQA.text = questionItem.question

            tvQuestionDescription.text = questionItem.description

            tvTagSubjectAnswer.text = questionItem.name
            tvTagClassAnswer.text = questionItem.classId + " " + "Standard"


            if ((position + 1) % 3 == 1) {
                tvAnswerLabelAnswer.setBackgroundResource(R.drawable.shape_circle_saffron)
            } else if ((position + 1) % 3 == 2) {
                tvAnswerLabelAnswer.setBackgroundResource(R.drawable.shape_circle_two)
            } else if ((position + 1) % 3 == 0) {
                tvAnswerLabelAnswer.setBackgroundResource(R.drawable.shape_circle_three)
            }

//            if (questionItem.answer != null && questionItem.answer.size > 0) {
//                rcvAnswerQuestion.adapter = QuestionGiveAnswerListAdapter(this@AnswerActivity, questionItem.answer)
//
//                tvQuestionDescriptionAnswer.text = questionItem.answer[0].answer
//            }

            getQuestionDetailsAPI(questionItem.id)

        }

        tvBackAnswer.setOnClickListener {
            finish()
        }

        llSubmitAnswer.setOnClickListener {
            if (!edtAnswer.text.toString().isNullOrBlank()) {
                submitAnswerWS()
            } else {
                Toast.show(this@AnswerActivity, getString(R.string.please_enter_answer))
            }
        }
    }

    private fun getQuestionDetailsAPI(id: String) {

        if (APIUtils.isOnline(this@AnswerActivity)) {
            showProgressDialog(this@AnswerActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@AnswerActivity)
            dataMap[WSKey.QUESTION_ID] = id
            apiHelper!!.callWB(Constants.POST_QUESTION_DETAILS, this@AnswerActivity, dataMap, Constants.W_QUESTION_DETAILS, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@AnswerActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun submitAnswerWS() {
        if (APIUtils.isOnline(this@AnswerActivity)) {
            showProgressDialog(this@AnswerActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@AnswerActivity)
            dataMap[WSKey.QUESTION_ID] = questionItem.id
            dataMap[WSKey.ANSWER] = edtAnswer.text.toString()
            apiHelper!!.callWB(Constants.POST_ANSWER, this@AnswerActivity, dataMap, Constants.W_ANSWER, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@AnswerActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            Constants.W_ANSWER -> {
                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    finish()
                    Toast.showLongToast(this@AnswerActivity, commonResponse.message)

                } else {
                    Toast.showLongToast(this@AnswerActivity, commonResponse.message)
                }
            }
            Constants.W_QUESTION_DETAILS -> {
                val gson = Gson()
                val questionDetailsResponse = gson.fromJson<QuestionDetailsResponse>(mRes.toString(), QuestionDetailsResponse::class.java!!)

                if (questionDetailsResponse.flag) {
                    if (questionDetailsResponse.question.answer != null && questionDetailsResponse.question.answer.size > 0) {
                        rcvAnswerQuestion.adapter = QuestionGiveAnswerListAdapter(this@AnswerActivity, questionDetailsResponse.question.answer)

                        tvQuestionDescriptionAnswer.text = questionDetailsResponse.question.answer[0].answer
                    }
                } else {
                    Toast.showLongToast(this@AnswerActivity, questionDetailsResponse.message)
                }
            }
        }

    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@AnswerActivity, getString(R.string.meesage_connection_timeout))
    }
}
