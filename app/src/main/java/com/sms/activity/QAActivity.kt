package com.sms.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.QAAdapter
import com.sms.adapters.QAListAdapter
import com.sms.common.AppConstants.Companion.TAB_TYPE_ALL_QUESTION
import com.sms.common.AppConstants.Companion.TAB_TYPE_MY_ANSWER
import com.sms.common.AppConstants.Companion.TAB_TYPE_MY_QUESTION
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_QUESTION_LIST
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.fragment.FragmentHome
import com.sms.loadmore.OnLoadMoreListener
import com.sms.loadmore.RecyclerViewLoadMoreScroll
import com.sms.model.questionanswer.QuestionItem
import com.sms.model.questionanswer.QuestionListResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_qa.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

class QAActivity : BaseActivity() {

    private var TAG = javaClass.simpleName
    private var tabType = TAB_TYPE_ALL_QUESTION
    private var pageNumber = 1
    private var totalPage = 0
    private var questionList: ArrayList<QuestionItem> = ArrayList()
    private var questionListAdapter: QAAdapter? = null
    private var gridLayoutManager: GridLayoutManager? = null
    var scrollListenerGrid: RecyclerViewLoadMoreScroll? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qa)

        tvBackQA.setOnClickListener {
            finish()
        }

        tvAskQuestion.setOnClickListener {

            var intentAskQuestion = Intent(this@QAActivity, AskQuestionActivity::class.java)
            startActivity(intentAskQuestion)

        }

        tvQAAllQuestions.setOnClickListener {
            tvQAAllQuestions.setBackgroundResource(R.color.colorAppBlue)
            tvQAMyQuestions.setBackgroundResource(R.color.colorAppGrey)
            tvQAMyAnswers.setBackgroundResource(R.color.colorAppGrey)
            tabType = TAB_TYPE_ALL_QUESTION
            pageNumber = 1
            if (questionList != null && questionList.size > 0) {
                questionList.clear()
            }
            getQAListWS()
        }

        tvQAMyQuestions.setOnClickListener {

            tvQAAllQuestions.setBackgroundResource(R.color.colorAppGrey)
            tvQAMyQuestions.setBackgroundResource(R.color.colorAppBlue)
            tvQAMyAnswers.setBackgroundResource(R.color.colorAppGrey)
            tabType = TAB_TYPE_MY_QUESTION
            pageNumber = 1
            if (questionList != null && questionList.size > 0) {
                questionList.clear()
            }
            getQAListWS()
        }

        tvQAMyAnswers.setOnClickListener {
            tvQAAllQuestions.setBackgroundResource(R.color.colorAppGrey)
            tvQAMyQuestions.setBackgroundResource(R.color.colorAppGrey)
            tvQAMyAnswers.setBackgroundResource(R.color.colorAppBlue)
            tabType = TAB_TYPE_MY_ANSWER
            pageNumber = 1
            if (questionList != null && questionList.size > 0) {
                questionList.clear()
            }
            getQAListWS()
        }

        gridLayoutManager = GridLayoutManager(this@QAActivity, 1)
        rcvAllQAList.layoutManager = gridLayoutManager


        scrollListenerGrid = RecyclerViewLoadMoreScroll(gridLayoutManager!!)
        scrollListenerGrid!!.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreDataGrid()
            }
        })

        // by default get all question list

//        getAllQAListWS()


//        var cartList = ArrayList<CartListItem>()
//
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//
//        rcvAllQAList.adapter = QAListAdapter(this@QAActivity, cartList)
    }

    private fun loadMoreDataGrid() {
        Logg.e(FragmentHome.TAG, "<=== loadMoreDataGrid ===>")

        if (pageNumber < totalPage) {
            questionListAdapter!!.addLoadingView()

            pageNumber++
            getQAListWS()

        }
    }

    override fun onResume() {
        super.onResume()

        if (questionList != null && questionList.size > 0) {
            questionList.clear()
        }

        getQAListWS()
    }

    private fun getQAListWS() {

        if (APIUtils.isOnline(this@QAActivity)) {
            showProgressDialog(this@QAActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@QAActivity)
            dataMap[WSKey.TAB_TYPE] = tabType
            dataMap[WSKey.PAGE_NO] = pageNumber.toString()
            apiHelper!!.callWB(Constants.POST_QUESTION_LIST, this@QAActivity, dataMap, Constants.W_QUESTION_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@QAActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

//    private fun getMyQAListWS() {
//
//        if (APIUtils.isOnline(this@QAActivity)) {
//            showProgressDialog(this@QAActivity)
//            val dataMap = HashMap<String, String>()
//            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@QAActivity)
//            dataMap[WSKey.TAB_TYPE] = TAB_TYPE_MY_QUESTION
//            apiHelper!!.callWB(Constants.POST_QUESTION_LIST, this@QAActivity, dataMap, Constants.W_QUESTION_LIST, this, false, false, true, Constants.POST)
//        } else {
//            Toast.show(this@QAActivity, Constants.PleaseCheckInternetConnectionEng)
//        }
//
//    }
//
//    private fun getMyAnswersListWS() {
//
//        if (APIUtils.isOnline(this@QAActivity)) {
//            showProgressDialog(this@QAActivity)
//            val dataMap = HashMap<String, String>()
//            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@QAActivity)
//            dataMap[WSKey.TAB_TYPE] = TAB_TYPE_MY_ANSWER
//            apiHelper!!.callWB(Constants.POST_QUESTION_LIST, this@QAActivity, dataMap, Constants.W_QUESTION_LIST, this, false, false, true, Constants.POST)
//        } else {
//            Toast.show(this@QAActivity, Constants.PleaseCheckInternetConnectionEng)
//        }
//
//    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_QUESTION_LIST -> {
                val gson = Gson()
                val questionListResponse = gson.fromJson<QuestionListResponse>(mRes.toString(), QuestionListResponse::class.java!!)

                if (questionListResponse.flag) {

                    totalPage = questionListResponse.totalPage!!

                    if (questionListResponse.question != null && questionListResponse.question.size > 0) {
                        tvNoItemFound.visibility = View.GONE
                        rcvAllQAList.visibility = View.VISIBLE

                        if (totalPage > 0) {
                            if (questionList != null && questionList.size > 0) {
                                questionListAdapter!!.removeLoadingView()
                                questionListAdapter!!.addData(questionListResponse.question)
                                questionListAdapter!!.notifyDataSetChanged()
                                scrollListenerGrid!!.setLoaded()

                            } else {
                                questionList!!.addAll(questionListResponse.question)

                                questionListAdapter = QAAdapter(this@QAActivity, questionList!!)
                                rcvAllQAList.adapter = questionListAdapter
                                questionListAdapter!!.notifyDataSetChanged()

                                gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                    override fun getSpanSize(position: Int): Int {
                                        return when (questionListAdapter!!.getItemViewType(position)) {
                                            Constants.VIEW_TYPE_ITEM -> 1
                                            Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                            else -> -1
                                        }
                                    }
                                }
                            }
                        } else {
                            questionList!!.addAll(questionListResponse.question)
//                        productDetailsSearchedList = modelProductList!!.productList

                            questionListAdapter = QAAdapter(this@QAActivity, questionList!!)
                            rcvAllQAList.adapter = questionListAdapter
                            questionListAdapter!!.notifyDataSetChanged()

                            gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                override fun getSpanSize(position: Int): Int {
                                    return when (questionListAdapter!!.getItemViewType(position)) {
                                        Constants.VIEW_TYPE_ITEM -> 1
                                        Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                        else -> -1
                                    }
                                }
                            }
                        }

                    } else {
                        tvNoItemFound.visibility = View.VISIBLE
                        rcvAllQAList.visibility = View.GONE
                    }

//                    if (questionListResponse.question != null && questionListResponse.question.size > 0) {
//                        tvNoItemFound.visibility = View.GONE
//                        rcvAllQAList.visibility = View.VISIBLE
//                        rcvAllQAList.adapter = QAListAdapter(this@QAActivity, questionListResponse.question)
//                    } else {
//                        tvNoItemFound.visibility = View.VISIBLE
//                        rcvAllQAList.visibility = View.GONE
//                    }

                } else {
                    tvNoItemFound.visibility = View.VISIBLE
                    rcvAllQAList.visibility = View.GONE
                    Toast.showLongToast(this@QAActivity, questionListResponse.message)
                }
            }
        }


    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@QAActivity, getString(R.string.meesage_connection_timeout))
    }

}
