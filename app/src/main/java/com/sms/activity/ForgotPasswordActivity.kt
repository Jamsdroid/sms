package com.sms.activity

import android.os.Bundle
import android.text.TextUtils
import com.alcohol.core.BaseActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.sms.R
import com.sms.common.*
import com.sms.model.authentication.ForgotPasswordResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.RetroFitResponse
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_forgot_password.*
import org.json.JSONObject
import java.util.HashMap

class ForgotPasswordActivity : BaseActivity(), RetroFitResponse, Constants {

    private val TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)

        tvBackForgotPassword.setOnClickListener {

            finish()
        }

        llSubmit.setOnClickListener {
            validateInput()
        }
    }

    private fun validateInput() {

        when {
            TextUtils.isEmpty(edtEmailForgotPassword.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_email))
            }
            !isEmailValid(edtEmailForgotPassword.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.validate_email))
            }
            else -> {
                var deviceToken = Prefs.getString(PrefsKey.DEVICE_TOKEN, "", this@ForgotPasswordActivity).toString()

                if (!deviceToken.isNullOrBlank()) {
                    Logg.e(TAG, "Stored Device Token ===> " + Prefs.getString(PrefsKey.DEVICE_TOKEN, "", this@ForgotPasswordActivity))
                    FirebaseApp.initializeApp(this@ForgotPasswordActivity)
                    deviceToken = FirebaseInstanceId.getInstance().token.toString()
                    Prefs.setString(PrefsKey.DEVICE_TOKEN, deviceToken, this@ForgotPasswordActivity)
                }

                val dataMap = HashMap<String, String>()
                dataMap[WSKey.EMAIL] = edtEmailForgotPassword.text.toString()
                apiHelper.callWB(Constants.POST_URL_FORGOT_PASSWORD, this@ForgotPasswordActivity, dataMap, Constants.W_FORGOT_PASSWORD, this@ForgotPasswordActivity, true, false, true, Constants.POST)
            }
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {

        try {
            if (!mRes.toString().isNullOrBlank()) {
                //I am managing both soical and simple registration in single response with W_REGISTER

                Logg.e(TAG, responseTag + " Response " + mRes.toString())
                val responseObject = JSONObject(mRes.toString())
//                if (responseObject.optInt("status_code") == 400) {
//
//                    if (responseTag.equals(Constants.W_REGISTER, ignoreCase = true)) {
//                        dismissDialog()
//                        if (responseObject.getString("msg").equals("Somthing went to wrong. Please try again.")) {
//
//                            val errorObject = responseObject.getJSONObject("errors")
//
//                            val iter = errorObject.keys()
//                            while (iter.hasNext()) {
//                                val key = iter.next() as String
//                                val value = errorObject.getString(key)
//                                Toast.show(this@ForgotPasswordActivity, value)
////                                if (!(value.equals("The requested email is already in use", true))) {
////                                }
//
//                                break
//                            }
//                            // hide because sign up with fc and google hides
//                            /*Prefs.setBoolean(Constants.SP_SOCIAL_REGISTRATION, true, this@ForgotPasswordActivity)
//
//                            Logg.e(TAG, "funllName ===> " + personData.fullName)
//                            Logg.e(TAG, "email ===> " + personData.emailID)
//                            Logg.e(TAG, "FbId ===> " + personData.fbId)
//                            Logg.e(TAG, "GoogleId ===> " + personData.googleId)
//                            passData(personData.fullName, personData.emailID, personData.fbId, personData.googleId)*/
//                        }
//                    } else {
//                        dismissDialog()
//                        if (responseObject.has("errors")) {
//                            val errorObject = responseObject.getJSONObject("errors")
//
//                            val iter = errorObject.keys()
//                            while (iter.hasNext()) {
//                                val key = iter.next() as String
//                                val value = errorObject.getString(key)
//                                com.alcohol.common.Toast.show(this@ForgotPasswordActivity, value)
//                                break
//                            }
//                        }
//                    }
//                } else if (responseObject.optInt("status_code") == 200) {

                if (responseTag.equals(Constants.W_FORGOT_PASSWORD, ignoreCase = true)) {
                    val gson = Gson()
                    val modelRegistration = gson.fromJson<ForgotPasswordResponse>(responseObject.toString(), ForgotPasswordResponse::class.java)

//                        Toast.show(this@ForgotPasswordActivity, getString(R.string.message_register_success))
                    if (modelRegistration.flag) {
                        Toast.showLongToast(this@ForgotPasswordActivity, modelRegistration.message)
                        finish()
                    } else {
                        Toast.showLongToast(this@ForgotPasswordActivity, modelRegistration.message)
                    }
//                        else if (responseObject.getInt("status_code") == 400) {
//                            if (responseObject.getString("msg").equals("Somthing went to wrong. Please try again.")) {
//                                Prefs.setBoolean(Constants.SP_SOCIAL_REGISTRATION, true, this@ForgotPasswordActivity)
//
//                                Logg.e(TAG, "funllName ===> " + personData.fullName)
//                                Logg.e(TAG, "email ===> " + personData.emailID)
//                                Logg.e(TAG, "FbId ===> " + personData.fbId)
//                                Logg.e(TAG, "GoogleId ===> " + personData.googleId)
//                                passData(personData.fullName, personData.emailID, personData.fbId, personData.googleId)
//                            }
//                        }
                } else {
//                        dismissDialog()
                    Toast.show(this@ForgotPasswordActivity, Constants.SOMETHING_WENT_WRONG)
                }
//                }
            }
        } catch (e: Exception) {
            Logg.e(TAG, " =====================> exeption ===> " + e.message)
//            dismissDialog()
            e.printStackTrace()
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {

        Toast.showLongToast(this@ForgotPasswordActivity, getString(R.string.meesage_connection_timeout))
    }
}