package com.sms.activity

import android.content.Intent
import android.os.Bundle
import com.alcohol.core.BaseActivity
import com.sms.R
import kotlinx.android.synthetic.main.activity_more.*

class MoreActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more)

        tvBackMore.setOnClickListener {
            finish()
        }

        rlAboutUs.setOnClickListener {


        }

        rlPrivacyPolicy.setOnClickListener {

        }

        rlTermsConditions.setOnClickListener {

        }

        rlNotifications.setOnClickListener {
            var intentMore = Intent(this@MoreActivity, NotificationListActivity::class.java)
            startActivity(intentMore)
        }

    }
}
