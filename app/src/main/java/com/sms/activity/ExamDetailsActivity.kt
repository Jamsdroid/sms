package com.sms.activity

import android.content.Intent
import android.os.Bundle
import com.alcohol.core.BaseActivity
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.sms.R
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_ADD_TO_CART_EXAM
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.common.CommonResponse
import com.sms.model.exam.ExamListItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_exam_details.*
import java.util.*

class ExamDetailsActivity : BaseActivity() {

    private lateinit var examListItem: ExamListItem
    private var TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exam_details)

        if (intent.extras != null) {
            examListItem = intent.extras.getSerializable("examListItem") as ExamListItem

            setDetails()
        }

        tvBackExamDetails.setOnClickListener {
            finish()
        }

        tvPurchaseExam.setOnClickListener {

            if (examListItem != null) {
                if (examListItem.isFree == "1") {
                    // free exam
                    // redirect to Exam Chapter List
                    var intentExamChaptersList = Intent(this@ExamDetailsActivity, ExamChaptersListActivity::class.java)
                    intentExamChaptersList.putExtra("examListItem", examListItem)
                    startActivity(intentExamChaptersList)
                } else {
                    // paid
                    if (examListItem.isPurchased == "1") {
                        // free
                        // redirect to Exam Chapter List
                        var intentExamChaptersList = Intent(this@ExamDetailsActivity, ExamChaptersListActivity::class.java)
                        intentExamChaptersList.putExtra("examListItem", examListItem)
                        startActivity(intentExamChaptersList)
                    } else {
                        // paid
                        addToCartExamAPI(examId = examListItem.id, examPrice = examListItem.price)
                    }
                }
            }

        }
    }

    private fun addToCartExamAPI(examId: String, examPrice: String) {

        if (APIUtils.isOnline(this@ExamDetailsActivity)) {
            showProgressDialog(this@ExamDetailsActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@ExamDetailsActivity)
            dataMap[WSKey.EXAM_ID] = examId
            dataMap[WSKey.PRICE] = examPrice
            apiHelper!!.callWB(Constants.POST_ADD_TO_CART_EXAM, this@ExamDetailsActivity, dataMap, Constants.W_ADD_TO_CART_EXAM, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@ExamDetailsActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun setDetails() {

        if (examListItem != null) {

            tvExamNameExamDetails.text = examListItem.examName

            tvFileNameExamDetails.text = examListItem.name

            tvExamUploadedBy.text = examListItem.examCreatedBy

            tvChaptersCountExamDetails.text = examListItem.totalChapter

            if (examListItem.isFree == "1") {

                // free course
                tvExamPriceExamDetails.text = "Free"
                tvPurchaseExam.text = getString(R.string.start_exam)

            } else {

                if (examListItem.isPurchased == "1") {
                    // free course
                    tvExamPriceExamDetails.text = "Free"
                    tvPurchaseExam.text = getString(R.string.start_exam)
                } else {
                    // paid course
                    tvExamPriceExamDetails.text = getString(R.string.symbol_rupee) + " " + examListItem.price
                    tvPurchaseExam.text = getString(R.string.add_to_cart)
                }
            }

            if (!examListItem.image.isNullOrBlank()) {
                Glide.with(this@ExamDetailsActivity).load(examListItem.image).into(ivExamImageExamDetails)
            }

        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_ADD_TO_CART_EXAM -> {

                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    if (commonResponse.message == "Your book has been added to cart successfully") {
                        Toast.showLongToast(this@ExamDetailsActivity, commonResponse.message)
                        finish()
                    } else {
                        Toast.showLongToast(this@ExamDetailsActivity, commonResponse.message)
                    }

                } else {
                    Toast.showLongToast(this@ExamDetailsActivity, commonResponse.message)
                    finish()
                }
            }
        }


    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@ExamDetailsActivity, getString(R.string.meesage_connection_timeout))
    }
}
