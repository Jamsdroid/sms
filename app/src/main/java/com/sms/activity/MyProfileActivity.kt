package com.sms.activity

//import com.bumptech.glide.load.resource.drawable.GlideDrawable
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.alcohol.core.BaseActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.sms.R
import com.sms.common.*
import com.sms.common.Constants.Companion.POST_URL_EDIT_PROFILE
import com.sms.common.Constants.Companion.W_EDIT_PROFILE
import com.sms.common.Constants.Companion.W_USER_DETAILS
import com.sms.fragment.FragmentMyAccount
import com.sms.model.authentication.LoginDetailsItem
import com.sms.model.profie.ProfileDetailsResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_my_profile.*
import java.io.File
import java.util.*

class MyProfileActivity : BaseActivity(), ImageInputHelperNew.ImageActionListener {

    private var TAG: String = javaClass.simpleName
    private var isEditMode: Boolean = false
    internal var finalFile: File? = null
    internal var isUpdatePhoto = false

    private var imageInputHelperNew: ImageInputHelperNew? = null
    private var imageFilePath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)

        imageInputHelperNew = ImageInputHelperNew(this@MyProfileActivity)
        imageInputHelperNew!!.setImageActionListener(this@MyProfileActivity)

        ivProfileImage.isEnabled = false
        edtFirstNameEdit.isEnabled = false
        edtLastNameEdit.isEnabled = false
        tvDateOfBirthEdit.isEnabled = false
        edtEmailAddressEdit.isEnabled = false
//        llSaveProfile.isEnabled = false
        llSaveProfile.visibility = View.INVISIBLE

        edtFirstNameEdit.alpha = 0.4f
        edtLastNameEdit.alpha = 0.4f
        tvDateOfBirthEdit.alpha = 0.4f
        edtEmailAddressEdit.alpha = 0.4f
//        llSaveProfile.alpha = 0.4f

        tvBackMyProfile.setOnClickListener {
            finish()
        }

        tvEditProfile.setOnClickListener {

            isEditMode = !isEditMode

            changeInputsMode()
        }

        ivProfileImage.setOnClickListener {

            if (isEditMode) {
                checkCameraPermission()
            }
        }

        tvDateOfBirthEdit.setOnClickListener {
            hideKeyboard(this@MyProfileActivity)
            Utils.selectBirthDate(this@MyProfileActivity, tvDateOfBirthEdit, null)
        }

        llSaveProfile.setOnClickListener {
            if (isEditMode) {
                validateInput()
            }
        }

        tvPaymentHistory.setOnClickListener {
            var intentPaymentHistory = Intent(this@MyProfileActivity, PaymentHistoryActivity::class.java)
            startActivity(intentPaymentHistory)
        }

        tvChangePassword.setOnClickListener {
            var intentChangePassword = Intent(this@MyProfileActivity, ChangePasswordActivity::class.java)
            startActivity(intentChangePassword)
        }

        getProfileDataAPI()
    }

    private fun changeInputsMode() {

        if (isEditMode) {

            ivProfileImage.isEnabled = true
            edtFirstNameEdit.isEnabled = true
            edtLastNameEdit.isEnabled = true
            tvDateOfBirthEdit.isEnabled = true
//            edtEmailAddressEdit.isEnabled = true
//            llSaveProfile.isEnabled = true
            llSaveProfile.visibility = View.VISIBLE

            edtFirstNameEdit.alpha = 1f
            edtLastNameEdit.alpha = 1f
            tvDateOfBirthEdit.alpha = 1f
//            edtEmailAddressEdit.alpha = 1f
//            llSaveProfile.alpha = 1f


        } else {

            ivProfileImage.isEnabled = false
            edtFirstNameEdit.isEnabled = false
            edtLastNameEdit.isEnabled = false
            tvDateOfBirthEdit.isEnabled = false
//            edtEmailAddressEdit.isEnabled = false
//            llSaveProfile.isEnabled = false
            llSaveProfile.visibility = View.INVISIBLE

            edtFirstNameEdit.alpha = 0.4f
            edtLastNameEdit.alpha = 0.4f
            tvDateOfBirthEdit.alpha = 0.4f
//            edtEmailAddressEdit.alpha = 0.4f
//            llSaveProfile.alpha = 0.4f

        }

    }

    private fun getProfileDataAPI() {

        if (APIUtils.isOnline(this@MyProfileActivity)) {
            showProgressDialog(this@MyProfileActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@MyProfileActivity)
            apiHelper!!.callWB(Constants.POST_URL_USER_DETAILS, this@MyProfileActivity, dataMap, Constants.W_USER_DETAILS, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@MyProfileActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun validateInput() {

        when {
            TextUtils.isEmpty(edtFirstNameEdit.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_first_name))
            }
            TextUtils.isEmpty(edtLastNameEdit.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_last_name))
            }
            TextUtils.isEmpty(tvDateOfBirthEdit.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_bithdate))
            }
            TextUtils.isEmpty(edtEmailAddressEdit.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_email))
            }
            !isEmailValid(edtEmailAddressEdit.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.validate_email))
            }
            else -> {

                if (APIUtils.isOnline(this@MyProfileActivity)) {
                    showProgressDialog(this@MyProfileActivity)
                    Logg.e(TAG, "request param date===>" + Utils.convertDDMMYYYTOYYYYMMDD(tvDateOfBirthEdit.text.toString()))
//                val dataMap = HashMap<String, String>()
//                dataMap[WSKey.FIRST_NAME] = edtFirstName.text.toString()
//                dataMap[WSKey.LAST_NAME] = edtLastName.text.toString()
//                dataMap[WSKey.EMAIL] = edtEmailAddress.text.toString()
//                dataMap[WSKey.DATE_OF_BIRTH] = Utils.convertDDMMYYYTOYYYYMMDD(tvDateOfBirth.text.toString())
//                apiHelper.callWB(Constants.POST_URL_REGISTER, this@MyProfileActivity, dataMap, Constants.W_REGISTER, this@MyProfileActivity, true, false, true, Constants.POST)
                    apiHelper.callUpdateProfile(POST_URL_EDIT_PROFILE, this@MyProfileActivity, W_EDIT_PROFILE, this@MyProfileActivity, false, false, false, Constants.POST,
                            finalFile, edtFirstNameEdit.text.toString(), edtLastNameEdit.text.toString(), Utils.convertDDMMYYYTOYYYYMMDDwithDesh(tvDateOfBirthEdit.text.toString()), edtEmailAddressEdit.toString(),
                            "0", Prefs.getString(PrefsKey.ID, "", this@MyProfileActivity))
                } else {
                    Toast.show(this@MyProfileActivity, Constants.PleaseCheckInternetConnectionEng)
                }
            }
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(FragmentMyAccount.TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_USER_DETAILS -> {
                val gson = Gson()
                val profileDetailsResponse = gson.fromJson<ProfileDetailsResponse>(mRes.toString(), ProfileDetailsResponse::class.java!!)

                if (profileDetailsResponse.flag) {
                    if (profileDetailsResponse.userDetails != null && profileDetailsResponse.userDetails.isNotEmpty()) {
                        setProfileDetails(profileDetailsResponse.userDetails[0])
                    }
                } else {
                    Toast.showLongToast(this@MyProfileActivity, profileDetailsResponse.message)
                }
            }
            W_EDIT_PROFILE -> {
                val gson = Gson()
                val userDetailsResponse = gson.fromJson<ProfileDetailsResponse>(mRes.toString(), ProfileDetailsResponse::class.java)

                if (userDetailsResponse.flag) {
                    Toast.showLongToast(this@MyProfileActivity, userDetailsResponse.message)

                    val loginDetailsItemList: ArrayList<LoginDetailsItem> = userDetailsResponse!!.userDetails

                    Prefs.setString(PrefsKey.EMAIL, loginDetailsItemList[0]!!.email, this@MyProfileActivity)
                    Prefs.setString(PrefsKey.FIRST_NAME, loginDetailsItemList[0]!!.firstName, this@MyProfileActivity)
                    Prefs.setString(PrefsKey.DATE_OF_BIRTH, loginDetailsItemList[0]!!.dateOfBirth, this@MyProfileActivity)
                    Prefs.setString(PrefsKey.LAST_NAME, loginDetailsItemList[0]!!.lastName, this@MyProfileActivity)
                    Prefs.setString(PrefsKey.PROFILE_IMAGE, loginDetailsItemList[0]!!.profileImage, this@MyProfileActivity)

                    finish()
                } else {
                    Toast.showLongToast(this@MyProfileActivity, userDetailsResponse.message)
                }
            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@MyProfileActivity, getString(R.string.meesage_connection_timeout))
    }

    private fun setProfileDetails(profileDetailsItem: LoginDetailsItem) {

        edtFirstNameEdit.setText(profileDetailsItem.firstName)
        edtLastNameEdit.setText(profileDetailsItem.lastName)
        tvDateOfBirthEdit.text = Utils.convertYYYYMMDDTODDMMYYY(profileDetailsItem.dateOfBirth)
        edtEmailAddressEdit.setText(profileDetailsItem.email)

        if (!profileDetailsItem.profileImage.isNullOrBlank()) {
            Glide.with(this@MyProfileActivity)
                    .load(profileDetailsItem.profileImage)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }
                    })
                    .into(ivProfileImage)
        }

        /*Glide.with(this@MyProfileActivity)
                .load(profileDetailsItem.profileImage)
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .listener(object  : RequestListener<String, GlideDrawable> {
                    override fun onException(e: java.lang.Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                        Logg.e(TAG, "<=== onResourceReady ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                        hideProgressDialog()
                        return false
                    }

                    override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                        Logg.e(TAG, "<=== onException ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                        hideProgressDialog()
                        return false
                    }

                })
                .into(ivProfileImage)*/

    }

    private fun checkCameraPermission() {

        var permissiongratned: Boolean? = null
        val permissionlistener = object : PermissionListener {
            override fun onPermissionGranted() {
                permissiongratned = true
//                uploadPhoto()
                // start picker to get image for cropping and then use the image in cropping activity
                CropImage.activity().start(this@MyProfileActivity)

            }

            override fun onPermissionDenied(deniedPermissions: ArrayList<String>) {
                permissiongratned = false
            }
        }

        TedPermission.with(this@MyProfileActivity)
                .setPermissionListener(permissionlistener)
//                .setRationaleMessage("we need permission to access your external storage")
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        imageInputHelperNew!!.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (data != null) {
                    var result: CropImage.ActivityResult = CropImage.getActivityResult(data)
                    if (resultCode == Activity.RESULT_OK) {
                        showProgressDialog(this@MyProfileActivity)
                        var resultUri: Uri = result.uri
                        finalFile = File(resultUri.path)


                        Glide.with(this@MyProfileActivity)
                                .load(finalFile)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        hideProgressDialog()
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        hideProgressDialog()
                                        return false
                                    }
                                })
                                .into(ivProfileImage)

                        /*Glide.with(this@MyProfileActivity)
                                .load(finalFile)
                                .placeholder(R.mipmap.ic_launcher)
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .dontAnimate()
                                .listener(object : RequestListener<File, GlideDrawable> {
                                    override fun onResourceReady(resource: GlideDrawable?, model: File?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                                        Logg.e(TAG, "<=== onResourceReady ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                                        hideProgressDialog()
                                        return false
                                    }
                                    override fun onException(e: java.lang.Exception?, model: File?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                                        Logg.e(TAG, "<=== onException ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                                        hideProgressDialog()
                                        return false
                                    }
                                })
                                .into(ivProfileImage)*/
                        Logg.e("image_path:", "" + finalFile + "");
                        isUpdatePhoto = true

                    }
                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onImageSelectedFromGallery(uri: Uri?, imageFile: File?) {
        try {
            imageInputHelperNew!!.requestCropImage(imageFile, uri, 1, 800, 600, 1, 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onImageTakenFromCamera(uri: Uri?, imageFile: File?) {
        try {
            val mDisplay = windowManager.defaultDisplay
            imageInputHelperNew!!.requestCropImage(imageFile, uri, 1, mDisplay.width, mDisplay.height, 1, 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onImageCropped(uri: Uri?, imageFile: File?) {
        try {
            Logg.e("ProfileActivity", "onImageCropped: " + imageFile!!.getAbsolutePath())
            imageFilePath = imageFile.absolutePath
            Logg.e("ProfileActivity", "imagepath >>" + imageFilePath)
            val filePath = imageFile.path
//             profileBitmap : Bitmap?= BitmapFactory.decodeFile(imageFilePath)
//            ivProfileImage.setImageBitmap(profileBitmap)

            Glide.with(this@MyProfileActivity)
                    .load(finalFile)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            return false
                        }
                    })
                    .into(ivProfileImage)

            /*Glide.with(this@MyProfileActivity)
                    .load(imageFile)
                    .placeholder(R.mipmap.ic_launcher)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .listener(object : RequestListener<File, GlideDrawable> {
                        override fun onResourceReady(resource: GlideDrawable?, model: File?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                            Logg.e(TAG, "<=== onResourceReady ===>")
//                            pbProfileImage.visibility = View.INVISIBLE
                            return false
                        }
                        override fun onException(e: java.lang.Exception?, model: File?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                            Logg.e(TAG, "<=== onException ===>")
//                            pbProfileImage.visibility = View.INVISIBLE
                            return false
                        }
                    })
                    .into(ivProfileImage)*/

            finalFile = File(imageFile.getAbsolutePath());
            Logg.e("image_path:", "" + finalFile + "")

            isUpdatePhoto = true

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}
