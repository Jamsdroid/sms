package com.sms.activity

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.HomeWorkListAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_HOME_WORK_LIST
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.fragment.FragmentHome
import com.sms.loadmore.OnLoadMoreListener
import com.sms.loadmore.RecyclerViewLoadMoreScroll
import com.sms.model.homework.HomeWorkListItem
import com.sms.model.homework.HomeWorkListResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_home_work_list.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

class HomeWorkListActivity : BaseActivity(), Constants {

    private var TAG: String = javaClass.simpleName

    var scrollListenerGrid: RecyclerViewLoadMoreScroll? = null
    var coursesAdapter: HomeWorkListAdapter? = null

    var gridLayoutManager: GridLayoutManager? = null

    var userQuestionsList = ArrayList<HomeWorkListItem>()

    var pageIndex: Int = 0
    var totalPage: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_work_list)

        gridLayoutManager = GridLayoutManager(this@HomeWorkListActivity, 2)

        scrollListenerGrid = RecyclerViewLoadMoreScroll(gridLayoutManager!!)
        scrollListenerGrid!!.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreDataGrid()
            }
        })

        rcvHomeWork?.layoutManager = gridLayoutManager

        rcvHomeWork.addOnScrollListener(scrollListenerGrid!!)

        tvBackHomeWork.setOnClickListener {
            finish()
        }

        getHomeWorkListAPI(pageIndex)

    }

    private fun loadMoreDataGrid() {

        Logg.e(FragmentHome.TAG, "<=== loadMoreDataGrid ===>")

        if (pageIndex < (totalPage - 1)) {
            coursesAdapter!!.addLoadingView()

            pageIndex++
            getHomeWorkListAPI(pageIndex)

        }
    }

    private fun getHomeWorkListAPI(pageNo: Int) {

        if (APIUtils.isOnline(this@HomeWorkListActivity)) {
            showProgressDialog(this@HomeWorkListActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@HomeWorkListActivity)
            dataMap[WSKey.PAGE_NO] = pageNo.toString()
            dataMap[WSKey.CLASS_ID] = ""
            apiHelper!!.callWB(Constants.POST_HOME_WORK_LIST, this@HomeWorkListActivity, dataMap, Constants.W_HOME_WORK_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@HomeWorkListActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_HOME_WORK_LIST -> {
                hideProgressDialog()

                val gson = Gson()
                val homeWorkListResponse = gson.fromJson<HomeWorkListResponse>(mRes.toString(), HomeWorkListResponse::class.java!!)

                if (homeWorkListResponse.flag) {

                    totalPage = homeWorkListResponse.totalPage!!

                    if (totalPage > 1) {

                        if (userQuestionsList != null && userQuestionsList.size > 0) {
                            coursesAdapter!!.removeLoadingView()
                            coursesAdapter!!.addData(homeWorkListResponse.homeWorkDetails)
                            coursesAdapter!!.notifyDataSetChanged()
                            scrollListenerGrid!!.setLoaded()

                        } else {
                            userQuestionsList!!.addAll(homeWorkListResponse.homeWorkDetails)

                            coursesAdapter = HomeWorkListAdapter(this@HomeWorkListActivity, userQuestionsList!!)
                            rcvHomeWork.adapter = coursesAdapter
                            coursesAdapter!!.notifyDataSetChanged()

                            gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                override fun getSpanSize(position: Int): Int {
                                    return when (coursesAdapter!!.getItemViewType(position)) {
                                        Constants.VIEW_TYPE_ITEM -> 1
                                        Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                        else -> -1
                                    }
                                }
                            }
                        }

                    } else {
                        userQuestionsList!!.addAll(homeWorkListResponse.homeWorkDetails)
//                        productDetailsSearchedList = modelProductList!!.productList

                        coursesAdapter = HomeWorkListAdapter(this@HomeWorkListActivity, userQuestionsList!!)
                        rcvHomeWork.adapter = coursesAdapter
                        coursesAdapter!!.notifyDataSetChanged()

                        gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                            override fun getSpanSize(position: Int): Int {
                                return when (coursesAdapter!!.getItemViewType(position)) {
                                    Constants.VIEW_TYPE_ITEM -> 1
                                    Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                    else -> -1
                                }
                            }
                        }
                    }


                } else {
                    Toast.showLongToast(this@HomeWorkListActivity, homeWorkListResponse.message)
                }
            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@HomeWorkListActivity, getString(R.string.meesage_connection_timeout))
    }

    override fun onDestroy() {
        super.onDestroy()
        hideProgressDialog()
    }
}
