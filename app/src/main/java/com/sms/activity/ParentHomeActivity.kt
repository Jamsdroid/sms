package com.sms.activity

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import com.alcohol.core.BaseActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.common.Constants
import com.sms.common.Toast
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import kotlinx.android.synthetic.main.activity_parent_home.*

class ParentHomeActivity : BaseActivity() {


    private var TAG: String = javaClass.simpleName
    private var deviceWidth = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_home)

        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        deviceWidth = size.x

        val params = ivProfileImageParentProfile.layoutParams as LinearLayout.LayoutParams
        params.height = (deviceWidth * 0.50).toInt()
        ivProfileImageParentProfile.layoutParams = params

        rlPaymentHistoryParent.setOnClickListener {
            var intent = Intent(this@ParentHomeActivity, PaymentHistoryActivity::class.java)
            startActivity(intent)
        }

        rlNotificationsParent.setOnClickListener {
            var intent = Intent(this@ParentHomeActivity, NotificationListActivity::class.java)
            startActivity(intent)
        }

        rlMyProfileParent.setOnClickListener {
            var intent = Intent(this@ParentHomeActivity, ParentProfileActivity::class.java)
            startActivity(intent)
        }

        rlExamHistory.setOnClickListener {
            var intent = Intent(this@ParentHomeActivity, ParentExamHistoryActivity::class.java)
            startActivity(intent)
        }

        tvLogoutParent.setOnClickListener {
            val dialog = Dialog(this@ParentHomeActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_custom)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val tvOne = dialog.findViewById(R.id.tvOne) as TextView
            val txtClose = dialog.findViewById(R.id.tvClose) as TextView
            val txtDone = dialog.findViewById(R.id.tvDone) as TextView

            tvOne.text = getString(R.string.message_logout)

            txtClose.setOnClickListener { dialog.dismiss() }

            txtDone.setOnClickListener {
                dialog.dismiss()
                if (APIUtils.isOnline(this@ParentHomeActivity)) {

                    Prefs.clearAll(this@ParentHomeActivity)
                    val intentLogin = Intent(this@ParentHomeActivity, LoginActivity::class.java)
                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intentLogin)
                    this@ParentHomeActivity.finish()

                } else {
                    Toast.show(this@ParentHomeActivity, Constants.PleaseCheckInternetConnectionEng)
                }
            }
            dialog.show()
        }

        if (!Prefs.getString(PrefsKey.PROFILE_IMAGE, "", this@ParentHomeActivity).isNullOrBlank()) {
            Glide.with(this@ParentHomeActivity)
                    .load(Prefs.getString(PrefsKey.PROFILE_IMAGE, "", this@ParentHomeActivity))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }
                    })
                    .into(ivProfileImageParentProfile)
        }

        if (!Prefs.getString(PrefsKey.FIRST_NAME, "", this@ParentHomeActivity).isNullOrBlank()
                && !Prefs.getString(PrefsKey.LAST_NAME, "", this@ParentHomeActivity).isNullOrBlank()) {
            tvUserNameParentProfile.text = Prefs.getString(PrefsKey.FIRST_NAME, "", this@ParentHomeActivity) + " " +
                    Prefs.getString(PrefsKey.LAST_NAME, "", this@ParentHomeActivity)

        } else if (!Prefs.getString(PrefsKey.FIRST_NAME, "", this@ParentHomeActivity).isNullOrBlank()) {
            tvUserNameParentProfile.text = Prefs.getString(PrefsKey.FIRST_NAME, "", this@ParentHomeActivity)

        } else if (!Prefs.getString(PrefsKey.LAST_NAME, "", this@ParentHomeActivity).isNullOrBlank()) {
            tvUserNameParentProfile.text = Prefs.getString(PrefsKey.LAST_NAME, "", this@ParentHomeActivity)
        }
    }

    override fun onBackPressed() {
        showExitAppDialog()
//        super.onBackPressed()
    }

    fun showExitAppDialog() {
        val dialog = Dialog(this@ParentHomeActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_custom)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        val tvOne = dialog.findViewById(R.id.tvOne) as TextView
        val txtClose = dialog.findViewById(R.id.tvClose) as TextView
        val txtDone = dialog.findViewById(R.id.tvDone) as TextView

        tvTitle.text = getString(R.string.exit_app)
        tvOne.text = getString(R.string.message_exit_app)

        txtClose.setOnClickListener { dialog.dismiss() }

        txtDone.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()
    }
}
