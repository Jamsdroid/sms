package com.sms.activity

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.NotificationAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.W_NOTIFICATION_LIST
import com.sms.common.Constants.Companion.W_NOTIFICATION_REPLY
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.fragment.FragmentHome
import com.sms.loadmore.OnLoadMoreListener
import com.sms.loadmore.RecyclerViewLoadMoreScroll
import com.sms.model.common.CommonResponse
import com.sms.model.notification.NotificationItem
import com.sms.model.notification.NotificationListResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_notification_list.*
import java.util.*
import kotlin.collections.set

class NotificationListActivity : BaseActivity() {

    private var TAG = javaClass.simpleName
    private var pageNumber = 1
    private var totalPage = 0

    private var notificationAdapter: NotificationAdapter? = null
    private var gridLayoutManager: GridLayoutManager? = null
    var scrollListenerGrid: RecyclerViewLoadMoreScroll? = null

    private var notificationList: ArrayList<NotificationItem> = ArrayList()

    companion object {
        var notificationClickedItem = -1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_list)

        tvBackNotification.setOnClickListener {
            finish()
        }

//        var linearLayoutManager = LinearLayoutManager(this@NotificationListActivity)
//        rcvNotificationList.layoutManager = linearLayoutManager


//        var cartList = ArrayList<CartListItem>()
//
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//        cartList.add(CartListItem())
//
//        rcvNotificationList.adapter = NotificationListAdapter(this@NotificationListActivity, cartList)

        gridLayoutManager = GridLayoutManager(this@NotificationListActivity, 1)
        rcvNotificationList.layoutManager = gridLayoutManager

        scrollListenerGrid = RecyclerViewLoadMoreScroll(gridLayoutManager!!)
        scrollListenerGrid!!.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreDataGrid()
            }
        })

        getNotificationsListWS()

        ivSendReply.setOnClickListener {
            if (edtAnswerNotification.text.toString().isNullOrBlank()) {
                Toast.show(this@NotificationListActivity, getString(R.string.please_enter_reply))
            } else {
                sendReplyWs()
            }

//            rlBottomNotification.visibility = View.GONE
        }

    }

    private fun loadMoreDataGrid() {

        Logg.e(FragmentHome.TAG, "<=== loadMoreDataGrid ===>")

        if (pageNumber < totalPage) {
            notificationAdapter!!.addLoadingView()

            pageNumber++
            getNotificationsListWS()

        }
    }

    private fun getNotificationsListWS() {

        if (APIUtils.isOnline(this@NotificationListActivity)) {
            showProgressDialog(this@NotificationListActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@NotificationListActivity)
            dataMap[WSKey.PAGE_NO] = pageNumber.toString()
            apiHelper!!.callWB(Constants.POST_NOTIFICATION_LIST, this@NotificationListActivity, dataMap, Constants.W_NOTIFICATION_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@NotificationListActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    private fun sendReplyWs() {

        if (APIUtils.isOnline(this@NotificationListActivity)) {
            showProgressDialog(this@NotificationListActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@NotificationListActivity)
            dataMap[WSKey.NOTIFICATION_ID] = notificationList[notificationClickedItem].id
            dataMap[WSKey.REPLY_MESSAGE] = edtAnswerNotification.text.toString()
            apiHelper!!.callWB(Constants.POST_NOTIFICATION_REPLY, this@NotificationListActivity, dataMap, Constants.W_NOTIFICATION_REPLY, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@NotificationListActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            W_NOTIFICATION_LIST -> {
                val gson = Gson()
                val notificationListResponse = gson.fromJson<NotificationListResponse>(mRes.toString(), NotificationListResponse::class.java!!)

                if (notificationListResponse.flag) {

                    totalPage = notificationListResponse.totalPage!!

                    if (notificationListResponse.question != null && notificationListResponse.question.size > 0) {
                        tvNoNotificationFound.visibility = View.GONE
                        rcvNotificationList.visibility = View.VISIBLE

                        if (totalPage > 0) {
                            if (notificationList != null && notificationList.size > 0) {
                                notificationAdapter!!.removeLoadingView()
                                notificationAdapter!!.addData(notificationListResponse.question)
                                notificationAdapter!!.notifyDataSetChanged()
                                scrollListenerGrid!!.setLoaded()

                            } else {
                                notificationList!!.addAll(notificationListResponse.question)

                                notificationAdapter = NotificationAdapter(this@NotificationListActivity, notificationList!!)
                                rcvNotificationList.adapter = notificationAdapter
                                notificationAdapter!!.notifyDataSetChanged()

                                gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                    override fun getSpanSize(position: Int): Int {
                                        return when (notificationAdapter!!.getItemViewType(position)) {
                                            Constants.VIEW_TYPE_ITEM -> 1
                                            Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                            else -> -1
                                        }
                                    }
                                }
                            }
                        } else {
                            notificationList!!.addAll(notificationListResponse.question)
//                        productDetailsSearchedList = modelProductList!!.productList

                            notificationAdapter = NotificationAdapter(this@NotificationListActivity, notificationList!!)
                            rcvNotificationList.adapter = notificationAdapter
                            notificationAdapter!!.notifyDataSetChanged()

                            gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                override fun getSpanSize(position: Int): Int {
                                    return when (notificationAdapter!!.getItemViewType(position)) {
                                        Constants.VIEW_TYPE_ITEM -> 1
                                        Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                        else -> -1
                                    }
                                }
                            }
                        }

                    } else {
                        tvNoNotificationFound.visibility = View.VISIBLE
                        rcvNotificationList.visibility = View.GONE
                    }

                } else {
                    Toast.showLongToast(this@NotificationListActivity, notificationListResponse.message)
                }
            }
            W_NOTIFICATION_REPLY -> {
                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    rlBottomNotification.visibility = View.GONE
                    notificationClickedItem = -1
                    Toast.show(this@NotificationListActivity, getString(R.string.notification_reply_success))

                    // call get notification

                    getNotificationsListWS()

                } else {
                    Toast.show(this@NotificationListActivity, commonResponse.message)
                }
            }
        }

    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@NotificationListActivity, getString(R.string.meesage_connection_timeout))
    }
}
