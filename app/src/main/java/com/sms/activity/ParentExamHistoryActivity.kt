package com.sms.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.ExamHistoryAdapter
import com.sms.common.Constants
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.parentexamhistory.ExamHistoryResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_parent_exam_history.*
import java.util.*

class ParentExamHistoryActivity : BaseActivity() {

    private var TAG = javaClass.simpleName


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_exam_history)

        var linearLayoutManager = LinearLayoutManager(this@ParentExamHistoryActivity)
        rcvExamHistory.layoutManager = linearLayoutManager

        tvBackExamHistory.setOnClickListener {
            finish()
        }

        getExamHistoryDataAPI()
    }

    private fun getExamHistoryDataAPI() {

        if (APIUtils.isOnline(this@ParentExamHistoryActivity)) {
            showProgressDialog(this@ParentExamHistoryActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.PARENT_ID] = Prefs.getString(PrefsKey.ID, "", this@ParentExamHistoryActivity)
            apiHelper!!.callWB(Constants.POST_PARENT_EXAM_LIST, this@ParentExamHistoryActivity, dataMap, Constants.W_PARENT_EXAM_LIST, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@ParentExamHistoryActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            Constants.W_PARENT_EXAM_LIST -> {
                val gson = Gson()
                val examHistoryResponse = gson.fromJson<ExamHistoryResponse>(mRes.toString(), ExamHistoryResponse::class.java!!)

                if (examHistoryResponse.flag) {

                    if (examHistoryResponse.examList != null && examHistoryResponse.examList.size > 0) {
                        rcvExamHistory.adapter = ExamHistoryAdapter(this@ParentExamHistoryActivity, examHistoryResponse.examList)
                    }
                }
            }
        }
    }


}
