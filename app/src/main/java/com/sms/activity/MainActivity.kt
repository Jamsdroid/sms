package com.sms.activity

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.alcohol.core.BaseActivity
import com.alcohol.helper.BottomNavigationPosition
import com.alcohol.helper.createFragment
import com.alcohol.helper.findNavigationPositionById
import com.alcohol.helper.getTag
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.sms.R
import com.sms.common.Constants
import com.sms.common.disableShiftMode
import com.sms.custom.BottomNavigationViewEx
import com.sms.fragment.FragmentExam
import com.sms.fragment.FragmentHome
import com.sms.fragment.FragmentMyAccount
import com.sms.fragment.FragmentMyCourses
import kotlinx.android.synthetic.main.include_home_header.*
import java.util.*

class MainActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener, Constants, FragmentHome.OnHomeFragmentInteractionListener, FragmentExam.OnExamFragmentInteractionListener, FragmentMyCourses.OnMyCoursesFragmentInteractionListener, FragmentMyAccount.OnMyProfileFragmentInteractionListener {

    override fun onHomeFragmentInteraction(uri: Uri) {
    }

    override fun onMyCoursesFragmentInteraction(uri: Uri) {
    }

    override fun onExamFragmentInteractionListener(uri: Uri) {
    }

    override fun onMyProfileFragmentInteraction(uri: Uri) {
    }

    private val KEY_POSITION = "keyPosition"

    private var navPosition: BottomNavigationPosition = BottomNavigationPosition.HOME


    companion object {

        public lateinit var bottomNavigation: BottomNavigationViewEx
    }

    //    private lateinit var toolbar: Toolbar
//    lateinit var tvHeaderTitle: TextView
//    lateinit var tvSearch: FontIonicons

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*tvLogout.setOnClickListener {
            val dialog = Dialog(this@MainActivity)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.dialog_custom)
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val tvOne = dialog.findViewById(R.id.tvOne) as TextView
            val txtClose = dialog.findViewById(R.id.tvClose) as TextView
            val txtDone = dialog.findViewById(R.id.tvDone) as TextView

            tvOne.text = getString(R.string.message_logout)

            txtClose.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    dialog.dismiss()
                }
            })

            txtDone.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    Prefs.clearAll(this@MainActivity)
                    val intentLogin = Intent(this@MainActivity, LoginActivity::class.java)
                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intentLogin)
                    finish()
//                    dialog.dismiss()
//                    if (APIUtils.isOnline(activity!!)) {
//                        showProgressDialog()
//                        val dataMap = HashMap<String, String>()
//                        dataMap.put(UtilsKey.LogoutRequest.USER_ID, Prefs.getString(SP_CUSTOMER_ID, "", activity!!).toString())
//                        dataMap.put(UtilsKey.LogoutRequest.PANEL, "app")
//                        dataMap.put(UtilsKey.LogoutRequest.CUSTOMER_ID, Prefs.getString(SP_CUSTOMER_ID, "", activity!!).toString())
//                        apiHelper!!.callWB(Constants.RELATIVE_URL_LOOUT, activity!!, dataMap, Constants.W_LOGOUT, this@FragmentSettings, false, false, true, Constants.POST)
//                    } else {
//                        Toast.show(context, Constants.PleaseCheckInternetConnectionEng)
//                    }
                }
            })
            dialog.show()
        }*/

        ivCart.setOnClickListener {
            var intentCart = Intent(this@MainActivity, CartListActivity::class.java)
            startActivity(intentCart)
        }

        bottomNavigation = findViewById(R.id.bottom_navigation)
        bottomNavigation.disableShiftMode()
        bottomNavigation.setTextVisibility(true)
        bottomNavigation.enableAnimation(false)
        bottomNavigation.enableShiftingMode(false)
        bottomNavigation.enableItemShiftingMode(false)
//        setSupportActionBar(toolbar)
        setupBottomNavigation()
        initFragment(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        // Store the current navigation position.
        outState?.putInt(KEY_POSITION, navPosition.id)
        super.onSaveInstanceState(outState)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        navPosition = findNavigationPositionById(item.itemId)
//        bottomNavigation.post { centerMenuIcon() }
        when (item.itemId) {
//            R.id.home -> item.setIcon(R.drawable.home_icon)
//            R.id.my_account -> item.setIcon(R.drawable.myacc_fill)
//            R.id.notifications -> item.setIcon(R.drawable.notification_filll)
//            R.id.settings -> item.setIcon(R.drawable.setting_fill)

            R.id.home -> item.setIcon(R.drawable.selector_home)
            R.id.my_courses -> item.setIcon(R.drawable.selector_notification)
            R.id.exam -> item.setIcon(R.drawable.selector_exam)
            R.id.my_account -> item.setIcon(R.drawable.selector_profile)
        }
//        bottomNavigation.post { centerMenuIcon() }
        return switchFragment(navPosition)
    }

    private fun restoreSaveInstanceState(savedInstanceState: Bundle?) {
        // Restore the current navigation position.
        savedInstanceState?.also {
            val id = it.getInt(KEY_POSITION, BottomNavigationPosition.HOME.id)
            navPosition = findNavigationPositionById(id)
        }
    }

    private fun setupBottomNavigation() {
        bottomNavigation.disableShiftMode() // Extension function
//        bottomNavigation.active(navPosition.position)   // Extension function
        bottomNavigation.onNavigationItemSelectedListener = this

//        onNavigationItemSelected(bottomNavigation.menu.getItem(0))
//        bottomNavigation.post { centerMenuIcon() }
    }

    private fun initFragment(savedInstanceState: Bundle?) {
        savedInstanceState ?: switchFragment(BottomNavigationPosition.HOME)
    }

    /**
     * Immediately execute transactions with FragmentManager#executePendingTransactions.
     */
    private fun switchFragment(navPosition: BottomNavigationPosition): Boolean {
        val fragment = supportFragmentManager.findFragment(navPosition)
        if (fragment.isAdded) return false
        detachFragment()
        attachFragment(fragment, navPosition.getTag())
        supportFragmentManager.executePendingTransactions()
        return true
    }

    private fun FragmentManager.findFragment(position: BottomNavigationPosition): Fragment {
        return findFragmentByTag(position.getTag()) ?: position.createFragment()
    }

    private fun detachFragment() {
        supportFragmentManager.findFragmentById(R.id.container)?.also {
            supportFragmentManager.beginTransaction().detach(it).commit()
        }
    }

    private fun attachFragment(fragment: Fragment, tag: String) {
        if (fragment.isDetached) {
            supportFragmentManager.beginTransaction().attach(fragment).commit()
        } else {
            supportFragmentManager.beginTransaction().add(R.id.container, fragment, tag).commit()
        }
        // Set a transition animation for this transaction.
        supportFragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()
    }

    private fun centerMenuIcon() {
        val navigationView = bottomNavigation
        for (i in 0 until navigationView.childCount) {
            val menuView = navigationView.getChildAt(i)
            if (menuView is BottomNavigationMenuView) {
                for (i1 in 0 until menuView.childCount) {
                    val menuItemView = menuView.getChildAt(i1) as BottomNavigationItemView
                    val smallText = menuItemView.findViewById<View>(R.id.smallLabel) as TextView
//                    smallText.post {
//                        smallText.visibility = View.GONE
//                    }
//                    val largeText = menuItemView.findViewById<View>(R.id.largeLabel) as TextView
//                    largeText.post {
//                        largeText.visibility = View.GONE
//                    }
                    val icon = menuItemView.findViewById<View>(R.id.icon) as ImageView
                    val params = icon.layoutParams as android.widget.FrameLayout.LayoutParams
                    params.gravity = Gravity.CENTER
                    icon.layoutParams = params
                }
            }
        }
    }


    public fun checkFilePermission() {

        var permissiongratned: Boolean? = null
        val permissionlistener = object : PermissionListener {
            override fun onPermissionGranted() {
                permissiongratned = true

                // process file

            }

            override fun onPermissionDenied(deniedPermissions: ArrayList<String>) {
                permissiongratned = false
            }
        }

        TedPermission.with(this@MainActivity)
                .setPermissionListener(permissionlistener)
//                .setRationaleMessage("we need permission to access your external storage")
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check()
    }

    override fun onBackPressed() {
        showExitAppDialog()
//        super.onBackPressed()
    }

    fun showExitAppDialog() {
        val dialog = Dialog(this@MainActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_custom)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        val tvOne = dialog.findViewById(R.id.tvOne) as TextView
        val txtClose = dialog.findViewById(R.id.tvClose) as TextView
        val txtDone = dialog.findViewById(R.id.tvDone) as TextView

        tvTitle.text = getString(R.string.exit_app)
        tvOne.text = getString(R.string.message_exit_app)

        txtClose.setOnClickListener { dialog.dismiss() }

        txtDone.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()
    }
}
