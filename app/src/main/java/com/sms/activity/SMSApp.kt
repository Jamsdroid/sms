package com.sms.activity

import android.content.Context
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
//import com.crashlytics.android.BuildConfig
//import com.crashlytics.android.Crashlytics
//import com.crashlytics.android.core.CrashlyticsCore
//import io.fabric.sdk.android.Fabric

class SMSApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
//        initFabric(this)
    }

//    fun initFabric(context: Context) {
//        val core = CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()
//        val kit = Crashlytics.Builder().core(core).build()
//        Fabric.with(context, kit)
//    }

}