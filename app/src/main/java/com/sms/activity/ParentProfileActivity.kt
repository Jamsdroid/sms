package com.sms.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.alcohol.core.BaseActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import com.sms.R
import com.sms.common.*
import com.sms.fragment.FragmentMyAccount
import com.sms.model.authentication.LoginDetailsItem
import com.sms.model.parent.ParentProfileResponse
import com.sms.model.parent.UserDetailsItem
import com.sms.model.profie.ProfileDetailsResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_my_profile.*
import kotlinx.android.synthetic.main.activity_parent_profile.*
import kotlinx.android.synthetic.main.activity_parent_profile.view.*
import java.io.File
import java.util.ArrayList
import java.util.HashMap

class ParentProfileActivity : BaseActivity(), ImageInputHelperNew.ImageActionListener {

    private var TAG: String = javaClass.simpleName
    private var isEditMode: Boolean = false
    internal var finalFile: File? = null
    internal var isUpdatePhoto = false

    private var imageInputHelperNew: ImageInputHelperNew? = null
    private var imageFilePath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parent_profile)

        imageInputHelperNew = ImageInputHelperNew(this@ParentProfileActivity)
        imageInputHelperNew!!.setImageActionListener(this@ParentProfileActivity)

        ivProfileImageParent.isEnabled = false

        edtFirstNameEditParent.isEnabled = false
        edtOccupationEditParent.isEnabled = false
        edtEducationEditParent.isEnabled = false
        edtPhoneNumberEditParent.isEnabled = false
//        llSaveProfileParent.isEnabled = false
        llSaveProfileParent.visibility = View.INVISIBLE

        edtFirstNameEditParent.alpha = 0.4f
        edtOccupationEditParent.alpha = 0.4f
        edtEducationEditParent.alpha = 0.4f
        edtPhoneNumberEditParent.alpha = 0.4f
//        llSaveProfileParent.alpha = 0.4f

        tvBackMyProfileParent.setOnClickListener {
            finish()
        }

        tvEditProfileParent.setOnClickListener {

            isEditMode = !isEditMode

            changeInputsMode()
        }

        ivProfileImageParent.setOnClickListener {

//            if (isEditMode) {
//
//                checkCameraPermission()
//            }
        }

//        edtEducationEditParent.setOnClickListener {
//            hideKeyboard(this@ParentProfileActivity)
//            Utils.selectBirthDate(this@ParentProfileActivity, edtEducationEditParent, null)
//        }

        llSaveProfileParent.setOnClickListener {
            if (isEditMode) {
                validateInput()
            }
        }

//        tvPaymentHistoryParent.setOnClickListener {
//            var intentPaymentHistory = Intent(this@ParentProfileActivity, PaymentHistoryActivity::class.java)
//            startActivity(intentPaymentHistory)
//        }
//
//        tvChangePasswordParent.setOnClickListener {
//            var intentChangePassword = Intent(this@ParentProfileActivity, ChangePasswordActivity::class.java)
//            startActivity(intentChangePassword)
//        }

        getProfileDataAPI()
    }

    private fun getProfileDataAPI() {

        if (APIUtils.isOnline(this@ParentProfileActivity)) {
            showProgressDialog(this@ParentProfileActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@ParentProfileActivity)
            apiHelper!!.callWB(Constants.POST_PARENT_PROFILE_DETAILS, this@ParentProfileActivity, dataMap, Constants.W_USER_DETAILS, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@ParentProfileActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun validateInput() {

        when {
            TextUtils.isEmpty(edtFirstNameEditParent.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_first_name))
            }
            TextUtils.isEmpty(edtOccupationEditParent.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_occupation))
            }
            TextUtils.isEmpty(edtEducationEditParent.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_education))
            }
            TextUtils.isEmpty(edtPhoneNumberEditParent.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_phone_number))
            }
            !isPhoneValid(edtPhoneNumberEditParent.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.validate_phone))
            }
            else -> {

                if (APIUtils.isOnline(this@ParentProfileActivity)) {
                    showProgressDialog(this@ParentProfileActivity)
//                    Logg.e(TAG, "request param date===>" + Utils.convertDDMMYYYTOYYYYMMDD(edtEducationEditParent.text.toString()))
//                val dataMap = HashMap<String, String>()
//                dataMap[WSKey.FIRST_NAME] = edtFirstName.text.toString()
//                dataMap[WSKey.LAST_NAME] = edtLastName.text.toString()
//                dataMap[WSKey.EMAIL] = edtEmailAddress.text.toString()
//                dataMap[WSKey.DATE_OF_BIRTH] = Utils.convertDDMMYYYTOYYYYMMDD(tvDateOfBirth.text.toString())
//                apiHelper.callWB(Constants.POST_URL_REGISTER, this@ParentProfileActivity, dataMap, Constants.W_REGISTER, this@ParentProfileActivity, true, false, true, Constants.POST)
                    apiHelper.callUpdateProfileParent(Constants.POST_PARENT_EDIT_PROFILE, this@ParentProfileActivity, Constants.W_EDIT_PROFILE, this@ParentProfileActivity, false, false, false, Constants.POST,
                            finalFile, edtFirstNameEditParent.text.toString(), edtOccupationEditParent.text.toString(), edtEducationEditParent.text.toString(), edtPhoneNumberEditParent.text.toString(),
                            Prefs.getString(PrefsKey.ID, "", this@ParentProfileActivity))
                } else {
                    Toast.show(this@ParentProfileActivity, Constants.PleaseCheckInternetConnectionEng)
                }
            }
        }

    }

    private fun isPhoneValid(phoneNumber: String): Boolean {

        if (phoneNumber.length < 10){
            return false
        }

        if (phoneNumber.toLong() == 0L) {
            return false
        }

        return true
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(FragmentMyAccount.TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            Constants.W_USER_DETAILS -> {
                val gson = Gson()
                val parentProfileResponse = gson.fromJson<ParentProfileResponse>(mRes.toString(), ParentProfileResponse::class.java!!)

                if (parentProfileResponse.flag) {
                    if (parentProfileResponse.userDetails != null && parentProfileResponse.userDetails.isNotEmpty()) {
                        setProfileDetails(parentProfileResponse.userDetails[0])
                    }
                } else {
                    Toast.showLongToast(this@ParentProfileActivity, parentProfileResponse.message)
                }
            }
            Constants.W_EDIT_PROFILE -> {
                val gson = Gson()
                val userDetailsResponse = gson.fromJson<ProfileDetailsResponse>(mRes.toString(), ProfileDetailsResponse::class.java)

                if (userDetailsResponse.flag) {
                    Toast.showLongToast(this@ParentProfileActivity, userDetailsResponse.message)

                    val loginDetailsItemList: ArrayList<LoginDetailsItem> = userDetailsResponse!!.userDetails

                    Prefs.setString(PrefsKey.EMAIL, loginDetailsItemList[0]!!.email, this@ParentProfileActivity)
                    Prefs.setString(PrefsKey.FIRST_NAME, loginDetailsItemList[0]!!.firstName, this@ParentProfileActivity)
                    Prefs.setString(PrefsKey.DATE_OF_BIRTH, loginDetailsItemList[0]!!.dateOfBirth, this@ParentProfileActivity)
                    Prefs.setString(PrefsKey.LAST_NAME, loginDetailsItemList[0]!!.lastName, this@ParentProfileActivity)
                    Prefs.setString(PrefsKey.PROFILE_IMAGE, loginDetailsItemList[0]!!.profileImage, this@ParentProfileActivity)

                    finish()
                } else {
                    Toast.showLongToast(this@ParentProfileActivity, userDetailsResponse.message)
                }
            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@ParentProfileActivity, getString(R.string.meesage_connection_timeout))
    }

    private fun setProfileDetails(profileDetailsItem: UserDetailsItem) {

        edtFirstNameEditParent.setText(profileDetailsItem.fatherFullName)
        edtOccupationEditParent.setText(profileDetailsItem.fatherOccupation)
        edtEducationEditParent.setText(profileDetailsItem.fatherEducation)
        edtPhoneNumberEditParent.setText(profileDetailsItem.fatherPhoneNo)

        if (!profileDetailsItem.profileImage.isNullOrBlank()) {
            Glide.with(this@ParentProfileActivity)
                    .load(profileDetailsItem.profileImage)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            hideProgressDialog()
                            return false
                        }
                    })
                    .into(ivProfileImageParent)
        }

        /*Glide.with(this@ParentProfileActivity)
                .load(profileDetailsItem.profileImage)
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .listener(object  : RequestListener<String, GlideDrawable> {
                    override fun onException(e: java.lang.Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                        Logg.e(TAG, "<=== onResourceReady ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                        hideProgressDialog()
                        return false
                    }

                    override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                        Logg.e(TAG, "<=== onException ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                        hideProgressDialog()
                        return false
                    }

                })
                .into(ivProfileImageParent)*/

    }

    private fun checkCameraPermission() {

        var permissiongratned: Boolean? = null
        val permissionlistener = object : PermissionListener {
            override fun onPermissionGranted() {
                permissiongratned = true
//                uploadPhoto()
                // start picker to get image for cropping and then use the image in cropping activity
                CropImage.activity().start(this@ParentProfileActivity)

            }

            override fun onPermissionDenied(deniedPermissions: ArrayList<String>) {
                permissiongratned = false
            }
        }

        TedPermission.with(this@ParentProfileActivity)
                .setPermissionListener(permissionlistener)
//                .setRationaleMessage("we need permission to access your external storage")
//                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                .setGotoSettingButtonText("setting")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        imageInputHelperNew!!.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                if (data != null) {
                    var result: CropImage.ActivityResult = CropImage.getActivityResult(data)
                    if (resultCode == Activity.RESULT_OK) {
                        showProgressDialog(this@ParentProfileActivity)
                        var resultUri: Uri = result.uri
                        finalFile = File(resultUri.path)


                        Glide.with(this@ParentProfileActivity)
                                .load(finalFile)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        hideProgressDialog()
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        hideProgressDialog()
                                        return false
                                    }
                                })
                                .into(ivProfileImageParent)

                        /*Glide.with(this@ParentProfileActivity)
                                .load(finalFile)
                                .placeholder(R.mipmap.ic_launcher)
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .dontAnimate()
                                .listener(object : RequestListener<File, GlideDrawable> {
                                    override fun onResourceReady(resource: GlideDrawable?, model: File?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                                        Logg.e(TAG, "<=== onResourceReady ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                                        hideProgressDialog()
                                        return false
                                    }
                                    override fun onException(e: java.lang.Exception?, model: File?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                                        Logg.e(TAG, "<=== onException ===>")
//                                        pbProfileImage.visibility = View.INVISIBLE
                                        hideProgressDialog()
                                        return false
                                    }
                                })
                                .into(ivProfileImageParent)*/
                        Logg.e("image_path:", "" + finalFile + "");
                        isUpdatePhoto = true

                    }
                }

            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onImageSelectedFromGallery(uri: Uri?, imageFile: File?) {
        try {
            imageInputHelperNew!!.requestCropImage(imageFile, uri, 1, 800, 600, 1, 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onImageTakenFromCamera(uri: Uri?, imageFile: File?) {
        try {
            val mDisplay = windowManager.defaultDisplay
            imageInputHelperNew!!.requestCropImage(imageFile, uri, 1, mDisplay.width, mDisplay.height, 1, 1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onImageCropped(uri: Uri?, imageFile: File?) {
        try {
            Logg.e("ProfileActivity", "onImageCropped: " + imageFile!!.getAbsolutePath())
            imageFilePath = imageFile.absolutePath
            Logg.e("ProfileActivity", "imagepath >>" + imageFilePath)
            val filePath = imageFile.path
//             profileBitmap : Bitmap?= BitmapFactory.decodeFile(imageFilePath)
//            ivProfileImageParent.setImageBitmap(profileBitmap)

            Glide.with(this@ParentProfileActivity)
                    .load(finalFile)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            return false
                        }
                    })
                    .into(ivProfileImageParent)

            /*Glide.with(this@ParentProfileActivity)
                    .load(imageFile)
                    .placeholder(R.mipmap.ic_launcher)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .listener(object : RequestListener<File, GlideDrawable> {
                        override fun onResourceReady(resource: GlideDrawable?, model: File?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                            Logg.e(TAG, "<=== onResourceReady ===>")
//                            pbProfileImage.visibility = View.INVISIBLE
                            return false
                        }
                        override fun onException(e: java.lang.Exception?, model: File?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                            Logg.e(TAG, "<=== onException ===>")
//                            pbProfileImage.visibility = View.INVISIBLE
                            return false
                        }
                    })
                    .into(ivProfileImageParent)*/

            finalFile = File(imageFile.getAbsolutePath());
            Logg.e("image_path:", "" + finalFile + "")

            isUpdatePhoto = true

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun changeInputsMode() {

        if (isEditMode) {

            ivProfileImageParent.isEnabled = true
            edtFirstNameEditParent.isEnabled = true
            edtOccupationEditParent.isEnabled = true
            edtEducationEditParent.isEnabled = true
            edtPhoneNumberEditParent.isEnabled = true
//            llSaveProfileParent.isEnabled = true

            llSaveProfileParent.visibility = View.VISIBLE

            edtFirstNameEditParent.alpha = 1f
            edtOccupationEditParent.alpha = 1f
            edtEducationEditParent.alpha = 1f
            edtPhoneNumberEditParent.alpha = 1f
//            llSaveProfileParent.alpha = 1f


        } else {

            ivProfileImageParent.isEnabled = false
            edtFirstNameEditParent.isEnabled = false
            edtOccupationEditParent.isEnabled = false
            edtEducationEditParent.isEnabled = false
            edtPhoneNumberEditParent.isEnabled = false
//            llSaveProfileParent.isEnabled = false

            llSaveProfileParent.visibility = View.INVISIBLE

            edtFirstNameEditParent.alpha = 0.4f
            edtOccupationEditParent.alpha = 0.4f
            edtEducationEditParent.alpha = 0.4f
            edtPhoneNumberEditParent.alpha = 0.4f
//            llSaveProfileParent.alpha = 0.4f

        }

    }
}
