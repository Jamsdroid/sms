package com.sms.activity

import android.os.Bundle
import android.text.TextUtils
import com.alcohol.core.BaseActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.sms.R
import com.sms.common.*
import com.sms.common.Constants.Companion.SOMETHING_WENT_WRONG
import com.sms.common.Utils.Companion.convertDDMMYYYTOYYYYMMDD
import com.sms.common.Utils.Companion.selectBirthDate
import com.sms.common.Utils.Companion.validatePassword
import com.sms.model.authentication.RegisterResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.RetroFitResponse
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject
import java.util.*

class RegisterActivity : BaseActivity(), RetroFitResponse, Constants {

    private val TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        tvBackRegister.setOnClickListener {
            finish()
        }

        tvLoginHere.setOnClickListener {
            finish()
        }

        tvDateOfBirth.setOnClickListener {
            hideKeyboard(this@RegisterActivity)
            selectBirthDate(this@RegisterActivity, tvDateOfBirth, null)
        }

        llSignup.setOnClickListener {

            hideKeyboard(this@RegisterActivity)
            var deviceToken = Prefs.getString(PrefsKey.DEVICE_TOKEN, "", this@RegisterActivity)

            if (deviceToken.isNullOrBlank()) {
                FirebaseApp.initializeApp(this@RegisterActivity)
                deviceToken = "Basic " + FirebaseInstanceId.getInstance().token.toString()
                Prefs.setString(PrefsKey.DEVICE_TOKEN, deviceToken, this@RegisterActivity)
            }

            validateInput()

//            if (validateInput(email, password)) {
//                val dataMap = HashMap<String, String>()
//                dataMap[WSKey.EMAIL] = email
//                dataMap[WSKey.PASSWORD] = password
//                dataMap[WSKey.DEVICE_TYPE] = DEVICE_TYPE_VALUE
//                dataMap[WSKey.DEVICE_TOKEN] = deviceToken
//                apiHelper.callWB(Constants.POST_URL_LOGIN, this@RegisterActivity, dataMap, Constants.W_LOGIN, this@RegisterActivity, true, false, true, Constants.POST)
//            }

        }
    }

    private fun validateInput() {

        when {
            TextUtils.isEmpty(edtFirstName.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_first_name))
            }
            TextUtils.isEmpty(edtLastName.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_last_name))
            }
            TextUtils.isEmpty(tvDateOfBirth.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_bithdate))
            }
            TextUtils.isEmpty(edtEmailAddress.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_email))
            }
            !isEmailValid(edtEmailAddress.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.validate_email))
            }
            TextUtils.isEmpty(edtPasswordRegister.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.empty_password))
            }
            !validatePassword(edtPasswordRegister.text.toString().trim()) -> {
                Toast.show(this, resources.getString(R.string.validate_password))
            }
            else -> {
                var deviceToken = Prefs.getString(PrefsKey.DEVICE_TOKEN, "", this@RegisterActivity).toString()

                if (!deviceToken.isNullOrBlank()) {
                    Logg.e(TAG, "Stored Device Token ===> " + Prefs.getString(PrefsKey.DEVICE_TOKEN, "", this@RegisterActivity))
                    FirebaseApp.initializeApp(this@RegisterActivity)
                    deviceToken = FirebaseInstanceId.getInstance().token.toString()
                    Prefs.setString(PrefsKey.DEVICE_TOKEN, deviceToken, this@RegisterActivity)
                }

                Logg.e(TAG, "request param date===>" + convertDDMMYYYTOYYYYMMDD(tvDateOfBirth.text.toString()))
                val dataMap = HashMap<String, String>()
                dataMap[WSKey.FIRST_NAME] = edtFirstName.text.toString()
                dataMap[WSKey.LAST_NAME] = edtLastName.text.toString()
                dataMap[WSKey.PASSWORD] = edtPasswordRegister.text.toString()
                dataMap[WSKey.EMAIL] = edtEmailAddress.text.toString()
                dataMap[WSKey.DATE_OF_BIRTH] = convertDDMMYYYTOYYYYMMDD(tvDateOfBirth.text.toString())
                dataMap[WSKey.DEVICE_TYPE] = AppConstants.DEVICE_TYPE_VALUE
                dataMap[WSKey.DEVICE_TOKEN] = deviceToken
                apiHelper.callWB(Constants.POST_URL_REGISTER, this@RegisterActivity, dataMap, Constants.W_REGISTER, this@RegisterActivity, true, false, true, Constants.POST)
            }
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {

        try {
            if (!mRes.toString().isNullOrBlank()) {
                //I am managing both soical and simple registration in single response with W_REGISTER

                Logg.e(TAG, responseTag + " Response " + mRes.toString())
                val responseObject = JSONObject(mRes.toString())
//                if (responseObject.optInt("status_code") == 400) {
//
//                    if (responseTag.equals(Constants.W_REGISTER, ignoreCase = true)) {
//                        dismissDialog()
//                        if (responseObject.getString("msg").equals("Somthing went to wrong. Please try again.")) {
//
//                            val errorObject = responseObject.getJSONObject("errors")
//
//                            val iter = errorObject.keys()
//                            while (iter.hasNext()) {
//                                val key = iter.next() as String
//                                val value = errorObject.getString(key)
//                                Toast.show(this@RegisterActivity, value)
////                                if (!(value.equals("The requested email is already in use", true))) {
////                                }
//
//                                break
//                            }
//                            // hide because sign up with fc and google hides
//                            /*Prefs.setBoolean(Constants.SP_SOCIAL_REGISTRATION, true, this@RegisterActivity)
//
//                            Logg.e(TAG, "funllName ===> " + personData.fullName)
//                            Logg.e(TAG, "email ===> " + personData.emailID)
//                            Logg.e(TAG, "FbId ===> " + personData.fbId)
//                            Logg.e(TAG, "GoogleId ===> " + personData.googleId)
//                            passData(personData.fullName, personData.emailID, personData.fbId, personData.googleId)*/
//                        }
//                    } else {
//                        dismissDialog()
//                        if (responseObject.has("errors")) {
//                            val errorObject = responseObject.getJSONObject("errors")
//
//                            val iter = errorObject.keys()
//                            while (iter.hasNext()) {
//                                val key = iter.next() as String
//                                val value = errorObject.getString(key)
//                                com.alcohol.common.Toast.show(this@RegisterActivity, value)
//                                break
//                            }
//                        }
//                    }
//                } else if (responseObject.optInt("status_code") == 200) {

                if (responseTag.equals(Constants.W_REGISTER, ignoreCase = true)) {
                    val gson = Gson()
                    val modelRegistration = gson.fromJson<RegisterResponse>(responseObject.toString(), RegisterResponse::class.java)

//                        Toast.show(this@RegisterActivity, getString(R.string.message_register_success))
                    if (modelRegistration.flag) {
                        Toast.showLongToast(this@RegisterActivity, modelRegistration.message)
                        finish()
                    } else {
                        Toast.showLongToast(this@RegisterActivity, modelRegistration.message)
                    }
//                        else if (responseObject.getInt("status_code") == 400) {
//                            if (responseObject.getString("msg").equals("Somthing went to wrong. Please try again.")) {
//                                Prefs.setBoolean(Constants.SP_SOCIAL_REGISTRATION, true, this@RegisterActivity)
//
//                                Logg.e(TAG, "funllName ===> " + personData.fullName)
//                                Logg.e(TAG, "email ===> " + personData.emailID)
//                                Logg.e(TAG, "FbId ===> " + personData.fbId)
//                                Logg.e(TAG, "GoogleId ===> " + personData.googleId)
//                                passData(personData.fullName, personData.emailID, personData.fbId, personData.googleId)
//                            }
//                        }
                } else {
//                        dismissDialog()
                    Toast.show(this@RegisterActivity, SOMETHING_WENT_WRONG)
                }
//                }
            }
        } catch (e: Exception) {
            Logg.e(TAG, " =====================> exeption ===> " + e.message)
//            dismissDialog()
            e.printStackTrace()
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {

        Toast.showLongToast(this@RegisterActivity, getString(R.string.meesage_connection_timeout))
    }
}
