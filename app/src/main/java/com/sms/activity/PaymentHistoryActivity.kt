package com.sms.activity

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.PaymentListAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.POST_PAYMENT_HISTORY
import com.sms.common.Constants.Companion.W_PAYMENT_HISTORY
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.paymenthistory.PaymentHistoryResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_payment_history.*
import java.util.*

class PaymentHistoryActivity : BaseActivity() {

    private var TAG: String = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_history)

        var linearLayoutManager = LinearLayoutManager(this@PaymentHistoryActivity)
        rcvPaymentList.layoutManager = linearLayoutManager
//        rcvPaymentList.addItemDecoration(DividerItemDecoration(this@PaymentHistoryActivity, DividerItemDecoration.VERTICAL))


        tvBackPaymentHistory.setOnClickListener {
            finish()
        }

        getPaymentHistory()
    }

    private fun getPaymentHistory() {

        if (APIUtils.isOnline(this@PaymentHistoryActivity)) {
            showProgressDialog(this@PaymentHistoryActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@PaymentHistoryActivity)
            apiHelper!!.callWB(POST_PAYMENT_HISTORY, this@PaymentHistoryActivity, dataMap, W_PAYMENT_HISTORY, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@PaymentHistoryActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {

            W_PAYMENT_HISTORY -> {

                val gson = Gson()
                val paymentResponse = gson.fromJson<PaymentHistoryResponse>(mRes.toString(), PaymentHistoryResponse::class.java!!)

                if (paymentResponse.flag) {

                    if (paymentResponse.totalAmount.isNullOrBlank()) {
                        tvTotalPrice.text = getString(R.string.symbol_rupee) + "0"
                    } else {
                        tvTotalPrice.text = getString(R.string.symbol_rupee) + paymentResponse.totalAmount
                    }

                    if (paymentResponse.paymentList != null && paymentResponse.paymentList.size > 0) {

                        rcvPaymentList.visibility = View.VISIBLE
                        tvNoPaymentFound.visibility = View.GONE

                        var allPaymentsList = paymentResponse.paymentList
                        var cartListAdapter = PaymentListAdapter(this@PaymentHistoryActivity, allPaymentsList!!)
                        rcvPaymentList.adapter = cartListAdapter

                    } else {

                        rcvPaymentList.visibility = View.GONE
                        tvNoPaymentFound.visibility = View.VISIBLE

                    }

//                    if (paymentResponse.message == "Your book has been added to cart successfully") {
//                        Toast.showLongToast(this@PaymentHistoryActivity, paymentResponse.message)
//                        finish()
//                    } else {
//                        Toast.showLongToast(this@PaymentHistoryActivity, paymentResponse.message)
//                    }

                } else {
                    Toast.showLongToast(this@PaymentHistoryActivity, paymentResponse.message)
                }

            }
        }
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@PaymentHistoryActivity, getString(R.string.meesage_connection_timeout))

    }
}
