package com.sms.activity

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.ClassSpinnerAdapter
import com.sms.adapters.CoursesAdapter
import com.sms.adapters.SubjectsSpinnerAdapter
import com.sms.common.AppConstants
import com.sms.common.Constants
import com.sms.common.Constants.Companion.POST_CLASS_LIST
import com.sms.common.Constants.Companion.POST_SUBJECT_LIST
import com.sms.common.Constants.Companion.W_CLASS_LIST
import com.sms.common.Constants.Companion.W_SUBJECT_LIST
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.loadmore.OnLoadMoreListener
import com.sms.loadmore.RecyclerViewLoadMoreScroll
import com.sms.model.filter.ClassListItem
import com.sms.model.filter.ClassListResponse
import com.sms.model.filter.SubjectListItem
import com.sms.model.filter.SubjectListResponse
import com.sms.model.home.CourseDetailsItem
import com.sms.model.home.DashboardResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_filter_course.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

class FilterCourseActivity : BaseActivity(), Constants {

    var TAG: String = javaClass.simpleName
    var subjectTypeValue: String = ""

    private var classId: String = ""
    private var subjectId: String = ""

    var dummyClassList = ArrayList<ClassListItem>()
    var dummySubjectList = ArrayList<SubjectListItem>()

    lateinit var classListResponse: ClassListResponse

    var scrollListenerGrid: RecyclerViewLoadMoreScroll? = null
    var gridLayoutManager: GridLayoutManager? = null
    var coursesAdapter: CoursesAdapter? = null

    var userDetailsList = ArrayList<CourseDetailsItem>()

    var pageIndex: Int = 0
    var totalPage: Int = 0

    var isFirstTimeLoading = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter_course)

        if (intent.extras != null) {
            subjectTypeValue = intent.extras.getString("subjectTypeValue")
        }

        // set classId which is saved in Login
//        if (!Prefs.getString(PrefsKey.CLASS_ID, "", this@FilterCourseActivity).isNullOrEmpty()) {
//            classId = Prefs.getString(PrefsKey.CLASS_ID, "", this@FilterCourseActivity)
//        }

        var classListItemDefault = ClassListItem()
        classListItemDefault.id = ""
        classListItemDefault.jsonMemberClass = "By Standard"

        dummyClassList.add(classListItemDefault)

        var subjectListItemDefault = SubjectListItem()
        subjectListItemDefault.id = ""
        subjectListItemDefault.name = "By Subjects"

        dummySubjectList.add(subjectListItemDefault)

        gridLayoutManager = GridLayoutManager(this@FilterCourseActivity, 2)

        scrollListenerGrid = RecyclerViewLoadMoreScroll(gridLayoutManager!!)
        scrollListenerGrid!!.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreDataGrid()
            }
        })

        rcvStandardCoursesFilter.layoutManager = GridLayoutManager(this@FilterCourseActivity, 2)
        rcvStandardCoursesFilter.addOnScrollListener(scrollListenerGrid!!)

        tvBackCourseFilter.setOnClickListener {
            finish()
        }

        getClassList()

    }

    private fun loadMoreDataGrid() {
        Logg.e(TAG, "<=== loadMoreDataGrid ===>")

        if (pageIndex < (totalPage - 1)) {
            coursesAdapter!!.addLoadingView()

            pageIndex++
//            callGetStandardCoursesWS(subjectTypeValue, pageIndex, searchedWord)
            callGetStandardCoursesWS(subjectTypeValue, pageIndex, classId, subjectId)

        }
    }

    private fun getClassList() {
        if (APIUtils.isOnline(this@FilterCourseActivity)) {
            showProgressDialog(this@FilterCourseActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@FilterCourseActivity)
            apiHelper.callWB(POST_CLASS_LIST, this@FilterCourseActivity, dataMap, W_CLASS_LIST, this@FilterCourseActivity, false, false, true, Constants.POST)
        } else {
            Toast.show(this@FilterCourseActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun getSubjectList() {
        if (APIUtils.isOnline(this@FilterCourseActivity)) {
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@FilterCourseActivity)
            dataMap[WSKey.SUBJECT_TYPE] = subjectTypeValue
            apiHelper.callWB(POST_SUBJECT_LIST, this@FilterCourseActivity, dataMap, W_SUBJECT_LIST, this@FilterCourseActivity, false, false, true, Constants.POST)
        } else {
            Toast.show(this@FilterCourseActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun callGetStandardCoursesWS(subjectType: String, pageNo: Int, classId: String, subjectId: String) {

        if (APIUtils.isOnline(this@FilterCourseActivity)) {
            showProgressDialog(this@FilterCourseActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@FilterCourseActivity)
            dataMap[WSKey.PAGE_NO] = pageNo.toString()
            dataMap[WSKey.CLASS_ID] = classId
            dataMap[WSKey.SUBJECT_ID] = subjectId

            dataMap[WSKey.SUBJECT_TYPE] = subjectType
//            dataMap[WSKey.SEARCH_TEXT] = searchedText
            apiHelper!!.callWB(Constants.POST_HOME_DASHBOARD, this@FilterCourseActivity, dataMap, Constants.W_HOME_DASHBOARD, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@FilterCourseActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        when (responseTag) {
            W_CLASS_LIST -> {

                if (subjectTypeValue == AppConstants.SUBJECT_TYPE_STANDARD_VALUE) {

                    rlClass.visibility = View.VISIBLE

                    val gson = Gson()
                    classListResponse = gson.fromJson<ClassListResponse>(mRes.toString(), ClassListResponse::class.java!!)

                    if (classListResponse != null && classListResponse.classList != null && classListResponse.classList.size > 0) {
                        dummyClassList.addAll(classListResponse.classList)

                        val adapterClass = ClassSpinnerAdapter(this@FilterCourseActivity, android.R.layout.simple_spinner_dropdown_item, dummyClassList)

                        spClass.adapter = adapterClass

                        spClass.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {

                                try {
                                    classId = dummyClassList[position].id
//                                    if (classId.isNullOrEmpty()) {
//                                        if (!Prefs.getString(PrefsKey.CLASS_ID, "", this@FilterCourseActivity).isNullOrEmpty()) {
//                                            classId = Prefs.getString(PrefsKey.CLASS_ID, "", this@FilterCourseActivity)
//                                        }
//                                    }
                                    Log.e(TAG, "classId ===> $classId")

                                    clearIndexAndDataList()

                                    if (isFirstTimeLoading) {
                                        isFirstTimeLoading = false
                                    } else {
                                        callGetStandardCoursesWS(subjectTypeValue, pageIndex, classId, subjectId)
                                    }

                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {
                            }
                        }
                    }
                } else {
                    rlClass.visibility = View.GONE
                }

                getSubjectList()

            }
            W_SUBJECT_LIST -> {
                hideProgressDialog()

                val gson = Gson()
                val subjectListResponse = gson.fromJson<SubjectListResponse>(mRes.toString(), SubjectListResponse::class.java!!)

                // set dropdown for class and subject

                if (subjectListResponse?.subjectList != null && subjectListResponse.subjectList.isNotEmpty()) {

                    dummySubjectList.addAll(subjectListResponse.subjectList)

                    val adapterSubjects = SubjectsSpinnerAdapter(this@FilterCourseActivity, android.R.layout.simple_spinner_dropdown_item, dummySubjectList)

                    spSubject.adapter = adapterSubjects

                    spSubject.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {

                            try {
                                subjectId = dummySubjectList[position].id
                                Log.e(TAG, "subjectId ===> $subjectId")

//                                if (userDetailsList != null && userDetailsList.size > 0) {
//                                    userDetailsList.clear()
//                                }
//
//                                pageIndex = 0
//                                totalPage = 0

                                clearIndexAndDataList()

                                callGetStandardCoursesWS(subjectTypeValue, pageIndex, classId, subjectId)

                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {
                        }
                    }
                }
            }
            Constants.W_HOME_DASHBOARD -> {
                hideProgressDialog()

                val gson = Gson()
                val dashboardResponse = gson.fromJson<DashboardResponse>(mRes.toString(), DashboardResponse::class.java!!)

                if (dashboardResponse.flag) {

                    totalPage = dashboardResponse.totalPage!!

                    if (totalPage > 1) {
//                        if (dashboardResponse.userDetails != null && dashboardResponse.userDetails.isNotEmpty()) {
//                            coursesAdapter = CoursesAdapter(this@FilterCourseActivity, dashboardResponse.userDetails)
//                            rcvStandardCoursesFilter.adapter = coursesAdapter
//                        }

                        if (userDetailsList != null && userDetailsList.size > 0) {
                            coursesAdapter!!.removeLoadingView()
                            coursesAdapter!!.addData(dashboardResponse.userDetails)
                            coursesAdapter!!.notifyDataSetChanged()
                            scrollListenerGrid!!.setLoaded()

                        } else {
                            userDetailsList!!.addAll(dashboardResponse.userDetails)

                            coursesAdapter = CoursesAdapter(this@FilterCourseActivity, userDetailsList!!)
                            rcvStandardCoursesFilter.adapter = coursesAdapter
                            coursesAdapter!!.notifyDataSetChanged()

                            gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                                override fun getSpanSize(position: Int): Int {
                                    return when (coursesAdapter!!.getItemViewType(position)) {
                                        Constants.VIEW_TYPE_ITEM -> 1
                                        Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                        else -> -1
                                    }
                                }
                            }
                        }

                    } else {
                        userDetailsList!!.addAll(dashboardResponse.userDetails)
//                        productDetailsSearchedList = modelProductList!!.productList

                        coursesAdapter = CoursesAdapter(this@FilterCourseActivity, userDetailsList!!)
                        rcvStandardCoursesFilter.adapter = coursesAdapter
                        coursesAdapter!!.notifyDataSetChanged()

                        gridLayoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                            override fun getSpanSize(position: Int): Int {
                                return when (coursesAdapter!!.getItemViewType(position)) {
                                    Constants.VIEW_TYPE_ITEM -> 1
                                    Constants.VIEW_TYPE_LOADING -> 2 //number of columns of the grid
                                    else -> -1
                                }
                            }
                        }
                    }
                } else {
                    clearIndexAndDataList()

                    coursesAdapter = CoursesAdapter(this@FilterCourseActivity, userDetailsList!!)
                    rcvStandardCoursesFilter.adapter = coursesAdapter
                    coursesAdapter!!.notifyDataSetChanged()

//                    if (userDetailsList !=null && userDetailsList.size > 0) {
//                        userDetailsList.clear()
//
//                        pageIndex = 0
//                        totalPage = 0
//
//                        coursesAdapter = CoursesAdapter(this@FilterCourseActivity, userDetailsList!!)
//                        rcvStandardCoursesFilter.adapter = coursesAdapter
//                        coursesAdapter!!.notifyDataSetChanged()
//                    }

                    Toast.showLongToast(this@FilterCourseActivity, dashboardResponse.message)
                }
            }
        }

    }

    private fun clearIndexAndDataList() {

        if (userDetailsList != null && userDetailsList.size > 0) {
            userDetailsList.clear()
        }

        pageIndex = 0
        totalPage = 0
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@FilterCourseActivity, getString(R.string.meesage_connection_timeout))
    }
}
