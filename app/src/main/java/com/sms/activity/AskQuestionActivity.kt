package com.sms.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.alcohol.core.BaseActivity
import com.google.gson.Gson
import com.sms.R
import com.sms.adapters.ClassSpinnerAdapter
import com.sms.adapters.SubjectsSpinnerAdapter
import com.sms.common.Constants
import com.sms.common.Constants.Companion.POST_ASK_QUESTION
import com.sms.common.Constants.Companion.W_ASK_QUESTION
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.model.common.CommonResponse
import com.sms.model.filter.ClassListItem
import com.sms.model.filter.ClassListResponse
import com.sms.model.filter.SubjectListItem
import com.sms.model.filter.SubjectListResponse
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import kotlinx.android.synthetic.main.activity_ask_question.*
import java.util.*

class AskQuestionActivity : BaseActivity() {

    private var TAG: String = javaClass.simpleName
    lateinit var classListResponse: ClassListResponse

    private var classId: String = ""
    private var subjectId: String = ""

    var dummyClassList = ArrayList<ClassListItem>()
    var dummySubjectList = ArrayList<SubjectListItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ask_question)

        tvBackAskQuestion.setOnClickListener {
            finish()
        }

        llSubmitAskQuestion.setOnClickListener {

            if (checkValidInputs()) {
                callAskQuestionWS()
            }
        }

        var classListItemDefault = ClassListItem()
        classListItemDefault.id = ""
        classListItemDefault.jsonMemberClass = "Select Standard"

        dummyClassList.add(classListItemDefault)

        var subjectListItemDefault = SubjectListItem()
        subjectListItemDefault.id = ""
        subjectListItemDefault.name = "Select Subjects"

        dummySubjectList.add(subjectListItemDefault)

        getClassList()

    }

    private fun getClassList() {
        if (APIUtils.isOnline(this@AskQuestionActivity)) {
            showProgressDialog(this@AskQuestionActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@AskQuestionActivity)
            apiHelper.callWB(Constants.POST_CLASS_LIST, this@AskQuestionActivity, dataMap, Constants.W_CLASS_LIST, this@AskQuestionActivity, false, false, true, Constants.POST)
        } else {
            Toast.show(this@AskQuestionActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun getSubjectList() {
        if (APIUtils.isOnline(this@AskQuestionActivity)) {
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@AskQuestionActivity)
//            dataMap[WSKey.SUBJECT_TYPE] = subjectTypeValue
            apiHelper.callWB(Constants.POST_SUBJECT_LIST, this@AskQuestionActivity, dataMap, Constants.W_SUBJECT_LIST, this@AskQuestionActivity, false, false, true, Constants.POST)
        } else {
            Toast.show(this@AskQuestionActivity, Constants.PleaseCheckInternetConnectionEng)
        }
    }

    private fun callAskQuestionWS() {

        if (APIUtils.isOnline(this@AskQuestionActivity)) {
            showProgressDialog(this@AskQuestionActivity)
            val dataMap = HashMap<String, String>()
            dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", this@AskQuestionActivity)
            dataMap[WSKey.SUBJECT_ID] = subjectId
            dataMap[WSKey.CLASS_ID] = classId
            dataMap[WSKey.QUESTION] = edtTitle.text.toString()
            dataMap[WSKey.DESCRIPTION] = edtDescription.text.toString()
            dataMap[WSKey.QUESTION_ID] = ""
            apiHelper!!.callWB(POST_ASK_QUESTION, this@AskQuestionActivity, dataMap, W_ASK_QUESTION, this, false, false, true, Constants.POST)
        } else {
            Toast.show(this@AskQuestionActivity, Constants.PleaseCheckInternetConnectionEng)
        }

    }

    private fun checkValidInputs(): Boolean {

        when {
            edtTitle.text.isNullOrBlank() -> {
                Toast.show(this@AskQuestionActivity, getString(R.string.enter_title))
                return false
            }
            edtDescription.text.isNullOrBlank() -> {
                Toast.show(this@AskQuestionActivity, getString(R.string.enter_description))
                return false
            }
            classId.isNullOrBlank() -> {
                Toast.show(this@AskQuestionActivity, getString(R.string.select_class))
                return false
            }
            subjectId.isNullOrBlank() -> {
                Toast.show(this@AskQuestionActivity, getString(R.string.select_subject))
                return false
            }
            else -> return true
        }

    }

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
        super.onRetrofitResponse(responseCode, mRes, responseTag)

        Logg.e(TAG, responseTag + " Response " + mRes.toString())

        hideProgressDialog()

        when (responseTag) {
            Constants.W_CLASS_LIST -> {

                val gson = Gson()
                classListResponse = gson.fromJson<ClassListResponse>(mRes.toString(), ClassListResponse::class.java!!)

                if (classListResponse != null && classListResponse.classList != null && classListResponse.classList.size > 0) {
                    dummyClassList.addAll(classListResponse.classList)

                    val adapterClass = ClassSpinnerAdapter(this@AskQuestionActivity, android.R.layout.simple_spinner_dropdown_item, dummyClassList)

                    spClassAskQuestion.adapter = adapterClass

                    spClassAskQuestion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {

                            try {
                                classId = dummyClassList[position].id
                                Log.e(TAG, "classId ===> $classId")

                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {
                        }
                    }
                }

                getSubjectList()

            }
            Constants.W_SUBJECT_LIST -> {
                hideProgressDialog()

                val gson = Gson()
                val subjectListResponse = gson.fromJson<SubjectListResponse>(mRes.toString(), SubjectListResponse::class.java!!)

                // set dropdown for class and subject

                if (subjectListResponse?.subjectList != null && subjectListResponse.subjectList.isNotEmpty()) {

                    dummySubjectList.addAll(subjectListResponse.subjectList)

                    val adapterSubjects = SubjectsSpinnerAdapter(this@AskQuestionActivity, android.R.layout.simple_spinner_dropdown_item, dummySubjectList)

                    spSubjectAskQuestion.adapter = adapterSubjects

                    spSubjectAskQuestion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {

                            try {
                                subjectId = dummySubjectList[position].id
                                Log.e(TAG, "subjectId ===> $subjectId")

                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {
                        }
                    }
                }
            }
            W_ASK_QUESTION -> {
                val gson = Gson()
                val commonResponse = gson.fromJson<CommonResponse>(mRes.toString(), CommonResponse::class.java!!)

                if (commonResponse.flag) {
                    Toast.show(this@AskQuestionActivity, commonResponse.message)
                    finish()
                } else {
                    Toast.show(this@AskQuestionActivity, commonResponse.message)
                }
            }
        }

    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
        super.onRetrofitError(Code, mError, responseTag)

        hideProgressDialog()

        Toast.showLongToast(this@AskQuestionActivity, getString(R.string.meesage_connection_timeout))
    }
}
