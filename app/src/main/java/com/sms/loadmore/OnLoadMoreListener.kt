package com.sms.loadmore

interface OnLoadMoreListener {
    fun onLoadMore()
}
