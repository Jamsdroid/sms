package com.sms.model.parentexamhistory

import com.google.gson.annotations.SerializedName

data class ExamListItem(

        @field:SerializedName("student_id")
        val studentId: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("exam_name")
        val examName: String = "",

        @field:SerializedName("created_date")
        val createdDate: String = "",

        @field:SerializedName("examanswer")
        val examanswer: ArrayList<ExamAnswerItem>,

        @field:SerializedName("exam_result")
        val examResult: ExamResult,

        @field:SerializedName("exam_id")
        val examId: String = ""
)