package com.sms.model.parentexamhistory

import com.google.gson.annotations.SerializedName

data class ExamResult(

        @field:SerializedName("total")
        val total: Int,

        @field:SerializedName("correct_answer")
        val correctAnswer: Int,

        @field:SerializedName("wrong_answer")
        val wrongAnswer: Int
)