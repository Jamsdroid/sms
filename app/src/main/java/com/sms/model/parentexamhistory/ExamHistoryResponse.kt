package com.sms.model.parentexamhistory

import com.google.gson.annotations.SerializedName

data class ExamHistoryResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false,

        @field:SerializedName("EXAMLIST")
        val examList: ArrayList<ExamListItem>
)