package com.sms.model.parentexamhistory

import com.google.gson.annotations.SerializedName

data class ExamAnswerItem(

        @field:SerializedName("answer")
        val answer: String = "",

        @field:SerializedName("question")
        val question: String = "",

        @field:SerializedName("correct_answer")
        val correctAnswer: String = "",

        @field:SerializedName("id")
        val id: String = ""
)