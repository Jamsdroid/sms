package com.sms.model.home

import com.google.gson.annotations.SerializedName

data class CartCountResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("TOTAL_CART")
        val totalCart: Int,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)