package com.sms.model.home

import com.google.gson.annotations.SerializedName

data class AdvertiseResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("ADS_LIST")
        val adsList: List<AdsListItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)