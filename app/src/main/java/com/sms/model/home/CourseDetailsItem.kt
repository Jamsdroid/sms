package com.sms.model.home

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CourseDetailsItem(

        @field:SerializedName("subject_id")
        val subjectId: String = "",

        @field:SerializedName("image")
        val image: String = "",

        @field:SerializedName("class_id")
        val classId: String = "",

        @field:SerializedName("document")
        val document: String = "",

        @field:SerializedName("total_download")
        val totalDownload: String = "",

        @field:SerializedName("description")
        val description: String = "",

        @field:SerializedName("type")
        val type: String = "",

        @field:SerializedName("ratings")
        val ratings: String = "",

        @field:SerializedName("price")
        val price: String = "",

        @field:SerializedName("is_free")
        val isFree: String = "",

        @field:SerializedName("name")
        val name: String = "",

        @field:SerializedName("subject_name")
        val subjectName: String = "",

        @field:SerializedName("subject_type")
        val subjectType: String = "",

        @field:SerializedName("course_version_id")
        val courseVersionId: String = "",

        @field:SerializedName("is_purchased")
        val isPurchased: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("class")
        val jsonMemberClass: Any? = null,

        @field:SerializedName("course_created_by")
        val courseCreatedBy: String = "",

        @field:SerializedName("status")
        val status: String = ""
) : Serializable {
    constructor() : this(
            "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""
    )

    val isEmpty: Boolean
        get() {
            if (subjectId.isNotEmpty()) {
                return false
            }
            return true
        }
}