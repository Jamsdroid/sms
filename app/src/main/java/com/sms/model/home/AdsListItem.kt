package com.sms.model.home

import com.google.gson.annotations.SerializedName

data class AdsListItem(

        @field:SerializedName("end_date")
        val endDate: String = "",

        @field:SerializedName("ads_text")
        val adsText: String = "",

        @field:SerializedName("ads_desc")
        val adsDesc: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("ads_image")
        val adsImage: String = "",

        @field:SerializedName("created_date")
        val createdDate: String = "",

        @field:SerializedName("start_date")
        val startDate: String = "",

        @field:SerializedName("status")
        val status: String = ""
)