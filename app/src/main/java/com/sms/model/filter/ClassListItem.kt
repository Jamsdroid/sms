package com.sms.model.filter

import com.google.gson.annotations.SerializedName

data class ClassListItem(

        @field:SerializedName("id")
        var id: String = "",

        @field:SerializedName("class")
        var jsonMemberClass: String = "",

        @field:SerializedName("status")
        var status: String = ""
)