package com.sms.model.filter

import com.google.gson.annotations.SerializedName

data class ClassListResponse(

	@field:SerializedName("MESSAGE")
	val message: String = "",

	@field:SerializedName("FLAG")
	val flag: Boolean = false,

	@field:SerializedName("IS_ACTIVE")
	val isActive: Boolean = false,

	@field:SerializedName("CLASS_LIST")
	val classList: ArrayList<ClassListItem>
)