package com.sms.model.filter

import com.google.gson.annotations.SerializedName

data class SubjectListResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("SUBJECT_LIST")
        val subjectList: List<SubjectListItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)