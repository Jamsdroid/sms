package com.sms.model.filter

import com.google.gson.annotations.SerializedName

data class SubjectListItem(

	@field:SerializedName("image")
	var image: String = "",

	@field:SerializedName("subject_type")
	var subjectType: String = "",

	@field:SerializedName("name")
	var name: String = "",

	@field:SerializedName("id")
	var id: String = "",

	@field:SerializedName("status")
	var status: String = ""
)