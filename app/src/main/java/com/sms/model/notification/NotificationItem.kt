package com.sms.model.notification

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class NotificationItem(

        @field:SerializedName("student_name")
        val studentName: String = "",

        @field:SerializedName("profile_image")
        val profileImage: String = "",

        @field:SerializedName("teacher_name")
        val teacherName: String = "",

        @field:SerializedName("school_id")
        val schoolId: String = "",

        @field:SerializedName("teacher_id")
        val teacherId: String = "",

        @field:SerializedName("student_msg_id")
        val studentMsgId: String = "",

        @field:SerializedName("student_id")
        val studentId: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("created_date")
        val createdDate: String = "",

        @field:SerializedName("message")
        val message: String = "",

        @field:SerializedName("reply_message")
        val replyMessage: String = "",

        @field:SerializedName("reply_date")
        val replyDate: String = ""

)  : Serializable {
        constructor() : this(
                "", "", "", "", "", "", "", "", "", "",
                "", ""

        )

        var isEmpty: Boolean = false
                get() {
                        if (id.isNotEmpty()) {
                                return false
                        }
                        return true
                }
}