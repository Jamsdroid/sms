package com.sms.model.notification

import com.google.gson.annotations.SerializedName

data class NotificationListResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("question")
        val question: ArrayList<NotificationItem>,

        @field:SerializedName("total_page")
        val totalPage: Int,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)