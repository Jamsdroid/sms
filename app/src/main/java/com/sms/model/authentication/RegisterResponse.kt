package com.sms.model.authentication

import com.google.gson.annotations.SerializedName

data class RegisterResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("FLAG")
        val flag: Boolean = false
)
