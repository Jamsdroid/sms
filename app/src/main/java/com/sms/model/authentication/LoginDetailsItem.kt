package com.sms.model.authentication

import com.google.gson.annotations.SerializedName

data class LoginDetailsItem(

	@field:SerializedName("profile_image")
	val profileImage: String = "",

	@field:SerializedName("date_of_birth")
	val dateOfBirth: String = "",

	@field:SerializedName("device_token")
	val deviceToken: String = "",

	@field:SerializedName("is_verify")
	val isVerify: String = "",

	@field:SerializedName("last_name")
	val lastName: String = "",

	@field:SerializedName("device_type")
	val deviceType: String = "",

	@field:SerializedName("id")
	val id: String = "",

	@field:SerializedName("first_name")
	val firstName: String = "",

	@field:SerializedName("email")
	val email: String = "",

	@field:SerializedName("father_email")
	val fatherEmail: String = "",

	@field:SerializedName("user_type")
	val userType: String = "",

	@field:SerializedName("class_id")
	val classID: String = "",

	@field:SerializedName("status")
	val status: String = ""
)