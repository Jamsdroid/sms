package com.sms.model.authentication

import com.google.gson.annotations.SerializedName

data class ForgotPasswordResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false,

        @field:SerializedName("FLAG")
        val flag: Boolean = false
)