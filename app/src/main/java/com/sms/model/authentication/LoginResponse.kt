package com.sms.model.authentication

import com.google.gson.annotations.SerializedName

data class LoginResponse(

        @field:SerializedName("MESSAGE")
        var message: String = "",

        @field:SerializedName("LOGIN_DETAILS")
        var loginDetails: ArrayList<LoginDetailsItem>,

        @field:SerializedName("FLAG")
        var flag: Boolean = false
)