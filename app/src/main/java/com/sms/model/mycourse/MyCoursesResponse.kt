package com.sms.model.mycourse

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MyCoursesResponse (

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("PAYMENT_LIST")
        val myCourseList: ArrayList<MyCourseListItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false

) : Serializable