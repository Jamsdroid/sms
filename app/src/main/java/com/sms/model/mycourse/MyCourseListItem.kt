package com.sms.model.mycourse

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MyCourseListItem(

        @field:SerializedName("course_id")
        val courseId: String = "",

        @field:SerializedName("user_id")
        val userId: String = "",

        @field:SerializedName("document")
        val document: String = "",

        @field:SerializedName("name")
        val name: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("cart_type")
        val cartType: String = ""
) : Serializable