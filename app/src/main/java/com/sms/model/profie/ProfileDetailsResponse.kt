package com.sms.model.profie

import com.google.gson.annotations.SerializedName
import com.sms.model.authentication.LoginDetailsItem

data class ProfileDetailsResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("USER_DETAILS")
        val userDetails: ArrayList<LoginDetailsItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)