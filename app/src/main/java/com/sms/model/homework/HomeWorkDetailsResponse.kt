package com.sms.model.homework

import com.google.gson.annotations.SerializedName

data class HomeWorkDetailsResponse (

        @field:SerializedName("MESSAGE")
    var message: String = "",

        @field:SerializedName("HOME_WORK_QUE_LIST")
    var homeWorkQueList: ArrayList<HomeWorkDetailsItem>,

        @field:SerializedName("total_page")
    var totalPage: Int,

        @field:SerializedName("FLAG")
    var flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
    var isActive: Boolean = false
)