package com.sms.model.homework

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class HomeWorkListItem(

        @field:SerializedName("id")
        var id: String = "",

        @field:SerializedName("home_work_id")
        var homeWorkId: String = "",

        @field:SerializedName("student_id")
        var studentId: String = "",

        @field:SerializedName("name")
        var name: String = "",

        @field:SerializedName("image")
        var image: String = "",

        @field:SerializedName("total_questions")
        var totalQuestions: String = "",

        @field:SerializedName("home_work_created_by")
        var homeWorkCreatedBy: String = ""

) : Serializable {
    constructor() : this(
            "", "", "", "", "", "", ""
    )

    var isEmpty: Boolean = false
        get() {
            if (id.isNotEmpty()) {
                return false
            }
            return true
        }
}