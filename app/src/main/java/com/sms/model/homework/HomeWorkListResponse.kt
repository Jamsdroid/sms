package com.sms.model.homework

import com.google.gson.annotations.SerializedName

data class HomeWorkListResponse (

    @field:SerializedName("MESSAGE")
    var message: String = "",

    @field:SerializedName("HOME_WORK_LIST")
    var homeWorkDetails: ArrayList<HomeWorkListItem>,

    @field:SerializedName("total_page")
    var totalPage: Int,

    @field:SerializedName("FLAG")
    var flag: Boolean = false,

    @field:SerializedName("IS_ACTIVE")
    var isActive: Boolean = false
)