package com.sms.model.homework

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class HomeWorkDetailsItem(

        @field:SerializedName("id")
        var id: String = "",

        @field:SerializedName("home_work_id")
        var homeWorkId: String = "",

        @field:SerializedName("student_id")
        var studentId: String = "",

        @field:SerializedName("question_id")
        var questionId: String = "",

        @field:SerializedName("question")
        var question: String = "",

        @field:SerializedName("status")
        var status: String = ""

) : Serializable {
    constructor() : this(
            "", "", "", "", "", ""
    )

    var isEmpty: Boolean = false
        get() {
            if (questionId.isNotEmpty()) {
                return false
            }
            return true
        }
}