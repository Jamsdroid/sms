package com.sms.model.parent

import com.google.gson.annotations.SerializedName

data class ParentProfileResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("USER_DETAILS")
        val userDetails: ArrayList<UserDetailsItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)