package com.sms.model.parent

import com.google.gson.annotations.SerializedName

data class UserDetailsItem(

        @field:SerializedName("phone_no")
        val phoneNo: String = "",

        @field:SerializedName("country")
        val country: String = "",

        @field:SerializedName("gender")
        val gender: String = "",

        @field:SerializedName("city")
        val city: String = "",

        @field:SerializedName("date_of_birth")
        val dateOfBirth: String = "",

        @field:SerializedName("father_email")
        val fatherEmail: String = "",

        @field:SerializedName("class_id")
        val classId: String = "",

        @field:SerializedName("description")
        val description: String = "",

        @field:SerializedName("device_type")
        val deviceType: String = "",

        @field:SerializedName("mother_occupation")
        val motherOccupation: String = "",

        @field:SerializedName("father_education")
        val fatherEducation: String = "",

        @field:SerializedName("mother_full_name")
        val motherFullName: String = "",

        @field:SerializedName("password")
        val password: String = "",

        @field:SerializedName("profile_image")
        val profileImage: String = "",

        @field:SerializedName("academic_year")
        val academicYear: String = "",

        @field:SerializedName("school_id")
        val schoolId: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("state")
        val state: String = "",

        @field:SerializedName("first_name")
        val firstName: String = "",

        @field:SerializedName("email")
        val email: String = "",

        @field:SerializedName("unique_id")
        val uniqueId: String = "",

        @field:SerializedName("address")
        val address: String = "",

        @field:SerializedName("father_occupation")
        val fatherOccupation: String = "",

        @field:SerializedName("date_of_admission")
        val dateOfAdmission: String = "",

        @field:SerializedName("is_verify")
        val isVerify: String = "",

        @field:SerializedName("last_name")
        val lastName: String = "",

        @field:SerializedName("division_id")
        val divisionId: String = "",

        @field:SerializedName("father_full_name")
        val fatherFullName: String = "",

        @field:SerializedName("middle_name")
        val middleName: String = "",

        @field:SerializedName("is_forgot_password")
        val isForgotPassword: String = "",

        @field:SerializedName("modified_date")
        val modifiedDate: String = "",

        @field:SerializedName("created_by")
        val createdBy: String = "",

        @field:SerializedName("aadhar_number")
        val aadharNumber: String = "",

        @field:SerializedName("zipcode")
        val zipcode: String = "",

        @field:SerializedName("father_phone_no")
        val fatherPhoneNo: String = "",

        @field:SerializedName("device_token")
        val deviceToken: String = "",

        @field:SerializedName("modified_by")
        val modifiedBy: String = "",

        @field:SerializedName("device_udid")
        val deviceUdid: String = "",

        @field:SerializedName("created_date")
        val createdDate: String = "",

        @field:SerializedName("mother_education")
        val motherEducation: String = "",

        @field:SerializedName("status")
        val status: String = ""
)