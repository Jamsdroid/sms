package com.sms.model.paymenthistory

import com.google.gson.annotations.SerializedName

data class PaymentListItem(

        @field:SerializedName("transaction_id")
        val transactionId: String = "",

        @field:SerializedName("date")
        val date: String = "",

        @field:SerializedName("course_id")
        val courseId: String = "",

        @field:SerializedName("image")
        val image: String = "",

        @field:SerializedName("user_id")
        val userId: String = "",

        @field:SerializedName("price")
        val price: String = "",

        @field:SerializedName("name")
        val name: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("cart_type")
        val cartType: String = ""
)