package com.sms.model.paymenthistory

import com.google.gson.annotations.SerializedName

data class PaymentHistoryResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("TOTAL_AMOUNT")
        val totalAmount: String = "",

        @field:SerializedName("PAYMENT_LIST")
        val paymentList: ArrayList<PaymentListItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)