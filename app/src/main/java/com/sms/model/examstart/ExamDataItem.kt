package com.sms.model.examstart

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamDataItem(

        @field:SerializedName("chapter")
        val chapter: String = "",

        @field:SerializedName("exam_bank_id")
        val examBankId: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("exam_time")
        val examTime: String = "",

        @field:SerializedName("student_exam_assign")
        val studentExamAssign: String = "",

        @field:SerializedName("exam_name")
        val examName: String = "",

        @field:SerializedName("eb_question_id")
        val ebQuestionId: String = "",

        @field:SerializedName("exam_question")
        val examQuestion: ArrayList<ExamQuestionItem>


) : Serializable