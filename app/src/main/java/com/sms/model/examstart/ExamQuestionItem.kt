package com.sms.model.examstart

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamQuestionItem(

        @field:SerializedName("question")
        var question: String = "",

        @field:SerializedName("answer")
        var answer: String = "",

        @field:SerializedName("selected_answer")
        var selectedAnswer: String = "",

        @field:SerializedName("answer_status")
        var answerStatus: String = "0",

        @field:SerializedName("exam_bank_id")
        var examBankId: String = "",

        @field:SerializedName("option3")
        var option3: String = "",

        @field:SerializedName("option4")
        var option4: String = "",

        @field:SerializedName("option1")
        var option1: String = "",

        @field:SerializedName("question_file")
        val questionFile: String = "",

        @field:SerializedName("id")
        var id: String = "",

        @field:SerializedName("option2")
        var option2: String = "",

        @field:SerializedName("created_date")
        var createdDate: String = ""

) : Serializable