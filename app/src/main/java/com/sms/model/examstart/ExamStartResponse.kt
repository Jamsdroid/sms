package com.sms.model.examstart

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamStartResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("data")
        val data: ArrayList<ExamDataItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
) : Serializable