package com.sms.model.questionanswer

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class QuestionItem(

        @field:SerializedName("subject_id")
        val subjectId: String = "",

        @field:SerializedName("question")
        val question: String = "",

        @field:SerializedName("description")
        val description: String = "",

        @field:SerializedName("answer")
        val answer: ArrayList<AnswerItem>,

        @field:SerializedName("class_id")
        val classId: String = "",

        @field:SerializedName("name")
        val name: String = "",

        @field:SerializedName("question_by")
        val questionBy: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("created_date")
        val createdDate: String = "",

        @field:SerializedName("created_by")
        val createdBy: String = "",

        @field:SerializedName("class")
        val jsonMemberClass: String = "",

        @field:SerializedName("total_answer")
        val totalAnswer: Int

) : Serializable {
        constructor() : this(
                "", "", "", ArrayList(), "", "", "", "", "", "",
                "", 0

        )

        var isEmpty: Boolean = false
                get() {
                        if (id.isNotEmpty()) {
                                return false
                        }
                        return true
                }
}