package com.sms.model.questionanswer

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AnswerItem(

        @field:SerializedName("answer_by_name")
        val answerByName: String = "",

        @field:SerializedName("profile_image")
        val profileImage: String = "",

        @field:SerializedName("answer_by")
        val answerBy: String = "",

        @field:SerializedName("answer")
        val answer: String = "",

        @field:SerializedName("created_date")
        val createdDate: String = "",

        @field:SerializedName("id")
        val id: String = ""

) : Serializable