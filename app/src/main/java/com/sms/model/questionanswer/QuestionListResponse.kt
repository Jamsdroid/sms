package com.sms.model.questionanswer

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class QuestionListResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("question")
        val question: ArrayList<QuestionItem>,

        @field:SerializedName("total_page")
        val totalPage: Int ,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false

) : Serializable