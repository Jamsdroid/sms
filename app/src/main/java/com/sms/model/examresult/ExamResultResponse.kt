package com.sms.model.examresult

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamResultResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("DATA")
        val data: ExamDataItem,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false

) : Serializable