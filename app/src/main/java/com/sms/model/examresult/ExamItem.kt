package com.sms.model.examresult

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamItem(

        @field:SerializedName("exam_status")
        val examStatus: String = "",

        @field:SerializedName("exam_status_text")
        val examStatusText: String = ""
) : Serializable