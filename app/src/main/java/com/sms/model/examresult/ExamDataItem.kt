package com.sms.model.examresult

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamDataItem(

        @field:SerializedName("exam")
        val exam: ExamItem,

        @field:SerializedName("correct_answer")
        val correctAnswer: Int,

        @field:SerializedName("incorrect_answer")
        val incorrectAnswer: Int,

        @field:SerializedName("not_attempt")
        val notAttempt: Int
) : Serializable