package com.sms.model.questiondetails

import com.google.gson.annotations.SerializedName
import com.sms.model.questionanswer.QuestionItem

data class QuestionDetailsResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("question")
        val question: QuestionItem,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)