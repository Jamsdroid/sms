package com.sms.model.cart

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CartListItem(

        @field:SerializedName("course_id")
        val courseId: String = "",

        @field:SerializedName("image")
        val image: String = "",

        @field:SerializedName("exam_id")
        val examId: String = "",

        @field:SerializedName("cart_type")
        val cartType: String = "",

        @field:SerializedName("user_id")
        val userId: String = "",

        @field:SerializedName("name")
        val name: String = "",

        @field:SerializedName("price")
        val coursePrice: String = "",

        @field:SerializedName("id")
        val id: String = ""

) : Serializable