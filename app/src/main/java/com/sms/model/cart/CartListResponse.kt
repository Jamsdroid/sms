package com.sms.model.cart

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CartListResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("TOTAL_CART")
        val totalCart: Int,

        @field:SerializedName("CART_LIST")
        val cartList: ArrayList<CartListItem>,

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
) : Serializable