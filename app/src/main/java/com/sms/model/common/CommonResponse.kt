package com.sms.model.common

import com.google.gson.annotations.SerializedName

data class CommonResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false
)