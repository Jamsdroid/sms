package com.sms.model.exam

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamListResponse(

        @field:SerializedName("MESSAGE")
        val message: String = "",

        @field:SerializedName("FLAG")
        val flag: Boolean = false,

        @field:SerializedName("IS_ACTIVE")
        val isActive: Boolean = false,

        @field:SerializedName("EXAM_LIST")
        val examList: ArrayList<ExamListItem>

) : Serializable