package com.sms.model.exam

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamChaptersItem(

        @field:SerializedName("subject_id")
        val subjectId: String = "",

        @field:SerializedName("chapter")
        val chapter: String = "",

        @field:SerializedName("class_id")
        val classId: String = "",

        @field:SerializedName("exam_bank_id")
        val examBankId: String = "",

        @field:SerializedName("total_question")
        val totalQuestion: Int? = null,

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("exam_name")
        val examName: String = "",

        @field:SerializedName("eb_question_id")
        val ebQuestionId: String = ""

) : Serializable