package com.sms.model.exam

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExamListItem(

        @field:SerializedName("subject_id")
        val subjectId: String = "",

        @field:SerializedName("image")
        val image: String = "",

        @field:SerializedName("chapter")
        val chapter: String = "",

        @field:SerializedName("total_chapter")
        val totalChapter: String = "",

        @field:SerializedName("price")
        val price: String = "",

        @field:SerializedName("class_id")
        val classId: String = "",

        @field:SerializedName("exam_bank_id")
        val examBankId: String = "",

        @field:SerializedName("is_free")
        val isFree: String = "",

        @field:SerializedName("name")
        val name: String = "",

        @field:SerializedName("id")
        val id: String = "",

        @field:SerializedName("is_purchased")
        val isPurchased: String = "",

        @field:SerializedName("exam_name")
        val examName: String = "",

        @field:SerializedName("exam_created_by")
        val examCreatedBy: String = ""

) : Serializable