package com.sms.merchant;

public class Constants {
	public static final String PARAMETER_SEP = "&";
	public static final String PARAMETER_EQUALS = "=";
	//	public static final String TRANS_URL = "https://test.ccavenue.com/transaction/initTrans"; // testing
	public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans"; // live
	public static final String MERCHANT_ID = "191771";

	// URL:http://germaniuminc.com
	public static final String ACCESS_CODE = "AVNA80FI60BZ98ANZB";
	public static final String WORKING_KEY = "7CFED8E0F573D613BCB5BAEBD24CC168";

	// URL:http://spaarg.com
//	public static final String ACCESS_CODE = "AVBY81FJ01AY82YBYA";
// 	public static final String WORKING_KEY = "C71CAE1181058F50F329AD982A30D03E";

	// URL:http://143.95.252.45
//	public static final String ACCESS_CODE = "AVEN80FI71CH50NEHC";
//	public static final String WORKING_KEY = "09AC9C1D816C6124381BA214E30B3EEE";
}