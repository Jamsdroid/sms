package com.sms.merchant;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.alcohol.core.BaseActivity;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.sms.R;
import com.sms.activity.MainActivity;
import com.sms.common.Logg;
import com.sms.common.Toast;
import com.sms.model.common.CommonResponse;
import com.sms.storage.Prefs;
import com.sms.storage.PrefsKey;
import com.sms.webservice.APIUtils;
import com.sms.webservice.WSKey;

import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class WebViewActivity extends BaseActivity implements com.sms.common.Constants {
    Intent mainIntent;
    String encVal;
    String vResponse;
    private String TAG = getClass().getSimpleName();


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_webview);
        mainIntent = getIntent();

        //get rsa key method
        get_RSA_key(mainIntent.getStringExtra(AvenuesParams.ACCESS_CODE), mainIntent.getStringExtra(AvenuesParams.ORDER_ID));
    }


    private class RenderView extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            LoadingDialog.showLoadingDialog(WebViewActivity.this, "Loading...");

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (!ServiceUtility.chkNull(vResponse).equals("")
                    && ServiceUtility.chkNull(vResponse).toString().indexOf("ERROR") == -1) {
                StringBuffer vEncVal = new StringBuffer("");
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.AMOUNT, mainIntent.getStringExtra(AvenuesParams.AMOUNT)));
                vEncVal.append(ServiceUtility.addToPostParams(AvenuesParams.CURRENCY, mainIntent.getStringExtra(AvenuesParams.CURRENCY)));
                encVal = RSAUtility.encrypt(vEncVal.substring(0, vEncVal.length() - 1), vResponse);  //encrypt amount and currency
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            LoadingDialog.cancelLoading();

            @SuppressWarnings("unused")
            class MyJavaScriptInterface {
                @JavascriptInterface
                public void processHTML(String html) {
                    // process the html source code to get final status of transaction
                    String status = null;

                    Log.d("html", html + "");
                    if (html.indexOf("Failure") != -1) {
                        status = "Transaction Declined!";
                        Toast.Companion.show(getApplicationContext(), status);
                    } else if (html.indexOf("transaction is successful") != -1) {
                        status = "Transaction Successful!";
                        Logg.Companion.e(TAG, "Message ====> $status");
                        //                        Toast.show(getApplicationContext(), status)
                        Document transactionResponse = Jsoup.parse(html);
                        Element tracking_id = transactionResponse.select("td").get(3);

                        Logg.Companion.e(TAG, "tracking_id =====> " + tracking_id.text());
                        getCartListAPI(tracking_id.text());

                        //                        CCAvenueResponse ccAvenueResponse = (CCAvenueResponse) AddToCart.addToCartContext;
                        //                        ccAvenueResponse.onReceiveId(tracking_id.text());
                        //                        finish();
                    } else if (html.indexOf("Aborted") != -1) {
                        status = "Transaction Cancelled!";
                        Toast.Companion.show(getApplicationContext(), status);
                    } else {
                        status = "Status Not Known!";
                        Toast.Companion.show(getApplicationContext(), status);
                    }

                    /*if (html.indexOf("Failure") != -1) {
                        status = "Transaction Declined!";
                    } else if (html.indexOf("Success") != -1) {
                        status = "Transaction Successful!";
                    } else if (html.indexOf("Aborted") != -1) {
                        status = "Transaction Cancelled!";
                    } else {
                        status = "Status Not Known!";
                    }
                    //Toast.makeText(getApplicationContext(), status, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), StatusActivity.class);
                    intent.putExtra("transStatus", status);
                    startActivity(intent);*/
                }
            }

            final WebView webview = (WebView) findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
            webview.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(webview, url);
                    LoadingDialog.cancelLoading();
                    if (url.indexOf("/ccavResponseHandler.php") != -1) {
                        webview.loadUrl("javascript:window.HTMLOUT.processHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
                    }
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    LoadingDialog.showLoadingDialog(WebViewActivity.this, "Loading...");
                }
            });


            try {
                String postData = AvenuesParams.ACCESS_CODE + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.ACCESS_CODE), "UTF-8")
                        + "&" + AvenuesParams.MERCHANT_ID + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.MERCHANT_ID), "UTF-8")
                        + "&" + AvenuesParams.ORDER_ID + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.ORDER_ID), "UTF-8")
                        + "&" + AvenuesParams.REDIRECT_URL + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.REDIRECT_URL), "UTF-8")
                        + "&" + AvenuesParams.CANCEL_URL + "=" + URLEncoder.encode(mainIntent.getStringExtra(AvenuesParams.CANCEL_URL), "UTF-8")
                        + "&" + AvenuesParams.ENC_VAL + "=" + URLEncoder.encode(encVal, "UTF-8");
                webview.postUrl(Constants.TRANS_URL, postData.getBytes());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    }

    public void get_RSA_key(final String ac, final String od) {
        LoadingDialog.showLoadingDialog(WebViewActivity.this, "Loading...");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, mainIntent.getStringExtra(AvenuesParams.RSA_KEY_URL),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(WebViewActivity.this,response,Toast.LENGTH_LONG).show();
                        LoadingDialog.cancelLoading();

                        if (response != null && !response.equals("")) {
                            Log.e(TAG, "response ==========> " + response);
                            vResponse = response;     ///save retrived rsa key
                            if (vResponse.contains("!ERROR!")) {
                                show_alert(vResponse);
                            } else {
                                new RenderView().execute();   // Calling async task to get display content
                            }
                        } else {
                            show_alert("No response");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.cancelLoading();
                        //Toast.makeText(WebViewActivity.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(AvenuesParams.ACCESS_CODE, ac);
                params.put(AvenuesParams.ORDER_ID, od);
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void show_alert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                WebViewActivity.this).create();

        alertDialog.setTitle("Error!!!");
        if (msg.contains("\n"))
            msg = msg.replaceAll("\\\n", "");

        alertDialog.setMessage(msg);

        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });


        alertDialog.show();
    }

    private void getCartListAPI(String transactionId) {

        if (APIUtils.Companion.isOnline(WebViewActivity.this)) {
            showProgressDialog(WebViewActivity.this);
            HashMap<String, String> dataMap = new HashMap<String, String>();
            dataMap.put(WSKey.USER_ID, Prefs.Companion.getString(PrefsKey.ID, "", WebViewActivity.this));
            dataMap.put(WSKey.TRANSACTION_ID, transactionId);
            dataMap.put(WSKey.TOTAL_AMOUNT, mainIntent.getStringExtra(AvenuesParams.AMOUNT));
            apiHelper.callWB(POST_PROCESS_TO_CHECKOUT, WebViewActivity.this, dataMap, W_PROCESS_TO_CHECKOUT, this, false, false, true, POST);
        } else {
            Toast.Companion.show(WebViewActivity.this, PleaseCheckInternetConnectionEng);
        }
    }

    @Override
    public void onRetrofitResponse(int responseCode, @NotNull Object mRes, @NotNull String responseTag) {
        super.onRetrofitResponse(responseCode, mRes, responseTag);

        Logg.Companion.e(TAG, responseTag + " Response " + mRes.toString());

        hideProgressDialog();

        if (responseTag.equalsIgnoreCase(W_PROCESS_TO_CHECKOUT)) {
            Gson gson = new Gson();
            CommonResponse commonResponse = gson.fromJson(mRes.toString(), CommonResponse.class);

            if (commonResponse.getFlag()) {
                Toast.Companion.showLongToast(WebViewActivity.this, commonResponse.getMessage());
                Intent intentHome = new Intent(WebViewActivity.this, MainActivity.class);
                intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intentHome);
                finish();
            } else {
                Toast.Companion.showLongToast(WebViewActivity.this, commonResponse.getMessage());
            }
        }

    }

    @Override
    public void onRetrofitError(int Code, @NotNull String mError, @NotNull String responseTag) {
        super.onRetrofitError(Code, mError, responseTag);

        hideProgressDialog();

        Toast.Companion.showLongToast(WebViewActivity.this, getString(R.string.meesage_connection_timeout));
    }
}