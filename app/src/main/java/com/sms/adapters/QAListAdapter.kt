package com.sms.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.activity.AnswerActivity
import com.sms.common.Utils
import com.sms.model.questionanswer.QuestionItem
import de.hdodenhof.circleimageview.CircleImageView

class QAListAdapter(val context: Activity, val cartList: ArrayList<QuestionItem>) : RecyclerView.Adapter<QAListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QAListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_qa, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: QAListAdapter.ViewHolder, position: Int) {
        holder.bindItems(cartList[position], position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return cartList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(questionItem: QuestionItem, position: Int) {

            val llAnswer = itemView.findViewById(R.id.llAnswer) as LinearLayout
            val llFirstAnswer = itemView.findViewById(R.id.llFirstAnswer) as LinearLayout

            val tvQuestionTitleQA = itemView.findViewById(R.id.tvQuestionTitleQA) as TextView
            val tvQuestionDescriptionQA = itemView.findViewById(R.id.tvQuestionDescriptionQA) as TextView
            val tvAnswerLabel = itemView.findViewById(R.id.tvAnswerLabel) as TextView
            val tvTagSubject = itemView.findViewById(R.id.tvTagSubject) as TextView
            val tvTagClass = itemView.findViewById(R.id.tvTagClass) as TextView

            val llAnsweredBy = itemView.findViewById(R.id.llAnsweredBy) as LinearLayout
            val ivAnserByOne = itemView.findViewById(R.id.ivAnserByOne) as CircleImageView
            val ivAnserByTwo = itemView.findViewById(R.id.ivAnserByTwo) as CircleImageView
            val ivAnserByThree = itemView.findViewById(R.id.ivAnserByThree) as CircleImageView
            val tvTotalAnsweredBy = itemView.findViewById(R.id.tvTotalAnsweredBy) as TextView

            tvQuestionTitleQA.text = questionItem.question
//            tvQuestionDescriptionQA.text = questionItem.description

            if ((position + 1) % 3 == 1) {
                tvAnswerLabel.setBackgroundResource(R.drawable.shape_circle_saffron)
            } else if ((position + 1) % 3 == 2) {
                tvAnswerLabel.setBackgroundResource(R.drawable.shape_circle_two)
            } else if ((position + 1) % 3 == 0) {
                tvAnswerLabel.setBackgroundResource(R.drawable.shape_circle_three)
            }

            if (questionItem.answer != null && questionItem.answer.isNotEmpty()) {
                llAnsweredBy.visibility = View.VISIBLE
                llFirstAnswer.visibility = View.VISIBLE

                tvQuestionDescriptionQA.text = questionItem.answer[0].answer

                if (questionItem.answer.size == 1) {

                    ivAnserByOne.visibility = View.VISIBLE
                    ivAnserByTwo.visibility = View.GONE
                    ivAnserByThree.visibility = View.GONE
                    tvTotalAnsweredBy.visibility = View.GONE

                    if (!questionItem.answer[0].profileImage.isNullOrBlank()) {
                        Glide.with(context)
                                .load(questionItem.answer[0].profileImage)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }
                                })
                                .into(ivAnserByOne)
                    }

                } else if (questionItem.answer.size == 2) {

                    ivAnserByOne.visibility = View.VISIBLE
                    ivAnserByTwo.visibility = View.VISIBLE
                    ivAnserByThree.visibility = View.GONE
                    tvTotalAnsweredBy.visibility = View.GONE

                    if (!questionItem.answer[0].profileImage.isNullOrBlank()) {
                        Glide.with(context)
                                .load(questionItem.answer[0].profileImage)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }
                                })
                                .into(ivAnserByOne)
                    }

                    if (!questionItem.answer[1].profileImage.isNullOrBlank()) {
                        Glide.with(context)
                                .load(questionItem.answer[1].profileImage)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }
                                })
                                .into(ivAnserByOne)
                    }

                } else if (questionItem.answer.size == 3) {

                    ivAnserByOne.visibility = View.VISIBLE
                    ivAnserByTwo.visibility = View.VISIBLE
                    ivAnserByThree.visibility = View.VISIBLE
                    tvTotalAnsweredBy.visibility = View.GONE

                    if (!questionItem.answer[0].profileImage.isNullOrBlank()) {
                        Glide.with(context)
                                .load(questionItem.answer[0].profileImage)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }
                                })
                                .into(ivAnserByOne)
                    }

                    if (!questionItem.answer[1].profileImage.isNullOrBlank()) {
                        Glide.with(context)
                                .load(questionItem.answer[1].profileImage)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }
                                })
                                .into(ivAnserByOne)
                    }

                    if (!questionItem.answer[2].profileImage.isNullOrBlank()) {
                        Glide.with(context)
                                .load(questionItem.answer[2].profileImage)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }

                                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                        return false
                                    }
                                })
                                .into(ivAnserByOne)
                    }

                } else if (questionItem.answer.size > 3) {

                    ivAnserByOne.visibility = View.VISIBLE
                    ivAnserByTwo.visibility = View.VISIBLE
                    ivAnserByThree.visibility = View.VISIBLE
                    tvTotalAnsweredBy.visibility = View.VISIBLE

                    tvTotalAnsweredBy.text = "+" + (questionItem.answer.size - 1)
                }

            } else {
                llAnsweredBy.visibility = View.INVISIBLE
                llFirstAnswer.visibility = View.GONE
            }

            tvTagSubject.text = questionItem.name
            tvTagClass.text = Utils.getNumberSuffix(questionItem.classId) + " " + "Standard"

            llAnswer.setOnClickListener {

                var intnetAnswerList = Intent(context, AnswerActivity::class.java)
                intnetAnswerList.putExtra("questionItem", questionItem)
                intnetAnswerList.putExtra("position", position)
                context.startActivity(intnetAnswerList)

            }
        }
    }
}