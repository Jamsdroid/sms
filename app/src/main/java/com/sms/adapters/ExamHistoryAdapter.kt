package com.sms.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.sms.R
import com.sms.common.Utils
import com.sms.model.parentexamhistory.ExamListItem

class ExamHistoryAdapter(val context: Activity, val cartList: ArrayList<ExamListItem>) : RecyclerView.Adapter<ExamHistoryAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamHistoryAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_exam_history, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ExamHistoryAdapter.ViewHolder, position: Int) {
        holder.bindItems(cartList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return cartList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examListItem: ExamListItem) {

//            val llAnswer = itemView.findViewById(R.id.llAnswer) as LinearLayout

            val tvExamTitle = itemView.findViewById(R.id.tvExamTitle) as TextView
            val tvTotalAnswer = itemView.findViewById(R.id.tvTotalAnswer) as TextView
            val tvCorrectAnswer = itemView.findViewById(R.id.tvCorrectAnswer) as TextView
            val tvWrongAnswer = itemView.findViewById(R.id.tvWrongAnswer) as TextView
            val tvExamCreated = itemView.findViewById(R.id.tvExamCreated) as TextView

            tvExamTitle.text = examListItem.examName

            tvExamCreated.text = Utils.formatExamHistoryDate(examListItem.createdDate)

            if (examListItem.examResult != null) {
                tvTotalAnswer.text = examListItem.examResult.total.toString()
                tvCorrectAnswer.text = examListItem.examResult.correctAnswer.toString()
                tvWrongAnswer.text = examListItem.examResult.wrongAnswer.toString()
            }

        }
    }
}
