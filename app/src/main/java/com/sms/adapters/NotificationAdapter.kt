/*
package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.common.Logg
import com.sms.model.home.UserDetailsItem

class CoursesAdapter(val context: Context, val userList: List<UserDetailsItem>) : RecyclerView.Adapter<CoursesAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_my_courses, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CoursesAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(courseDetail: UserDetailsItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView
            tvCourseName.text = courseDetail.name

            tvOwnerName.text = courseDetail.courseCreatedBy
            tvDownloadCount.text = "0" // courseDetail.

            if (courseDetail.isFree == "0") {
                // paid course
                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + courseDetail.price
            } else if (courseDetail.isFree == "1") {
                // free course
                tvCoursePrice.text = "free"
            }

            Logg.e(TAG, "product Name  = " + courseDetail.name)

            val imageURL = courseDetail.image

            Glide.with(context).load(imageURL).into(ivProductImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(courseDetail.productId!!)
//                }
//
//
//            }

        }
    }
}*/

package com.sms.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.boozetastic.viewholder.LinearHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.R.id.edtAnswerNotification
import com.sms.activity.MainActivity
import com.sms.activity.NotificationListActivity
import com.sms.activity.ParentHomeActivity
import com.sms.common.Constants
import com.sms.common.Utils
import com.sms.model.notification.NotificationItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import kotlinx.android.synthetic.main.activity_notification_list.*

class NotificationAdapter(val context: Activity, val userList: ArrayList<NotificationItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_grid_product, parent, false)
//        return ViewHolder(v)

        if (viewType == Constants.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_item_notification, parent, false)
            return ViewHolder(view)
        } else if (viewType == Constants.VIEW_TYPE_LOADING) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)
            return LoadingHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_item_notification, parent, false)
            return LinearHolder(view)
        }
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        holder.bindItems(userList.get(position))
        if (holder is ViewHolder) {
            holder.bindItems(userList.get(position)!!, position)
        } else if (holder is LoadingHolder) {
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
//        return userList.size
        return if (userList == null) 0 else userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(notificationItem: NotificationItem, position: Int) {

            val llAnswer = itemView.findViewById(R.id.llAnswer) as LinearLayout
            val llNotificationReplyArea = itemView.findViewById(R.id.llNotificationReplyArea) as LinearLayout
            val ivProfileImage = itemView.findViewById(R.id.ivProfileImage) as ImageView
            val tvSenderName = itemView.findViewById(R.id.tvSenderName) as TextView
            val tvDateOfNotification = itemView.findViewById(R.id.tvDateOfNotification) as TextView
            val tvNotification = itemView.findViewById(R.id.tvNotification) as TextView
            val tvNotificationReply = itemView.findViewById(R.id.tvNotificationReply) as TextView
            val tvNotificationReplyDate = itemView.findViewById(R.id.tvNotificationReplyDate) as TextView


            if (Prefs.getString(PrefsKey.USER_TYPE, "", context) == "0") {
                // student can not reply to notifications
                llAnswer.visibility = View.GONE
            } else {
                // parent can reply to notifications
                llAnswer.visibility = View.VISIBLE
            }

            tvSenderName.text = notificationItem.teacherName

            tvDateOfNotification.text = Utils.formateNotificationDate(notificationItem.createdDate)

            if (!notificationItem.message.isNullOrBlank()) {
                tvNotification.text = Html.fromHtml(notificationItem.message)
            }

            if (!notificationItem.replyMessage.isNullOrBlank()) {
                tvNotificationReply.text = notificationItem.replyMessage
                llNotificationReplyArea.visibility = View.VISIBLE
            } else {
                llNotificationReplyArea.visibility = View.GONE
            }

            if (!notificationItem.replyDate.isNullOrBlank()) {
                tvNotificationReplyDate.text = notificationItem.replyDate
            }

            llAnswer.setOnClickListener {

                //                var intnetAnswerList = Intent(context, AnswerActivity::class.java)
//                context.startActivity(intnetAnswerList)

                NotificationListActivity.notificationClickedItem = position

                context.rlBottomNotification.visibility = View.VISIBLE

                context.edtAnswerNotification.requestFocus()

                Utils.showKeyboard(context, context.edtAnswerNotification)

            }

            if (!notificationItem.profileImage.isNullOrBlank()) {
                Glide.with(context)
                        .load(notificationItem.profileImage)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }
                        })
                        .into(ivProfileImage)
            }
        }
    }

    inner class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemViewType(position: Int): Int {
        return if (userList.get(position).isEmpty) Constants.VIEW_TYPE_LOADING else Constants.VIEW_TYPE_ITEM
    }

    fun addData(dataViews: ArrayList<NotificationItem>) {
        this.userList.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun addLoadingView() {
        //add loading item
        Handler().post {
            userList.add(NotificationItem())
            notifyItemInserted(userList.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        userList.removeAt(userList.size - 1)
        notifyItemRemoved(userList.size)
    }
}
