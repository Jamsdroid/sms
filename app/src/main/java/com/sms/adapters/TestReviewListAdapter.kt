/*
package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.common.Logg
import com.sms.model.home.UserDetailsItem

class CoursesAdapter(val context: Context, val userList: List<UserDetailsItem>) : RecyclerView.Adapter<CoursesAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_my_courses, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CoursesAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examListItem: UserDetailsItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView
            tvCourseName.text = examListItem.name

            tvOwnerName.text = examListItem.courseCreatedBy
            tvDownloadCount.text = "0" // examListItem.

            if (examListItem.isFree == "0") {
                // paid course
                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price
            } else if (examListItem.isFree == "1") {
                // free course
                tvCoursePrice.text = "free"
            }

            Logg.e(TAG, "product Name  = " + examListItem.name)

            val imageURL = examListItem.image

            Glide.with(context).load(imageURL).into(ivProductImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(examListItem.productId!!)
//                }
//
//
//            }

        }
    }
}*/

package com.sms.adapters

import android.app.Activity
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.model.examstart.ExamQuestionItem

class TestReviewListAdapter(val context: Activity, val examList: ArrayList<ExamQuestionItem>) : RecyclerView.Adapter<TestReviewListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestReviewListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_test_review, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: TestReviewListAdapter.ViewHolder, position: Int) {
        holder.bindItems(examList.get(position), position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return examList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examQuestionItem: ExamQuestionItem, position: Int) {

            val rlQuestionWithImagesResult = itemView.findViewById(R.id.rlQuestionWithImagesResult) as RelativeLayout
            val llQuestionWith = itemView.findViewById(R.id.llQuestionWith) as LinearLayout

            val ivQuestionAnswerImageResult = itemView.findViewById(R.id.ivQuestionAnswerImageResult) as ImageView
            val rgQuestionsResult = itemView.findViewById(R.id.rgQuestionsResult) as RadioGroup

            val rbOptionAResult = itemView.findViewById(R.id.rbOptionAResult) as RadioButton
            val rbOptionBResult = itemView.findViewById(R.id.rbOptionBResult) as RadioButton
            val rbOptionCResult = itemView.findViewById(R.id.rbOptionCResult) as RadioButton
            val rbOptionDResult = itemView.findViewById(R.id.rbOptionDResult) as RadioButton

            val tvQuestionNameExam = itemView.findViewById(R.id.tvQuestionNameExam) as TextView
            val tvQuestionNotAnswred = itemView.findViewById(R.id.tvQuestionNotAnswred) as TextView

            val tvOptionACorrectAnswers = itemView.findViewById(R.id.tvOptionACorrectAnswers) as TextView
            val tvOptionBCorrectAnswers = itemView.findViewById(R.id.tvOptionBCorrectAnswers) as TextView
            val tvOptionCCorrectAnswers = itemView.findViewById(R.id.tvOptionCCorrectAnswers) as TextView
            val tvOptionDCorrectAnswers = itemView.findViewById(R.id.tvOptionDCorrectAnswers) as TextView

            val tvOptionA = itemView.findViewById(R.id.tvOptionA) as TextView
            val tvOptionB = itemView.findViewById(R.id.tvOptionB) as TextView
            val tvOptionC = itemView.findViewById(R.id.tvOptionC) as TextView
            val tvOptionD = itemView.findViewById(R.id.tvOptionD) as TextView

            val rlOptionA = itemView.findViewById(R.id.rlOptionA) as RelativeLayout
            val rlOptionB = itemView.findViewById(R.id.rlOptionB) as RelativeLayout
            val rlOptionC = itemView.findViewById(R.id.rlOptionC) as RelativeLayout
            val rlOptionD = itemView.findViewById(R.id.rlOptionD) as RelativeLayout

            val ivOptionA = itemView.findViewById(R.id.ivOptionA) as ImageView
            val ivOptionB = itemView.findViewById(R.id.ivOptionB) as ImageView
            val ivOptionC = itemView.findViewById(R.id.ivOptionC) as ImageView
            val ivOptionD = itemView.findViewById(R.id.ivOptionD) as ImageView

            if (examQuestionItem.questionFile.isNullOrBlank()) {
                rlQuestionWithImagesResult.visibility = View.GONE
                llQuestionWith.visibility = View.VISIBLE
            } else {
                rlQuestionWithImagesResult.visibility = View.VISIBLE
                llQuestionWith.visibility = View.GONE

                Glide.with(context)
                        .load(examQuestionItem.questionFile)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }
                        })
                        .into(ivQuestionAnswerImageResult)

                when (examQuestionItem.answer) {

                    "1" -> {
                        rbOptionAResult.isChecked = true
                    }

                    "2" -> {
                        rbOptionBResult.isChecked = true
                    }

                    "3" -> {
                        rbOptionCResult.isChecked = true
                    }

                    "4" -> {
                        rbOptionDResult.isChecked = true

                    }
                    else -> {
                        rgQuestionsResult.clearCheck()
                    }
                }
            }


            tvQuestionNameExam.text = (position + 1).toString() + ". " + examQuestionItem?.question

            tvOptionA.text = examQuestionItem?.option1
            tvOptionB.text = examQuestionItem?.option2
            tvOptionC.text = examQuestionItem?.option3
            tvOptionD.text = examQuestionItem?.option4


            if (examQuestionItem.selectedAnswer.isNullOrBlank()) {
                tvQuestionNameExam.setTextColor(ContextCompat.getColor(context, R.color.colorAppGrey))
                tvQuestionNotAnswred.visibility = View.VISIBLE
            } else {
                tvQuestionNameExam.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
                tvQuestionNotAnswred.visibility = View.GONE
            }

            when (examQuestionItem.answer) {

                "1" -> {
                    tvOptionACorrectAnswers.visibility = View.VISIBLE
                }

                "2" -> {
                    tvOptionBCorrectAnswers.visibility = View.VISIBLE
                }

                "3" -> {
                    tvOptionCCorrectAnswers.visibility = View.VISIBLE
                }

                "4" -> {
                    tvOptionDCorrectAnswers.visibility = View.VISIBLE
                }

            }

            when (examQuestionItem.selectedAnswer) {

                "1" -> {
                    ivOptionA.visibility = View.VISIBLE
                    if (examQuestionItem.selectedAnswer == examQuestionItem.answer) {
                        rlOptionA.setBackgroundResource(R.drawable.rectange_bg_green)
                        ivOptionA.setImageResource(R.drawable.correct)
                        tvOptionACorrectAnswers.visibility = View.GONE
                    } else {
                        rlOptionA.setBackgroundResource(R.drawable.rectange_bg_red)
                        ivOptionA.setImageResource(R.drawable.incorrect)
                    }

                    rlOptionB.setBackgroundResource(R.color.transparent)
                    rlOptionC.setBackgroundResource(R.color.transparent)
                    rlOptionD.setBackgroundResource(R.color.transparent)
                }
                "2" -> {
                    ivOptionB.visibility = View.VISIBLE
                    if (examQuestionItem.selectedAnswer == examQuestionItem.answer) {
                        rlOptionB.setBackgroundResource(R.drawable.rectange_bg_green)
                        ivOptionB.setImageResource(R.drawable.correct)
                        tvOptionBCorrectAnswers.visibility = View.GONE
                    } else {
                        rlOptionB.setBackgroundResource(R.drawable.rectange_bg_red)
                        ivOptionB.setImageResource(R.drawable.incorrect)
                    }

                    rlOptionA.setBackgroundResource(R.color.transparent)
                    rlOptionC.setBackgroundResource(R.color.transparent)
                    rlOptionD.setBackgroundResource(R.color.transparent)
                }
                "3" -> {
                    ivOptionC.visibility = View.VISIBLE
                    if (examQuestionItem.selectedAnswer == examQuestionItem.answer) {
                        rlOptionC.setBackgroundResource(R.drawable.rectange_bg_green)
                        ivOptionC.setImageResource(R.drawable.correct)
                        tvOptionCCorrectAnswers.visibility = View.GONE
                    } else {
                        rlOptionC.setBackgroundResource(R.drawable.rectange_bg_red)
                        ivOptionC.setImageResource(R.drawable.incorrect)
                    }

                    rlOptionA.setBackgroundResource(R.color.transparent)
                    rlOptionB.setBackgroundResource(R.color.transparent)
                    rlOptionD.setBackgroundResource(R.color.transparent)
                }
                "4" -> {
                    ivOptionD.visibility = View.VISIBLE
                    if (examQuestionItem.selectedAnswer == examQuestionItem.answer) {
                        rlOptionD.setBackgroundResource(R.drawable.rectange_bg_green)
                        ivOptionD.setImageResource(R.drawable.correct)
                        tvOptionDCorrectAnswers.visibility = View.GONE
                    } else {
                        rlOptionD.setBackgroundResource(R.drawable.rectange_bg_red)
                        ivOptionD.setImageResource(R.drawable.incorrect)
                    }

                    rlOptionA.setBackgroundResource(R.color.transparent)
                    rlOptionB.setBackgroundResource(R.color.transparent)
                    rlOptionC.setBackgroundResource(R.color.transparent)
                }
            }

        }
    }
}
