/*
package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.common.Logg
import com.sms.model.home.UserDetailsItem

class CoursesAdapter(val context: Context, val userList: List<UserDetailsItem>) : RecyclerView.Adapter<CoursesAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_my_courses, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CoursesAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examListItem: UserDetailsItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView
            tvCourseName.text = examListItem.name

            tvOwnerName.text = examListItem.courseCreatedBy
            tvDownloadCount.text = "0" // examListItem.

            if (examListItem.isFree == "0") {
                // paid course
                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price
            } else if (examListItem.isFree == "1") {
                // free course
                tvCoursePrice.text = "free"
            }

            Logg.e(TAG, "product Name  = " + examListItem.name)

            val imageURL = examListItem.image

            Glide.with(context).load(imageURL).into(ivProductImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(examListItem.productId!!)
//                }
//
//
//            }

        }
    }
}*/

package com.sms.adapters

import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.sms.R
import com.sms.activity.ExamChaptersListActivity
import com.sms.activity.ExamStartActivity
import com.sms.model.exam.ExamChaptersItem
import com.sms.model.exam.ExamListItem

class ExamChapterListAdapter(val context: ExamChaptersListActivity, val examList: ArrayList<ExamChaptersItem>, var examListItem: ExamListItem) : RecyclerView.Adapter<ExamChapterListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamChapterListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_exam_chapter, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ExamChapterListAdapter.ViewHolder, position: Int) {
        holder.bindItems(examList.get(position), position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return examList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examChapterItem: ExamChaptersItem, position: Int) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val llChapterNumber = itemView.findViewById(R.id.llChapterNumber) as LinearLayout
            val cvCourse = itemView.findViewById(R.id.cvCourse) as CardView
            val tvChapterName = itemView.findViewById(R.id.tvChapterName) as TextView
            val tvChapterNumber = itemView.findViewById(R.id.tvChapterNumber) as TextView
            val tvQuestionCount = itemView.findViewById(R.id.tvQuestionCount) as TextView

            if (position % 2 == 0) {
                llChapterNumber.setBackgroundColor(ContextCompat.getColor(context, R.color.colorChapterOdd))
            } else {
                llChapterNumber.setBackgroundColor(ContextCompat.getColor(context, R.color.colorChapterEven))
            }

            tvChapterNumber.text = getNumberInString((position + 1))
            tvQuestionCount.text = examChapterItem.totalQuestion.toString()
            tvChapterName.text = examChapterItem.examName
//            if (examListItem.isFree == "0") {
//                // paid course
//                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price
//            } else if (examListItem.isFree == "1") {
//                // free course
//                tvCoursePrice.text = "Free"
//            }

//            var magin5Sdp = context.resources.getDimension(R.dimen._4sdp).toInt()
//            if (position % 2 == 0) {
//                ((cvCourse.layoutParams) as LinearLayout.LayoutParams).setMargins(0, 0, magin5Sdp, magin5Sdp)
//            } else {
//                ((cvCourse.layoutParams) as LinearLayout.LayoutParams).setMargins(magin5Sdp, 0, 0, magin5Sdp)
//            }

            root.setOnClickListener {
                var intentExamStart = Intent(context, ExamStartActivity::class.java)
                intentExamStart.putExtra("examChapterItem", examChapterItem)
                intentExamStart.putExtra("examListItem", examListItem)
                intentExamStart.putExtra("chapterNumber", getNumberInString((position + 1)))
                context.startActivity(intentExamStart)
            }

        }
    }

    private fun getNumberInString(position: Int): String {

        var num: String

        if (position <= 9) {
            num = "0$position"
        } else {
            num = position.toString()
        }
        return num
    }
}
