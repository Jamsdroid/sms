/*
package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.common.Logg
import com.sms.model.home.UserDetailsItem

class CoursesAdapter(val context: Context, val userList: List<UserDetailsItem>) : RecyclerView.Adapter<CoursesAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_my_courses, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CoursesAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examListItem: UserDetailsItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView
            tvCourseName.text = examListItem.name

            tvOwnerName.text = examListItem.courseCreatedBy
            tvDownloadCount.text = "0" // examListItem.

            if (examListItem.isFree == "0") {
                // paid course
                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price
            } else if (examListItem.isFree == "1") {
                // free course
                tvCoursePrice.text = "free"
            }

            Logg.e(TAG, "product Name  = " + examListItem.name)

            val imageURL = examListItem.image

            Glide.with(context).load(imageURL).into(ivProductImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(examListItem.productId!!)
//                }
//
//
//            }

        }
    }
}*/

package com.sms.adapters

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sms.R
import com.sms.activity.ExamStartActivity

class ExamQuestionCountAdapter(val context: ExamStartActivity, val examList: ArrayList<String>) : RecyclerView.Adapter<ExamQuestionCountAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamQuestionCountAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_exam_question_count, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ExamQuestionCountAdapter.ViewHolder, position: Int) {
        holder.bindItems(examList.get(position), position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return examList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(data: String, position: Int) {

            val tvQuestionNameExam = itemView.findViewById(R.id.tvQuestionNameExam) as TextView

            tvQuestionNameExam.text = data

            if (position == context.index) {
                tvQuestionNameExam.setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
                tvQuestionNameExam.setBackgroundResource(R.drawable.shape_circle_exam_question)
            } else {
                tvQuestionNameExam.setTextColor(ContextCompat.getColor(context, R.color.colorGrey))
                tvQuestionNameExam.setBackgroundResource(0)
            }

            tvQuestionNameExam.setOnClickListener {
                context.index = position
                context.setQuestionsToViews(context.index)
                notifyDataSetChanged()
            }

        }
    }
}
