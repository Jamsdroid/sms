/*
package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.common.Logg
import com.sms.model.home.UserDetailsItem

class CoursesAdapter(val context: Context, val userList: List<UserDetailsItem>) : RecyclerView.Adapter<CoursesAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_my_courses, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CoursesAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(courseDetail: UserDetailsItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView
            tvCourseName.text = courseDetail.name

            tvOwnerName.text = courseDetail.courseCreatedBy
            tvDownloadCount.text = "0" // courseDetail.

            if (courseDetail.isFree == "0") {
                // paid course
                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + courseDetail.price
            } else if (courseDetail.isFree == "1") {
                // free course
                tvCoursePrice.text = "free"
            }

            Logg.e(TAG, "product Name  = " + courseDetail.name)

            val imageURL = courseDetail.image

            Glide.with(context).load(imageURL).into(ivProductImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(courseDetail.productId!!)
//                }
//
//
//            }

        }
    }
}*/

package com.sms.adapters

import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.boozetastic.viewholder.LinearHolder
import com.bumptech.glide.Glide
import com.sms.activity.CourseDetailsActivity
import com.sms.R
import com.sms.R.id.*
import com.sms.activity.AnswerOfQuestionActivity
import com.sms.common.AppConstants
import com.sms.common.Constants
import com.sms.common.Logg
import com.sms.model.home.CourseDetailsItem
import com.sms.model.homework.HomeWorkDetailsItem
import com.sms.model.homework.HomeWorkListItem

class HomeWorkAdapter(val context: Activity, val userList: ArrayList<HomeWorkDetailsItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_grid_product, parent, false)
//        return ViewHolder(v)

        if (viewType == Constants.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_item_home_work_list, parent, false)
            return ViewHolder(view)
        } else if (viewType == Constants.VIEW_TYPE_LOADING) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.progress_loading, parent, false)
            return LoadingHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_item_home_work_list, parent, false)
            return LinearHolder(view)
        }
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        holder.bindItems(userList.get(position))
        if (holder is ViewHolder) {
            holder.bindItems(userList.get(position)!!, position)
        } else if (holder is LoadingHolder) {
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
//        return userList.size
        return if (userList == null) 0 else userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(homeWorkDetailsItem: HomeWorkDetailsItem, position: Int) {
            val root = itemView.findViewById(R.id.root) as LinearLayout

            val tvHomeWorkQuestion = itemView.findViewById(R.id.tvHomeWorkQuestion) as TextView
            val tvHomeWorkQuestionStatus = itemView.findViewById(R.id.tvHomeWorkQuestionStatus) as TextView
            val tvAnswerNow = itemView.findViewById(R.id.tvAnswerNow) as TextView

            tvHomeWorkQuestion.text = homeWorkDetailsItem.question

            if (homeWorkDetailsItem.status == "0") {
                tvAnswerNow.visibility = View.VISIBLE
                tvHomeWorkQuestionStatus.text = context.getString(R.string.question_status_pending)
                tvHomeWorkQuestionStatus.setTextColor(ContextCompat.getColor(context, R.color.colorAppGrey))
            } else {
                tvAnswerNow.visibility = View.GONE
                tvHomeWorkQuestionStatus.text = context.getString(R.string.question_status_completed)
                tvHomeWorkQuestionStatus.setTextColor(ContextCompat.getColor(context, R.color.colorAppGreen))
            }

            tvAnswerNow.setOnClickListener {
                var intentAnswerOfQuestion = Intent(context, AnswerOfQuestionActivity::class.java)
                intentAnswerOfQuestion.putExtra("homeWorkDetailsItem", homeWorkDetailsItem)
                context.startActivityForResult(intentAnswerOfQuestion, AppConstants.REQUEST_CODE_ANSWER)
            }
        }
    }

    inner class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemViewType(position: Int): Int {
        return if (userList.get(position).isEmpty) Constants.VIEW_TYPE_LOADING else Constants.VIEW_TYPE_ITEM
    }

    fun addData(dataViews: ArrayList<HomeWorkDetailsItem>) {
        this.userList.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun addLoadingView() {
        //add loading item
        Handler().post {
            userList.add(HomeWorkDetailsItem())
            notifyItemInserted(userList.size - 1)
        }
    }

    fun removeLoadingView() {
        //Remove loading item
        userList.removeAt(userList.size - 1)
        notifyItemRemoved(userList.size)
    }
}
