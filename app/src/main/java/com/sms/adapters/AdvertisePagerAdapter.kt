package com.sms.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.model.home.AdsListItem


class AdvertisePagerAdapter(internal var context: Context, internal var arrayList: List<AdsListItem>?) : PagerAdapter() {
    internal var layoutInflater: LayoutInflater

    init {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getCount(): Int {
        return if (arrayList != null) {
            arrayList!!.size
        } else 0
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = layoutInflater.inflate(R.layout.row_item_home_advertise, container, false)

        var productDetail: AdsListItem = arrayList!![position]

        val root = itemView.findViewById(R.id.root) as LinearLayout
        val tvAdsTitle = itemView.findViewById(R.id.tvAdsTitle) as TextView
        val tvAdsDescription = itemView.findViewById(R.id.tvAdsDescription) as TextView
        val ivAdsImage = itemView.findViewById(R.id.ivAdsImage) as ImageView
        tvAdsTitle.text = productDetail.adsText
        tvAdsDescription.text = productDetail.adsDesc

        val imageURL = productDetail.adsImage

        if (!imageURL.isNullOrBlank()) {
            Glide.with(context).load(imageURL).into(ivAdsImage)
        }


//        val imageView = itemView.findViewById(R.id.viewPagerItem_image1) as ImageView
//
////        Picasso.with(context).load(arrayList!![position])
////                .placeholder(R.drawable.image_uploading)
////                .error(R.drawable.image_not_found).into(imageView)
//        val imageURL = arrayList!!.get(position).image
//
//        Glide.with(AlcoholApplication.context).load(imageURL).diskCacheStrategy(DiskCacheStrategy.SOURCE).dontAnimate().into(imageView)

        container.addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }
}