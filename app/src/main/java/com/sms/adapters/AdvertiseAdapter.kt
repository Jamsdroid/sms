package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.model.home.AdsListItem

class AdvertiseAdapter(val context: Context, val userList: List<AdsListItem>) : RecyclerView.Adapter<AdvertiseAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdvertiseAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_home_advertise, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: AdvertiseAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(productDetail: AdsListItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvAdsTitle = itemView.findViewById(R.id.tvAdsTitle) as TextView
            val tvAdsDescription = itemView.findViewById(R.id.tvAdsDescription) as TextView
            val ivAdsImage = itemView.findViewById(R.id.ivAdsImage) as ImageView
            tvAdsTitle.text = productDetail.adsText
            tvAdsDescription.text = productDetail.adsDesc

            val imageURL = productDetail.adsImage

            Glide.with(context).load(imageURL).into(ivAdsImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(productDetail.productId!!)
//                }
//
//
//            }

        }
    }
}