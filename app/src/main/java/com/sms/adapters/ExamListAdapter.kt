/*
package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.common.Logg
import com.sms.model.home.UserDetailsItem

class CoursesAdapter(val context: Context, val userList: List<UserDetailsItem>) : RecyclerView.Adapter<CoursesAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_my_courses, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CoursesAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examListItem: UserDetailsItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView
            tvCourseName.text = examListItem.name

            tvOwnerName.text = examListItem.courseCreatedBy
            tvDownloadCount.text = "0" // examListItem.

            if (examListItem.isFree == "0") {
                // paid course
                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price
            } else if (examListItem.isFree == "1") {
                // free course
                tvCoursePrice.text = "free"
            }

            Logg.e(TAG, "product Name  = " + examListItem.name)

            val imageURL = examListItem.image

            Glide.with(context).load(imageURL).into(ivProductImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(examListItem.productId!!)
//                }
//
//
//            }

        }
    }
}*/

package com.sms.adapters

import android.content.Intent
import android.graphics.drawable.Drawable
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.activity.ExamChaptersListActivity
import com.sms.activity.ExamDetailsActivity
import com.sms.activity.MainActivity
import com.sms.common.Logg
import com.sms.fragment.FragmentExam
import com.sms.model.exam.ExamListItem

class ExamListAdapter(val context: MainActivity, val examList: ArrayList<ExamListItem>, val examFragment:FragmentExam) : RecyclerView.Adapter<ExamListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_exam, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ExamListAdapter.ViewHolder, position: Int) {
        holder.bindItems(examList.get(position), position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return examList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examListItem: ExamListItem, position: Int) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val cvCourse = itemView.findViewById(R.id.cvCourse) as CardView
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvDownloadLabel = itemView.findViewById(R.id.tvDownloadLabel) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView

            tvCourseName.text = examListItem.name
            tvOwnerName.text = examListItem.examCreatedBy
            tvDownloadCount.text = examListItem.totalChapter
            tvDownloadLabel.text = context.resources.getString(R.string.chapters)

            if (examListItem.isFree == "0") {
                // paid course
//                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price

                if (examListItem.isPurchased == "1") {
                    tvCoursePrice.text = "Free"
                } else {
                    tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price
                }

            } else if (examListItem.isFree == "1") {
                // free course
                tvCoursePrice.text = "Free"
            }

            var magin5Sdp = context.resources.getDimension(R.dimen._4sdp).toInt()
            if (position % 2 == 0) {
                ((cvCourse.layoutParams) as LinearLayout.LayoutParams).setMargins(0, 0, magin5Sdp, magin5Sdp)
            } else {
                ((cvCourse.layoutParams) as LinearLayout.LayoutParams).setMargins(magin5Sdp, 0, 0, magin5Sdp)
            }

            Logg.e(TAG, "product Name  = " + examListItem.name)

            val imageURL = examListItem.image

//            Glide.with(context).load(imageURL).into(ivProductImage)


            if (!imageURL.isNullOrBlank()) {
                Glide.with(context)
                        .load(examListItem.image)
                        .listener(object : RequestListener<Drawable> {

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                ivProductImage.setImageResource(R.mipmap.ic_launcher)
                                return false
                            }

                        })
                        .into(ivProductImage)
            }

//            GlideApp.with(context)
//                    .load(examListItem.image)
//                    .placeholder(R.drawable.default_image)
//                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .dontAnimate()
//                    .listener(object : RequestListener<String, GlideDrawable> {
//                        override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
//                            Log.e(TAG, "<=== onResourceReady ===>")
//                            return false
//                        }
//
//                        override fun onException(e: java.lang.Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
//                            Log.e(TAG, "<=== onException ===>")
//                            return false
//                        }
//                    })
//                    .into(ivCourseImageCart)

            root.setOnClickListener {

                var intentExamChaptersList = Intent(context, ExamDetailsActivity::class.java)
                intentExamChaptersList.putExtra("examListItem", examListItem)
                context.startActivity(intentExamChaptersList)

//                if (examListItem.isFree == "0") {
//                    // paid course
//
//                    examFragment.addToCartExamAPI(examId = examListItem.id, examPrice = examListItem.price)
//
//                } else if (examListItem.isFree == "1") {
//                    // free course
//                    var intentExamChaptersList = Intent(context, ExamChaptersListActivity::class.java)
//                    intentExamChaptersList.putExtra("examListItem", examListItem)
//                    context.startActivity(intentExamChaptersList)
//                }


            }

        }
    }
}
