package com.sms.adapters

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.provider.SyncStateContract
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.activity.CartListActivity
import com.sms.common.Constants
import com.sms.common.Toast
import com.sms.model.cart.CartListItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import java.util.HashMap

class CartListAdapter(val context: CartListActivity, val cartList: List<CartListItem>) : RecyclerView.Adapter<CartListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_cart, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CartListAdapter.ViewHolder, position: Int) {
        holder.bindItems(cartList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return cartList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(cartListItem: CartListItem) {

            val rootOrder = itemView.findViewById(R.id.rootCart) as RelativeLayout

            val ivCourseImageCart = itemView.findViewById(R.id.ivCourseImageCart) as ImageView

            val tvCounrseNameCart = itemView.findViewById(R.id.tvCounrseNameCart) as TextView
            val tvCoursePriceCart = itemView.findViewById(R.id.tvCoursePriceCart) as TextView
            val ivDeleteCartItem = itemView.findViewById(R.id.ivDeleteCartItem) as ImageView

            tvCounrseNameCart.text = cartListItem.name
            tvCoursePriceCart.text = context.getString(R.string.symbol_rupee) + "" + cartListItem.coursePrice

            if (!cartListItem.image.isNullOrBlank()) {
                Glide.with(context)
                        .load(cartListItem.image)
                        .listener(object : RequestListener<Drawable> {

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                ivCourseImageCart.setImageResource(R.mipmap.ic_launcher)
                                return false
                            }

                        })
                        .into(ivCourseImageCart)
            }

//            GlideApp.with(context)
//                    .load(cartListItem.image)
//                    .placeholder(R.drawable.default_image)
//                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .dontAnimate()
//                    .listener(object : RequestListener<String, GlideDrawable> {
//                        override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
//                            Log.e(TAG, "<=== onResourceReady ===>")
//                            return false
//                        }
//
//                        override fun onException(e: java.lang.Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
//                            Log.e(TAG, "<=== onException ===>")
//                            return false
//                        }
//                    })
//                    .into(ivCourseImageCart)

            ivDeleteCartItem.setOnClickListener {

                val dialog = Dialog(context)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.setContentView(R.layout.dialog_custom)
                dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                val tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
                val tvOne = dialog.findViewById(R.id.tvOne) as TextView
                val txtClose = dialog.findViewById(R.id.tvClose) as TextView
                val txtDone = dialog.findViewById(R.id.tvDone) as TextView

                tvTitle.text = context.getString(R.string.delete_cart_item)
                tvOne.text = context.getString(R.string.delete_cart_item_text)

                txtClose.setOnClickListener { dialog.dismiss() }

                txtDone.setOnClickListener {
                    dialog.dismiss()

                    if (APIUtils.isOnline(context)) {
                        context.showProgressDialog(context)
//                        deletePosition = position
                        val dataMap = HashMap<String, String>()
                        dataMap[WSKey.USER_ID] = Prefs.getString(PrefsKey.ID, "", context)
                        dataMap[WSKey.CART_ID] = cartListItem.id
                        context.apiHelper!!.callWB(Constants.POST_REMOVE_CART, context, dataMap, Constants.W_REMOVE_CART, context, false, false, true, Constants.POST)
                    } else {
                        Toast.show(context, Constants.PleaseCheckInternetConnectionEng)
                    }
                }
                dialog.show()

            }
        }
    }
}