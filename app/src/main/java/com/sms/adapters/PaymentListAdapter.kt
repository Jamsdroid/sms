package com.sms.adapters

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.provider.SyncStateContract
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.R.id.ivCourseImageCart
import com.sms.R.id.tvCounrseNameCart
import com.sms.activity.CartListActivity
import com.sms.activity.PaymentHistoryActivity
import com.sms.common.Constants
import com.sms.common.Toast
import com.sms.common.Utils
import com.sms.model.cart.CartListItem
import com.sms.model.paymenthistory.PaymentListItem
import com.sms.storage.Prefs
import com.sms.storage.PrefsKey
import com.sms.webservice.APIUtils
import com.sms.webservice.WSKey
import java.util.HashMap

class PaymentListAdapter(val context: PaymentHistoryActivity, val paymentList: ArrayList<PaymentListItem>) : RecyclerView.Adapter<PaymentListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_payment_history, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: PaymentListAdapter.ViewHolder, position: Int) {
        holder.bindItems(paymentList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return paymentList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(paymentListItem: PaymentListItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout


            val tvCourseExamName = itemView.findViewById(R.id.tvCourseExamName) as TextView
            val tvPaymentMoney = itemView.findViewById(R.id.tvPaymentMoney) as TextView
            val tvPurchaseDate = itemView.findViewById(R.id.tvPurchaseDate) as TextView
            val tvType = itemView.findViewById(R.id.tvType) as TextView
            val ivCourseImage = itemView.findViewById(R.id.ivCourseImage) as ImageView

            tvCourseExamName.text = paymentListItem.name
            tvPaymentMoney.text = context.getString(R.string.symbol_rupee) + "" + paymentListItem.price

            tvPurchaseDate.text = Utils.formatePaymentDate(paymentListItem.date)
            if (paymentListItem.cartType == "1") {
                tvType.text = context.getString(R.string.course)
            } else {
                tvType.text = context.getString(R.string.title_exam)
            }

            if (!paymentListItem.image.isNullOrBlank()) {
                Glide.with(context)
                        .load(paymentListItem.image)
                        .listener(object : RequestListener<Drawable> {

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                ivCourseImage.setImageResource(R.mipmap.ic_launcher)
                                return false
                            }

                        })
                        .into(ivCourseImage)
            }

//            GlideApp.with(context)
//                    .load(paymentListItem.image)
//                    .placeholder(R.drawable.default_image)
//                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .dontAnimate()
//                    .listener(object : RequestListener<String, GlideDrawable> {
//                        override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
//                            Log.e(TAG, "<=== onResourceReady ===>")
//                            return false
//                        }
//
//                        override fun onException(e: java.lang.Exception?, model: String?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
//                            Log.e(TAG, "<=== onException ===>")
//                            return false
//                        }
//                    })
//                    .into(ivCourseImageCart)


        }
    }
}