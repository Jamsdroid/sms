package com.sms.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.sms.R
import com.sms.R.id.rlBottomNotification
import com.sms.model.cart.CartListItem
import com.sms.model.notification.NotificationItem
import kotlinx.android.synthetic.main.activity_notification_list.*

class NotificationListAdapter(val context: Activity, val cartList: List<NotificationItem>) : RecyclerView.Adapter<NotificationListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_notification, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: NotificationListAdapter.ViewHolder, position: Int) {
        holder.bindItems(cartList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return cartList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(notificationItem: NotificationItem) {

            val llAnswer = itemView.findViewById(R.id.llAnswer) as LinearLayout

            llAnswer.setOnClickListener {

                //                var intnetAnswerList = Intent(context, AnswerActivity::class.java)
//                context.startActivity(intnetAnswerList)

                context.rlBottomNotification.visibility = View.VISIBLE

            }

        }
    }
}