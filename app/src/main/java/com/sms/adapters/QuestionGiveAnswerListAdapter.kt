package com.sms.adapters

import android.app.Activity
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.sms.R
import com.sms.common.Utils
import com.sms.model.questionanswer.AnswerItem
import de.hdodenhof.circleimageview.CircleImageView

class QuestionGiveAnswerListAdapter(val context: Activity, val cartList: ArrayList<AnswerItem>) : RecyclerView.Adapter<QuestionGiveAnswerListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionGiveAnswerListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_answer, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: QuestionGiveAnswerListAdapter.ViewHolder, position: Int) {
        holder.bindItems(cartList[position], position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return cartList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(answerItem: AnswerItem, position: Int) {

            val llAnswer = itemView.findViewById(R.id.rootAnswer) as RelativeLayout

            val tvAnswerPost = itemView.findViewById(R.id.tvAnswerPost) as TextView
            val tvAnswerPostedBy = itemView.findViewById(R.id.tvAnswerPostedBy) as TextView
            val tvAnswerPostedOn = itemView.findViewById(R.id.tvAnswerPostedOn) as TextView

            val ivAnswerPostedBy = itemView.findViewById(R.id.ivAnswerPostedBy) as CircleImageView


            tvAnswerPost.text = answerItem.answer

            tvAnswerPostedBy.text = answerItem.answerByName
            tvAnswerPostedOn.text = Utils.convertDDMMYYYYTODDMMYYY(answerItem.createdDate)

            if (!answerItem.profileImage.isNullOrBlank()) {
                Glide.with(context)
                        .load(answerItem.profileImage)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }
                        })
                        .into(ivAnswerPostedBy)
            }

        }
    }
}