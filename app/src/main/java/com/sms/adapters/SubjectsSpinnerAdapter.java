package com.sms.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sms.R;
import com.sms.model.filter.SubjectListItem;

import java.util.ArrayList;

public class SubjectsSpinnerAdapter extends ArrayAdapter<SubjectListItem> {
    // Initialise custom font, for example:
    Typeface font = null;

    private ArrayList<SubjectListItem> dataList;

    // (In reality I used a manager which caches the Typeface objects)
    // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

    public SubjectsSpinnerAdapter(Context context, int resource, ArrayList<SubjectListItem> items) {
        super(context, resource, items);
        dataList = items;
//        font = Typeface.createFromAsset(getContext().getAssets(),
//                "font/" + context.getResources().getString(R.string.font_regular));
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
//        view.setTypeface(font);
        view.setText(dataList.get(position).getName());
        view.setTextColor(getContext().getResources().getColor(R.color.colorLoginText));
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setText(dataList.get(position).getName());
//        view.setTypeface(font);
        return view;
    }
}
