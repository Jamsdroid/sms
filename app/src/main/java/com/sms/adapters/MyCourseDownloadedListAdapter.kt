/*
package com.sms.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.sms.R
import com.sms.common.Logg
import com.sms.model.home.UserDetailsItem

class CoursesAdapter(val context: Context, val userList: List<UserDetailsItem>) : RecyclerView.Adapter<CoursesAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoursesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_item_my_courses, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CoursesAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList.get(position))
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(examListItem: UserDetailsItem) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView
            val tvCoursePrice = itemView.findViewById(R.id.tvCoursePrice) as TextView
            val tvOwnerName = itemView.findViewById(R.id.tvOwnerName) as TextView
            val tvDownloadCount = itemView.findViewById(R.id.tvDownloadCount) as TextView
            val ivProductImage = itemView.findViewById(R.id.ivCourseImage) as ImageView
            tvCourseName.text = examListItem.name

            tvOwnerName.text = examListItem.courseCreatedBy
            tvDownloadCount.text = "0" // examListItem.

            if (examListItem.isFree == "0") {
                // paid course
                tvCoursePrice.text = context.getString(R.string.symbol_rupee) + " " + examListItem.price
            } else if (examListItem.isFree == "1") {
                // free course
                tvCoursePrice.text = "free"
            }

            Logg.e(TAG, "product Name  = " + examListItem.name)

            val imageURL = examListItem.image

            Glide.with(context).load(imageURL).into(ivProductImage)
//            root.setOnClickListener {
//                if (context is ProductListingActivity) {
//                    (context as ProductListingActivity).loadProductDetail(examListItem.productId!!)
//                }
//
//
//            }

        }
    }
}*/

package com.sms.adapters

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Environment
import android.support.v4.content.FileProvider
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.sms.R
import com.sms.activity.MainActivity
import com.sms.common.Logg
import com.sms.common.Toast
import com.sms.fragment.FragmentMyCourses
import java.io.File


class MyCourseDownloadedListAdapter(val context: MainActivity, val examList: ArrayList<String>, val fragmentMyCourses: FragmentMyCourses) : RecyclerView.Adapter<MyCourseDownloadedListAdapter.ViewHolder>() {

    companion object {
        private val TAG = javaClass.simpleName
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyCourseDownloadedListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_my_course_item, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: MyCourseDownloadedListAdapter.ViewHolder, position: Int) {
        holder.bindItems(examList.get(position), position)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return examList.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(courseName: String, position: Int) {

            val root = itemView.findViewById(R.id.root) as LinearLayout
            val cvCourse = itemView.findViewById(R.id.cvCourse) as CardView
            val tvCourseName = itemView.findViewById(R.id.tvCourseName) as TextView

//            tvCourseName.text = courseName

            var strExt = ""
            if (courseName.contains("ciphernenc")) {
                strExt = courseName
                strExt = strExt.replace("ciphernenc", "epub")
                tvCourseName.text = strExt
            } else {
                tvCourseName.text = courseName
            }

            var magin5Sdp = context.resources.getDimension(R.dimen._4sdp).toInt()
            if (position % 2 == 0) {
                ((cvCourse.layoutParams) as LinearLayout.LayoutParams).setMargins(0, 0, magin5Sdp, magin5Sdp)
            } else {
                ((cvCourse.layoutParams) as LinearLayout.LayoutParams).setMargins(magin5Sdp, 0, 0, magin5Sdp)
            }

            Logg.e(TAG, "courseName ====> $courseName")

            root.setOnClickListener {

                fragmentMyCourses.cipherDecFileAndopen(context, courseName)

                /*var fileLocation = Environment.getExternalStorageDirectory().toString() + "" + File.separator + "SMS/" + courseName

                val file = File(fileLocation)
//                val path = Uri.fromFile(file)
                val path = FileProvider.getUriForFile(context, "com.sms", file)
                val pdfOpenintent = Intent(Intent.ACTION_VIEW)
                pdfOpenintent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                pdfOpenintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                pdfOpenintent.setDataAndType(path, "application/pdf")
                try {
                    context.startActivity(pdfOpenintent)
                } catch (ex: ActivityNotFoundException) {
                    Toast.showLongToast(context, "Please install pdf reader app from google play store to open the pdf")
                    ex.printStackTrace()
                }*/
            }
        }
    }
}
