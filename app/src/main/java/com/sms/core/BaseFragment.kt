package com.alcohol.core

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.sms.ApiHelper
import com.sms.common.Constants
import com.sms.common.CustomProgressBar
import com.sms.webservice.RetroFitResponse


open class BaseFragment : Fragment(), RetroFitResponse, Constants {

    var customProgressBar: CustomProgressBar? = null

    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
    }

    /* SharedPreferences is custom class use shared preferences perform */
    //    protected SharedPreferences sharedPreferences;
    /* ApiHelper use for perform api calls */
    var apiHelper: ApiHelper? = null
    private val TAG = javaClass.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        apiHelper = ApiHelper(this!!.activity!!)
        //        sharedPreferences = SharedPreferences.getInstance(getActivity());
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    /**
     * replace fragment with other fragment
     *
     * @param mFragment : fragment object which you want to replace
     */
    fun setFragment(mFragment: Fragment) {
        val fragmentManager = fragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()
        fragmentTransaction?.addToBackStack(null)
        //        fragmentTransaction.replace(R.id.fragment_container, mFragment);
        fragmentTransaction?.commit()
    }

    /**
     * for check editText empty validation
     *
     * @param editText : object of editText
     * @return : true if editText will be empty else it retrus false
     */
    fun checkEditTextBlank(editText: EditText?): Boolean {
        return null != editText && editText.text.toString().trim { it <= ' ' }.isEmpty()
    }

    /**
     * for check editText empty validation
     *
     * @param textView : object of editText
     * @return : true if editText will be empty else it retrus false
     */
    fun checkTextBlank(textView: TextView?): Boolean {
        return null != textView && textView.text.toString().trim { it <= ' ' }.isEmpty()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //        Logg.e(TAG, "onActivityResult() called with: requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
    }

    fun showProgressDialog(activity: Activity) {
        if (customProgressBar == null && !activity.isFinishing) {
            customProgressBar = CustomProgressBar(activity)
            customProgressBar!!.show()
        } else if (!customProgressBar!!.isShowing) {
            customProgressBar!!.show()
        }
    }

    fun hideProgressDialog() {
        if (customProgressBar != null && customProgressBar!!.isShowing) {
            customProgressBar!!.dismiss()
            customProgressBar = null
        }
    }
}
