package com.alcohol.core

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import android.widget.TextView
import com.sms.ApiHelper
import com.sms.common.Constants
import com.sms.common.CustomProgressBar
import com.sms.common.Logg
import com.sms.webservice.RetroFitResponse


/**
 * Created by Tops on 27-Jun-17.
 * BaseActivity is use to extends every activity
 */

open class BaseActivity : AppCompatActivity(), RetroFitResponse, Constants {
    override fun onRetrofitResponse(responseCode: Int, mRes: Any, responseTag: String) {
    }

    override fun onRetrofitError(Code: Int, mError: String, responseTag: String) {
    }

    private val TAG = BaseActivity::class.java.simpleName
    /* SharedPreferences is custom class use shared preferences perform */
    //    protected SharedPreferences sharedPreferences;

    private var fragmentManager: FragmentManager? = null
    /* ApiHelper use for perform api calls */
    lateinit var apiHelper: ApiHelper

    var customProgressBar: CustomProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //        sharedPreferences = SharedPreferences.getInstance(this);
        apiHelper = ApiHelper(this)
        fragmentManager = supportFragmentManager
    }

    //    private boolean isAlarmOn() {
    //        Intent intent = new Intent(this, AlarmReceive.class);//the same as up
    //        boolean isWorking = (PendingIntent.getBroadcast(this, UtilKey.constant.ALARM_REQUEST_CODE_BACKGROUNDSERVICE, intent, PendingIntent.FLAG_NO_CREATE) != null);//just changed the flag
    //        Logg.d(TAG, "alarm is " + (isWorking ? "" : "not") + " working...");
    //        return isWorking;
    //    }

    //    private void destroyAlarm() {
    //        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    //        //and stopping
    //        Intent intent = new Intent(this, AlarmReceive.class);//the same as up
    //        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1001, intent, PendingIntent.FLAG_CANCEL_CURRENT);//the same as up
    //        alarmManager.cancel(pendingIntent);//important
    //        pendingIntent.cancel();//important
    //        Logg.i(TAG, "destroyAlarm: ");
    //    }

    override fun onPause() {
        Logg.i(TAG, "onPause: ")
        //        if(isAlarmOn())
        //        {
        //            destroyAlarm();
        //        }
        super.onPause()
    }

    override fun onDestroy() {
        Logg.i(TAG, "onDestroy: ")
        super.onDestroy()
        /*destroy sharedPreferences object*/
        //        sharedPreferences.destroySP();
    }

    protected fun replaceFragment(mFragment: Fragment, tag: String) {
        val fm = supportFragmentManager // or 'getSupportFragmentManager();'
        val count = fm.backStackEntryCount
        for (i in 0 until count) {
            fm.popBackStack()
        }

        //        if (mFragment instanceof FragmentHome) {
        //            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        //            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //        } else {
        //            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        //        }

        val fragmentTransaction = fragmentManager!!.beginTransaction()
        //        fragmentTransaction.replace(R.id.fragment_container, mFragment, tag);
        fragmentTransaction.commit()
    }

    protected fun addFragment(mFragment: Fragment, tag: String) {
        val fm = supportFragmentManager // or 'getSupportFragmentManager();'
        val count = fm.backStackEntryCount
        for (i in 0 until count) {
            fm.popBackStack()
        }

        val fragmentTransaction = fragmentManager!!.beginTransaction()
        //        fragmentTransaction.add(R.id.fragment_container, mFragment, tag);
        fragmentTransaction.commit()
    }

    protected fun replaceFragmentWithAnim(mFragment: Fragment) {
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        //        fragmentTransaction.replace(R.id.fragment_container, mFragment);
        fragmentTransaction.commit()
    }

    protected fun addFragmentWithAnim(mFragment: Fragment) {
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        //        fragmentTransaction.replace(R.id.fragment_container, mFragment);
        fragmentTransaction.commit()
    }

    fun checkEditTextBlank(editText: EditText?): Boolean {
        return null != editText && editText.text.toString().trim { it <= ' ' }.isEmpty()
    }

    fun checkTextBlank(textView: TextView?): Boolean {
        return null != textView && textView.text.toString().trim { it <= ' ' }.isEmpty()
    }

    fun showProgressDialog(activity: Activity) {
        if (customProgressBar == null && !activity.isFinishing) {
            customProgressBar = CustomProgressBar(activity)
            customProgressBar!!.show()
        } else if (!customProgressBar!!.isShowing) {
            customProgressBar!!.show()
        }
    }

    fun hideProgressDialog() {
        if (customProgressBar != null && customProgressBar!!.isShowing) {
            customProgressBar!!.dismiss()
        }
    }

}