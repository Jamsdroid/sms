package com.sms.fcm

import android.app.ActivityManager
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.sms.activity.MainActivity
import com.sms.R
import com.sms.activity.SplashActivity
import com.sms.common.Logg
import org.json.JSONObject

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val TAG = javaClass.simpleName
    private var numMessages = 0
    private var machineId: String? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)
        Logg.e(TAG, "onMessageReceived called")
        val notification = remoteMessage!!.notification
        val data = remoteMessage.data
        Logg.d("FROM", remoteMessage.from.toString())
        sendNotification(notification, data)

        if (isApplRunningInBackground(this@MyFirebaseMessagingService)) {
            Logg.e(TAG, "<=== App is running in Background ===>")
        } else {
            Logg.e(TAG, "<=== App is not running ===>")
        }
    }

    private fun sendNotification(notification: RemoteMessage.Notification?, data: Map<String, String>?) {
        var title: String? = ""
        var body: String? = ""

        val bundle = Bundle()
        //        bundle.putString(FCM_PARAM, data.get(FCM_PARAM));

        try {
            if (notification != null) {
                if (notification.title != null) {
                    Logg.e(TAG, "Title =========> " + notification.title!!)
                    title = notification.title
                }
                if (notification.body != null) {
                    Logg.e(TAG, "Body =========> " + notification.body!!)
                    body = notification.body
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        if (data != null) {
            Logg.e(TAG, "data ===> " + data.toString())
            try {
                val dataObject = JSONObject(data)
                if (dataObject != null) {
                    if (dataObject.has("machine_id")) {
                        machineId = dataObject.getString("machine_id")
                        //                        if (!AlcoholApplication.machineAlarmsIds.contains(machineId)) {
                        //                            AlcoholApplication.machineAlarmsIds.add(machineId);
                        //                        }
                    }
                    /*if (dataObject.has("title")) {
                        title = dataObject.getString("title");
                    }

                    if (dataObject.has("message")) {
                        body = dataObject.getString("message");
                    }
                    if (dataObject.has("is_background")) {
                        isBackground = dataObject.getBoolean("is_background");
                    }*/
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        val intent: Intent
        //        intent = new Intent(this, ActivitySplash.class);

        if (isApplRunningInBackground(baseContext)) {
            Logg.e(TAG, "App is running in Background")
            intent = Intent(this, SplashActivity::class.java)
        } else {
            Logg.e(TAG, "App is not running ")
            intent = Intent(this, MainActivity::class.java)
            //            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            //            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        }

        bundle.putBoolean("fcm_notification", true)
        bundle.putString("msg", body)
        if (machineId.isNullOrBlank()) {
            bundle.putString("machine_id", machineId)
        }

        intent.putExtras(bundle)
        //        intent.putExtra("fcm_notification", true);
        //        intent.putExtra("msg", body);

        Logg.e(TAG, "title ===> " + title!!)
        Logg.e(TAG, "body ===> " + body!!)

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notificationBuilder = NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                .setContentIntent(pendingIntent)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setColor(resources.getColor(R.color.colorAccent))
                .setStyle(NotificationCompat.BigTextStyle().bigText(body))
                .setLights(Color.RED, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setNumber(++numMessages)
                .setSmallIcon(R.mipmap.ic_launcher)

        //        try {
        //            String picture = data.get(FCM_PARAM);
        //            if (picture != null && !"".equals(picture)) {
        //                URL url = new URL(picture);
        //                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        //                notificationBuilder.setStyle(
        //                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(body)
        //                );
        //            }
        //        } catch (IOException e) {
        //            e.printStackTrace();
        //        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val channel = NotificationChannel(getString(R.string.notification_channel_id), CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
//            channel.description = CHANNEL_DESC
//            channel.setShowBadge(true)
//            channel.canShowBadge()
//            channel.enableLights(true)
//            channel.lightColor = Color.RED
//            channel.enableVibration(true)
//            channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500)
//
//            assert(notificationManager != null)
//            notificationManager.createNotificationChannel(channel)
//        }

        assert(notificationManager != null)
        notificationManager.notify(0, notificationBuilder.build())
    }

    //    public boolean isApplRunningInBackground(final Context context) {
    //        ActivityManager am = (ActivityManager) context
    //                .getSystemService(Context.ACTIVITY_SERVICE);
    //        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
    //        if (!tasks.isEmpty()) {
    //            ComponentName topActivity = tasks.get(0).topActivity;
    //            if (!topActivity.getPackageName().equals(context.getPackageName())) {
    //                return false;
    //            }
    //        } else {
    //            return false;
    //        }
    //        return true;
    //    }

    private fun isApplRunningInBackground(context: Context): Boolean {
        var isInBackground = true
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == context.packageName) {
                            isInBackground = false
                        }
                    }
                }
            }
        } else {
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo.packageName == context.packageName) {
                isInBackground = false
            }
        }

        return isInBackground
    }

    companion object {
        //    public static final String FCM_PARAM = "picture";
        private val CHANNEL_NAME = "FCM"
        private val CHANNEL_DESC = "Firebase Cloud Messaging"
    }
}