package com.sms.custom;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class FileList {

    private String path;

    public FileList(String path) {
        this.path = path;
    }

    public ArrayList<String> getFilesList() {
        File f = new File(path);
        ArrayList<String> names = new ArrayList<String>(Arrays.asList(f.list()));

        return names;
    }
}
