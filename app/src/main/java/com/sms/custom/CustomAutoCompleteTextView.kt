package com.sms.custom

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.util.AttributeSet
import android.widget.AutoCompleteTextView
import com.sms.R

/**
 * TextView subclass which allows the user to define a truetype font file to use as the view's typeface.
 */
class CustomAutoCompleteTextView : AutoCompleteTextView {
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context) : super(context) {
        init(null)
    }

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
//            val a = context.obtainStyledAttributes(attrs, R.styleable.CustomView)
//            val fontName = a.getString(R.styleable.CustomView_font_name)
//            try {
//                if (fontName != null) {
//                    val myTypeface = Typeface.createFromAsset(context.assets, "fonts/" + fontName)
//                    typeface = myTypeface
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//            a.recycle()
        }
    }
}