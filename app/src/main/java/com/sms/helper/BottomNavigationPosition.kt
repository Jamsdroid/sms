package com.alcohol.helper

import android.support.v4.app.Fragment
import com.sms.R
import com.sms.fragment.*

enum class BottomNavigationPosition(val position: Int, val id: Int) {
    HOME(0, R.id.home),
    MY_COURSES(1, R.id.my_courses),
    EXAM(2, R.id.exam),
    MY_ACCOUNT(3, R.id.my_account);
}

fun findNavigationPositionById(id: Int): BottomNavigationPosition = when (id) {
    BottomNavigationPosition.HOME.id -> BottomNavigationPosition.HOME
    BottomNavigationPosition.MY_COURSES.id -> BottomNavigationPosition.MY_COURSES
    BottomNavigationPosition.EXAM.id -> BottomNavigationPosition.EXAM
    BottomNavigationPosition.MY_ACCOUNT.id -> BottomNavigationPosition.MY_ACCOUNT
    else -> BottomNavigationPosition.HOME
}

fun BottomNavigationPosition.createFragment(): Fragment = when (this) {
    BottomNavigationPosition.HOME -> FragmentHome.newInstance()
    BottomNavigationPosition.MY_COURSES -> FragmentMyCourses.newInstance()
    BottomNavigationPosition.EXAM -> FragmentExam.newInstance()
    BottomNavigationPosition.MY_ACCOUNT -> FragmentMyAccount.newInstance()
}

fun BottomNavigationPosition.getTag(): String = when (this) {
    BottomNavigationPosition.HOME -> FragmentHome.TAG
    BottomNavigationPosition.MY_COURSES -> FragmentMyCourses.TAG
    BottomNavigationPosition.EXAM -> FragmentExam.TAG
    BottomNavigationPosition.MY_ACCOUNT -> FragmentMyAccount.TAG
}

